class Occam
  # Index
  get '/resources/application-zip/:uuid/:revision' do
  end

  # File Listing
  get '/resources/application-zip/:uuid/:revision/tree/*' do
  end

  # File Retrieval
  get '/resources/application-zip/:uuid/:revision/raw/*' do
  end

  # File Stat
  get '/resources/application-zip/:uuid/:revision/stat/*' do
  end
end
