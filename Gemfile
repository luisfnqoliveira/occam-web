source 'https://rubygems.org'
ruby '2.3.0'

# Web Framework
gem 'sinatra', '~> 1.4.4'
gem 'sinatra-contrib'

# Database Abstraction
gem 'sinatra-activerecord'
gem 'activerecord', '~> 4.0.0'

# Markup Rendering Engine
gem 'haml'      # Haml
gem 'redcarpet', '~> 3.1.2' # Markdown
gem 'nokogiri'  # XML/HTML Parsing

# Time duration markup (270s => "4 mins 30 secs")
gem 'chronic_duration'

# MIME Type Determination
gem 'mime-types', '>= 2.0.0'

# Internationalization
gem 'i18n'

# URL-Safe String Processing
gem 'stringex'

# Password hashing
gem 'bcrypt-ruby'

# SSH key generator
gem 'sshkey'

# Runs Rakefiles
gem 'rake'

# Zip file access
gem 'rubyzip'

# Development only tools
group :development do
  gem 'sqlite3'      # Development Database
  gem 'ruby-prof'    # Performance monitoring
end

# Testing environment libraries
group :test do
  gem 'capybara', '~> 1.1.2',  :require => 'capybara/dsl'
  gem 'fabrication', '~> 1.2.0'
  gem 'database_cleaner', '~> 1.2.0'
  gem 'rack-test', '~> 0.6.1', :require => 'rack/test'
  gem 'minitest', '~> 4.7.0', :require => 'minitest/autorun'
  gem "ansi"              # minitest colors
  gem "turn"              # minitest output
  gem "mocha", "~> 1.1.0" # stubs

  # Javascript testing
  gem "jasmine"
end

# Production level libraries
group :production do
  gem 'pg'
end

gem 'bunny'

# Web Server
gem 'thin'

# Email server (smtp) support
gem 'mail'

# WebSocket support
gem "faye-websocket"

# Sass (Stylesheet Format)
gem 'sass'
