class Occam
  # This module
  module Backend
    def self.connected
      not self.config.empty?
    end

    def self.parse_configuration
      config = Occam::Config.configuration['backend'] || {}

      if not config.empty?
        config['port'] ||= 80
        config['scheme'] ||= 'http'
      end

      config
    end

    def self.config
      @backend_config ||= self.parse_configuration
    end

    def self.gitIssueService(uuid, input, service, url, writer, length=nil)
      # Forward through git service url of backend node
      if input.nil?
        reader = self.download(url, '*/*')
        reader = StringIO.new(reader)
      else
        reader = self.upload(url, input, length)
        reader = StringIO.new(reader)
      end

      while !reader.eof?
        block = reader.read(8192) # 8K at a time
        writer << block
      end
    end

    def self.gitInfoRefs(uuid, service, path)
    end

    def self.fullRevision(uuid, revision)
      data = self.downloadJSON("/objects/#{uuid}/#{revision}/full_revision")

      if data
        data["revision"]
      else
        data
      end
    end

    def self.parentRevision(uuid, revision)
      data = self.downloadJSON("/objects/#{uuid}/#{revision}/parent_revision")

      if data
        data["parentRevision"]
      else
        data
      end
    end

    def self.childRevisions(uuid, revision)
      data = self.downloadJSON("/objects/#{uuid}/#{revision}/child_revisions")

      if data
        data["childRevisions"]
      else
        data
      end
    end

    def self.commits(uuid, revision, forPath)
      commits = self.downloadJSON("/objects/#{uuid}/#{revision}/history")

      commits.each do |commit|
        commit[:message]  = commit["message"]
        commit[:author]   = commit["author"]
        commit[:date]     = commit["date"]
        commit[:revision] = commit["revision"]
      end

      commits
    end

    def self.uploadFile(uuid, revision, workset_uuid, workset_revision, name, io, to)
      if self.connected
        # Hit the same upload spot
        self.uploadMultipart("/worksets/#{workset_uuid}/#{workset_revision}/objects/#{uuid}/#{revision}/tree/#{to}", io, name)
      else
        false
      end
    end

    def self.eachFile(uuid, revision, filepath, pretext, &block)
      if self.connected
        pretext = pretext || "/objects"
        data = self.downloadJSON("#{pretext}/#{uuid}/#{revision}/tree/#{filepath}")

        if !data
          return false
        end

        data["files"] ||= []
        data["files"].each do |file|
          file.keys.each do |k|
            if ["name", "size", "type", "stat", "hash"].include? k
              file[k.intern] = file[k]
            end
          end
          yield(file)
        end
      else
        false
      end
    end

    def self.retrieveInvocationData(uuid, type)
      if self.connected
        self.downloadJSON("/objects/#{uuid}/invocations/#{type}")
      else
        false
      end
    end

    def self.retrieveJSON(uuid, revision, path, pretext=nil)
      if self.connected
        pretext = pretext || "/objects"
        self.downloadJSON("#{pretext}/#{uuid}/#{revision}/raw/#{path}")
      else
        false
      end
    end

    def self.retrieveFile(uuid, revision, path, pretext)
      if self.connected
        pretext = pretext || "/objects"
        url = "#{pretext}/#{uuid}/#{revision}/raw/#{path}"
        data = self.download(url)
        data
      else
        false
      end
    end

    def self.retrieveFileInfo(uuid, revision, path, pretext)
      if self.connected
        pretext = pretext || "/objects"
        url = "#{pretext}/#{uuid}/#{revision}/stat/#{path}"
        data = self.downloadJSON(url)
        data
      else
        false
      end
    end

    def self.downloadJSON(path)
      if self.connected
        data = self.download(path, 'application/json')
        if data == false
          false
        else
          begin
            JSON.parse(data)
          rescue
            puts "JSON Parsing Error #{data}"
            {}
          end
        end
      else
        false
      end
    end

    def self.download(path, content_type = 'text/plain', limit = 10)
      if limit < 0
        false
      end

      request = Net::HTTP::Get.new(URI.encode(path))
      request['Accept'] = content_type
      request.content_type = content_type

      http = Net::HTTP.new(self.config['host'], self.config['port'])
      if self.config['scheme'].downcase == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end

      response = http.request(request)

      # Did we get a response? (allow redirects)
      if response.is_a?(Net::HTTPRedirection) && limit > 0
        location = response['location']
        uri = URI(location)
        if uri.host == self.config['host'] && uri.port == self.config['port'] && uri.scheme == self.config['scheme']
          location = uri.request_uri
          Occam::Backend.download(location, content_type, limit - 1)
        else
          # Do not allow a host/port change redirect
          false
        end
      elsif response.is_a?(Net::HTTPSuccess)
        response.body
      else # Otherwise, error out
        false
      end
    end

    class MultipartStream
      def initialize(streams)
        @streams = streams
        @current = 0
        @offset = 0
      end

      def read(length=nil, outbuf=nil)
        ret = ""

        if length.nil?
          length = self.length
          if @current >= @streams.length
            return ret
          end
        end

        if length == 0
          return ret
        end

        if @current >= @streams.length
          return nil
        end

        left = length

        current_length = @offset + length
        if current_length >= @streams[@current].length
          # Read entire part and advance

          left -= @streams[@current].length - @offset
          ret = @streams[@current].read || ""

          @current += 1
          @offset = 0
        else
          ret = @streams[@current].read(left) || ""
          @offset += left
          left = 0
        end

        ret += read(left) || ""

        if outbuf
          outbuf.replace(ret)
        else
          ret
        end
      end

      def size
        @streams.map(&:length).reduce(&:+)
      end

      alias_method :length, :size
    end

    def self.upload(path, io, size = nil)
      if size.nil?
        size = io.length
      end

      request = Net::HTTP::Post.new(path)

      http = Net::HTTP.new(self.config['host'], self.config['port'])
      if self.config['scheme'].downcase == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end

      # TODO: Handle streaming.

      request.content_length = size
      request.content_type = 'text/plain'
      request.body_stream = io

      response = http.request(request)

      # Did we get a response?
      if response.is_a?(Net::HTTPSuccess)
        response.body
      else # Otherwise, error out
        false
      end
    end

    def self.uploadMultipart(path, io, name=nil)
      request = Net::HTTP::Post.new(path)

      http = Net::HTTP.new(self.config['host'], self.config['port'])
      if self.config['scheme'].downcase == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end

      # TODO: Handle streaming.
      # TODO: Handle more than one file.
      boundary = '-------------OccamMultipartUpload' + rand(1000000).to_s + 'zz'

      names = [name]
      parts = []
      names.each do |filename|
        # TODO: urlencode the form
        disposition = ""
        if filename
          disposition = "Content-Disposition: form-data; name=\"files[]\"; filename=\"#{filename}\"\r\n"
        end

        parts << StringIO.new("--#{boundary}\r\n#{disposition}Content-Type: application/octet-stream\r\n\r\n")
        parts << io
      end

      parts << StringIO.new("\r\n--#{boundary}--\r\n")

      post_stream = MultipartStream.new(parts)

      request.content_length = post_stream.size
      request.content_type = 'multipart/form-data; boundary=' + boundary
      request.body_stream = post_stream

      response = http.request(request)

      # Did we get a response?
      if response.is_a?(Net::HTTPSuccess)
        response.body
      else # Otherwise, error out
        false
      end
    end
  end
end
