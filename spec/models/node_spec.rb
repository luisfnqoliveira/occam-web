require_relative "helper"
require_model "node"

describe Occam::Node do
  describe "#name" do
    it "should use the host when no name is given" do
      node = Occam::Node.create(:host => "occam.example.org",
                                :http_ssl => 0,
                                :http_port => 80)

      node.name.must_equal node.host
    end

    it "should use only the name when it exists" do
      node = Occam::Node.create(:host => "occam.example.org",
                                :name => "OCCAM NODE",
                                :http_ssl => 0,
                                :http_port => 80)

      node.name.must_equal "OCCAM NODE"
    end
  end

  describe "#http_ssl" do
    it "should return false when http_ssl is nil" do
      node = Occam::Node.create(:host => "occam.example.org",
                                :http_port => 80)

      node.http_ssl.must_equal false
    end

    it "should return false when http_ssl is 0" do
      node = Occam::Node.create(:host => "occam.example.org",
                                :http_ssl => 0,
                                :http_port => 80)

      node.http_ssl.must_equal false
    end

    it "should return true when http_ssl is 1" do
      node = Occam::Node.create(:host => "occam.example.org",
                                :http_ssl => 1,
                                :http_port => 80)

      node.http_ssl.must_equal true
    end
  end

  describe "#capabilities" do
    it "should return an empty array when the capabilities are nil" do
      node = Occam::Node.create(:host => "occam.example.org",
                                :http_port => 80)

      node.capabilities.must_equal []
    end

    it "should return an array for the capabilities delimited by ;" do
      node = Occam::Node.create(:host => "occam.example.org",
                                :capabilities => ";foo;bar;",
                                :http_port => 80)

      node.capabilities.must_equal ["foo", "bar"]
    end
  end
end
