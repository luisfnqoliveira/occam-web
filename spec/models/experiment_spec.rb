require_relative "helper"
require_model "object"
require_model "local_link"
require_model "person"
require_model "authorship"
require_model "review_capability"
require_model "collaboratorship"
require_model "workset"
require_model "group"
require_model "experiment"

describe Occam::Experiment do
  before :each do
    @object = Occam::Object.new(nil, :uid => "EXPERIMENT_UUID", :name => "experiment", :objectInfo => {}, :private => 0, :revision => "CURRENT_REVISION")
    @object.save
    @experiment = Occam::Experiment.create(:object => @object)

    object = Occam::Object.new(nil, :uid => "WORKSET_UUID", :name => "workset", :objectInfo => {}, :private => 0, :revision => "CURRENT_REVISION")
    object.save
    @workset = Occam::Workset.create(:occam_object_id => @object.id)

    object = Occam::Object.new(nil, :uid => "PERSON_UUID", :name => "person", :objectInfo => {}, :private => 0, :revision => "CURRENT_REVISION")
    object.save
    @person = Occam::Person.create(:occam_object_id => @object.id)

    @simulator = Occam::Object.new(nil, :uid => "SIMULATOR_UUID", :name => "simulator", :object_type => "simulator", :object_type_safe => "simulator", :objectInfo => {}, :private => 0, :revision => "CURRENT_REVISION")
    @simulator.save
    Occam::Object.any_instance.stubs(:inputs_info).returns([{}])
  end

  describe "#run" do
    it "should invoke an occam worker with the given workset and person" do
      Occam::Worker.expects(:perform).returns("")
      @experiment.run(@workset, @person)
    end
  end

  describe "#fork" do
    it "should invoke an occam worker with the given name, destination object, and person" do
      Occam::Worker.expects(:perform).returns("")
      @experiment.fork("new_name", @workset, @person)
    end
  end

  describe "#attach" do
    # TODO: work on thoroughly testing this
    it "should invoke an occam worker with the given name, destination object, and person" do
      Occam::Worker.expects(:perform).returns("")
      @experiment.attach(@simulator, @workset, 0, nil, nil)
    end
  end

  describe "#detach" do
    it "should invoke an occam worker with the given workset and connection index" do
      Occam::Worker.expects(:perform).returns("")
      @experiment.detach(@workset, 0)
    end
  end

  describe "#update_info" do
    it "should invoke an occam worker with the given data and workset" do
      Occam::Worker.expects(:perform).returns("")
      @experiment.update_info({}, @workset)
    end
  end

  describe "#configure" do
    # TODO: work on thoroughly testing this
    it "should invoke an occam worker with the given data and connection index, workset and file list" do
      Occam::Worker.expects(:perform).returns("")
      @experiment.configure(0, {}, @workset, [])
    end
  end

  describe "#workflow" do
    it "should return a workflow with an empty connections array when the object has no workflow" do
      object = Occam::Object.new(nil, :uid => "EXPERIMENT_UUID",
                                      :name => "experiment",
                                      :objectInfo => {
                                        "id" => "EXPERIMENT_UUID"
                                      },
                                      :private => 0,
                                      :revision => "CURRENT_REVISION")
      object.save
      experiment = Occam::Experiment.create(:object => object)
      experiment.workflow['connections'].length.must_equal 0
    end

    it "should return a workflow with an empty connections array when the object has an empty workflow" do
      object = Occam::Object.new(nil, :uid => "EXPERIMENT_UUID",
                                      :name => "experiment",
                                      :objectInfo => {
                                        "id" => "EXPERIMENT_UUID",
                                        "workflow" => {
                                        }
                                      },
                                      :private => 0,
                                      :revision => "CURRENT_REVISION")
      object.save
      experiment = Occam::Experiment.create(:object => object)
      experiment.workflow['connections'].length.must_equal 0
    end

    it "should return a workflow with an empty connections array when the object has a workflow with an empty connections array" do
      object = Occam::Object.new(nil, :uid => "EXPERIMENT_UUID",
                                      :name => "experiment",
                                      :objectInfo => {
                                        "id" => "EXPERIMENT_UUID",
                                        "workflow" => {
                                          "connections" => []
                                        }
                                      },
                                      :private => 0,
                                      :revision => "CURRENT_REVISION")
      object.save
      experiment = Occam::Experiment.create(:object => object)
      experiment.workflow['connections'].length.must_equal 0
    end

    it "should return a workflow with a connections array that corresponds to the object's workflow metadata" do
      object = Occam::Object.new(nil, :uid => "EXPERIMENT_UUID",
                                      :name => "experiment",
                                      :objectInfo => {
                                        "id" => "EXPERIMENT_UUID",
                                        "workflow" => {
                                          "connections" => [
                                            {
                                              "object" => {
                                                "id"   => @simulator.uid,
                                                "name" => @simulator.name
                                              }
                                            }
                                          ]
                                        }
                                      },
                                      :private => 0,
                                      :revision => "CURRENT_REVISION")
      object.save
      experiment = Occam::Experiment.create(:object => object)
      experiment.workflow['connections'].length.must_equal 1
    end

    it "should return a workflow with a connections array that contains the instance of the depicted object as object_realized" do
      object = Occam::Object.new(nil, :uid => "EXPERIMENT_UUID",
                                      :name => "experiment",
                                      :objectInfo => {
                                        "id" => "EXPERIMENT_UUID",
                                        "workflow" => {
                                          "connections" => [
                                            {
                                              "object" => {
                                                "id"   => @simulator.uid,
                                                "name" => @simulator.name
                                              }
                                            }
                                          ]
                                        }
                                      },
                                      :private => 0,
                                      :revision => "CURRENT_REVISION")
      object.save
      experiment = Occam::Experiment.create(:object => object)
      experiment.workflow['connections'].first['object_realized'].uid == @simulator.uid
    end

    it "should return a workflow with a connections array which contains input objects with a 'to' key that points to the main object" do
      object = Occam::Object.new(nil, :uid => "EXPERIMENT_UUID",
                                      :name => "experiment",
                                      :objectInfo => {
                                        "id" => "EXPERIMENT_UUID",
                                        "workflow" => {
                                          "connections" => [
                                            {
                                              "object" => {
                                                "id"   => @simulator.uid,
                                                "name" => @simulator.name
                                              }
                                            },
                                            {
                                              "object" => {
                                                "id"   => @simulator.uid,
                                                "name" => @simulator.name
                                              },
                                              "to" => 0
                                            }
                                          ]
                                        }
                                      },
                                      :private => 0,
                                      :revision => "CURRENT_REVISION")
      object.save
      experiment = Occam::Experiment.create(:object => object)
      experiment.workflow['connections'][1]['to'].must_equal 0
    end

    #TODO: add tests for input_max/input_types etc
  end

  describe "#tail_connections" do
    it "should return an empty array when the workflow connections is empty" do
      @experiment.tail_connections.must_equal []
    end

    it "should return the array of connections with no inputs" do
      input_object = Occam::Object.new(nil, :uid => "OBJECT_UUID", :name => "object", :object_type => "object", :object_type_safe => "object", :objectInfo => {}, :private => 0, :revision => "CURRENT_REVISION")
      input_object.save
      object = Occam::Object.new(nil, :uid => "EXPERIMENT_UUID",
                                      :name => "experiment",
                                      :objectInfo => {
                                        "id" => "EXPERIMENT_UUID",
                                        "workflow" => {
                                          "connections" => [
                                            {
                                              "object" => {
                                                "id"   => @simulator.uid,
                                                "name" => "A"
                                              }
                                            },
                                            {
                                              "object" => {
                                                "id"   => input_object.uid,
                                                "name" => "B"
                                              },
                                              "to" => 0
                                            }
                                          ]
                                        }
                                      },
                                      :private => 0,
                                      :revision => "CURRENT_REVISION")
      object.save
      experiment = Occam::Experiment.create(:object => object)
      experiment.tail_connections[0]["object_realized"].uid.must_equal @simulator.uid
    end
  end
end
