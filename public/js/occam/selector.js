/* This files handles dropdown widgets and object/person selectors.
 *
 * This will replace any <SELECT> that has a 'selector' class with the dropdown
 * widget. Any <OPTION> with certain attributes will have certain behaviors:
 *
 * <option> attributes:
 * data-icon: the css class to attach with the 'icon' class to add an icon
 */

// Initialize all selectors on the page
$(function() {
  var selectors = $('select.selector');

  selectors.each(function() {
    var selector = new Occam.Selector($(this));
  });
});

var initOccamSelector = function(Occam) {
  var Selector = Occam.Selector = function(element) {
    if (element[0].tagName.toUpperCase() == "SELECT") {
      this.element = undefined;
      this.select  = element;
    }
    else {
      this.element = element;
      this.select  = element.prev();
    }

    // Bind events
    this.bindEvents();
  };

  Selector.prototype.bindEvents = function() {
    var self = this;

    var options = this.select.children('option');

    // Form dropdown section
    var dropdown = $('<ul class="dropdown" tabindex="1"></ul>');
    options.each(function() {
      var item = $('<li></li>');
      var option = $(this);
      var header = $('<h2></h2>');
      header.addClass(option.data('icon'));
      header.addClass('icon');
      header.text(option.text());
      item.data('original-text', option.text());
      if (option.data('i18n')) {
        header.text(option.data('i18n'));
      }
      item.append(header);
      dropdown.append(item);
    });

    // Append dropdown to body
    $('body .content').append(dropdown);

    var selector = this.element;
    if (this.element === undefined) {
      selector = $('<div class="selector"></div>');
      selector.text(this.select.children('option:selected').text());
      selector.data('original-text', selector.text());
      if (this.select.children('option:selected').data('i18n')) {
        selector.text(this.select.children('option:selected').data('i18n'));
      }
      var selected = this.select.children('option:selected').index();
      this.select.after(selector);
      this.element = selector;
    }

    this.select.css({
      "display": "none"
    });

    selector.css({
      "background-image": dropdown.children().slice(selected, selected+1).children('h2').css("background-image")
    });

    this.element.on('click', function(event) {
      dropdown.css({
        "width": $(this).width() + 70
      }).on('blur', function(event) {
        $(this).hide();
        event.stopPropagation();
        event.preventDefault();
      });

      var offset = selector.offset();

      var height = dropdown.height();

      dropdown.attr("aria-hidden", "false").css({
        "left": offset.left + "px",
        "top":  (offset.top + 41) + "px",
        "display": "block",
        "height": "0px"
      }).focus().animate({
        "height": height + "px"
      }, 200);

      dropdown.children('li').on('click', function(event) {
        dropdown.trigger('blur');
        selector.text($(this).children('h2').text());
        selector.data('original-text', $(this).data('original-text'));
        selector.css({
          "background-image": $(this).children('h2').css("background-image")
        });
        var index = $(this).index();
        self.select.children().slice(index, index+1).prop('selected', true);
        event.stopPropagation();
        event.preventDefault();
      });

      event.stopPropagation();
      event.preventDefault();
    });
  };
};
