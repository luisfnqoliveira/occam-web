/*
 * This module handles Terminal views. These are the nice TTY and logging
 * windows that exist throughout the site. You can create a Terminal by
 * passing along the element that will serve as the terminal or log container
 * along with some text for the button/link to initiate it.
 */

var initOccamTerminal = function(Occam) {
  'use strict';

  var messageCallback = function(data) {
    var terminal = this;

    if (terminal.type == "tty") {
      var maxScroll = terminal.element[0].scrollHeight - terminal.element[0].clientHeight;
      var autoScroll = (terminal.element[0].scrollTop >= maxScroll - 0.5);
      var response = Occam.VT100.parse(data.output, terminal.element, terminal.context);

      if (response.length > 0) {
        var message = {
          "request": "write",
          "terminal": terminal.terminalId,
          "input":    response
        };
        terminal.ws.send(message);
      }
      if (autoScroll) {
        terminal.element[0].scrollTop = terminal.element[0].scrollHeight - terminal.element[0].clientHeight;
      }
    }
    else {
      var inputs = data.output.split('\n');

      var maxScroll = terminal.logRootElement[0].scrollHeight - terminal.logRootElement[0].clientHeight;
      var autoScroll = (terminal.logRootElement[0].scrollTop >= maxScroll - 0.5);

      if (inputs.length > 0) {
        inputs[0] = terminal.pendingLogItem + inputs[0];
        terminal.pendingLogItem = "";
      }

      for (var i = 0; i < inputs.length; i++) {
        var logItem = {};

        inputs[i] = inputs[i].trim();
        if (inputs[i].length == 0) {
          continue;
        }

        try {
          logItem = JSON.parse(inputs[i]);
        }
        catch(e) {
          if (i == inputs.length-1) {
            terminal.pendingLogItem = inputs[i];
          }
          continue;
        }

        if (logItem.params === undefined) {
          logItem.params = {};
        }
        logItem.params = logItem.params || {};

        if (logItem.type == "event") {
          // Handle the event
          if (logItem.message == "video") {
            // Switch tabs and start the VNC client
            var videoTab = $(terminal.logTabs.children('.tab').get(3));
            videoTab.attr('aria-hidden', 'false').trigger('click');
            var button = $(terminal.logTabPanels.children('.tab-panel').get(3)).find('#stream_connect_button');
            button.data('port', logItem.params.port);
            button.trigger('click');
          }
          continue;
        }

        var logElement = $('<li></li>');
        logElement.addClass(logItem.type);
        logElement.text(logItem.message);
        logElement.css({"opacity": 0});
        logItem.params = logItem.params || {};
        if (logItem.params['source']) {
          logElement.data('source', logItem.params['source']);
        }

        if (logItem.params['source'] && (terminal.lastLogElement.data('source') == logElement.data('source'))) {
          // Combine elements with the same source
          terminal.lastLogElement.append('<br>');
          var section = $('<span></span>');
          section.text(logItem.message);
          terminal.lastLogElement.append(section);
        }
        else {
          terminal.logListElement.append(logElement);
          terminal.lastLogElement = logElement;

          logElement.animate({"opacity": 1.0});
        }

        if (autoScroll) {
          terminal.logRootElement[0].scrollTop = terminal.logRootElement[0].scrollHeight - terminal.logRootElement[0].clientHeight;
        }
      }
    }
  };

  /*
   * This constructor creates an object that represents an active Terminal
   * session. This can be a "tty" which emulates a real terminal session via
   * emulation. Or it can be a "log" which parses Occam log information and
   * presents it styled.
   */
  var Terminal = Occam.Terminal = function(name, type, data, element, link) {
    var self = this;

    // Determine a unique id
    this.terminalId = name + Math.floor(Math.random() * (1000 + 1));

    // Remember this terminal
    Occam.Terminal._terminals[this.terminalId] = this;

    // The terminal data to send upon open
    this.data = data || {};

    // Initialize the vt100 context
    this.context = Occam.VT100.initializeContext();

    // Pull out link text from element
    if (element.data('link-text') != undefined) {
      link = element.data('link-text');
    }

    this.name    = name;
    this.link    = link || "Run Terminal";
    this.type    = type;
    this.element = element;
    this.element.data('terminal-id', this.terminalId);

    this.pendingLogItem = "";

    // Open websocket route.
    this.ws = Occam.WebSocket.route(this.terminalId, messageCallback, this);

    // Append the run link to the tty terminal
    if (type == "tty") {
      var runLink = this.runLink();
      element.prepend(runLink);

      // Map key events

      element.on('keydown', function(event) {
        // Handle special cases
        var key = "";
        if (event.which == 8) {
          key = "\b";
        }
        else if (event.which == 0x1b) {
          key = "\x1b";
        }
        else if (event.which == 37) { // Left arrow
          key = "\x1b[D";
        }
        else if (event.which == 38) { // Up arrow
          key = "\x1b[A";
        }
        else if (event.which == 39) { // Right arrow
          key = "\x1b[C";
        }
        else if (event.which == 40) { // Down arrow
          key = "\x1b[B";
        }

        if (key != "") {
          self.sendKey(key);
          event.stopPropagation();
          event.preventDefault();
        }
      });

      element.on('keyup', function(event) {
        // Handle special cases
        if (event.which == 8) {
          event.stopPropagation();
          event.preventDefault();
        }
      });

      element.on('keypress', function(event) {
        event.stopPropagation();
        event.preventDefault();

        var key = event.key;
        if (key == undefined) {
          key = String.fromCharCode(event.which || event.charCode);
        }

        if (event.which == 0x1e) {
          return;
        }
        else if (event.which == 13) {
          key = "\n";
        }
        else if (event.which == 9 || event.charCode == 9 || event.key == "Tab") {
          key = "\t";
        }
        else if (event.which == 8) {
          return;
        }
        else if (key.length > 1) {
          return;
        }
        else if (event.ctrlKey) {
          key = String.fromCharCode(key.charCodeAt(0) - "a".charCodeAt(0) + 1);
        }
        else if (event.altKey) {
          key = "\e" + key;
        }

        self.sendKey(key);
      });
    }
    else {
      var terminal = this;
      terminal.logTabPanels = element.parent().parent();
      terminal.logTabs      = terminal.logTabPanels.prev();

      terminal.logRootElement = terminal.logTabPanels.children('.tab-panel.run-log').find('.log');

      var list = $('<ul></ul>');
      terminal.logRootElement.append(list);
      terminal.logListElement = list;

      var logElement = $('<li></li>');
      logElement.addClass('debug');
      logElement.text('Waiting to run.');
      terminal.logListElement.append(logElement);

      terminal.lastLogElement = logElement;

      terminal.logRootElement.parent().parent().find('input.start-run').on('click', function(event) {
        var tab_panel = terminal.logRootElement.parent();
        var tab_index = tab_panel.index();
        tab_panel.parent().prev().children('.tab').slice(tab_index, tab_index+1).trigger('click');

        event.stopPropagation();
        event.preventDefault();

        $(this).remove();

        terminal.open();
      });
    }

    return this;
  };

  /* Contains a list of all active terminals
   */
  Terminal._terminals = {};

  Terminal.retrieve = function(element_or_terminalId) {
    var id = element_or_terminalId;
    console.log(element_or_terminalId);
    if (id.data !== undefined) {
      id = id.data('terminal-id');
      console.log(id);
    }
    console.log(Occam.Terminal._terminals);
    return Occam.Terminal._terminals[id];
  };

  Terminal.prototype.runLink = function() {
    var terminal = this;

    if (this._runLink === undefined) {
      this._runLink = $('<a>' + terminal.link + '</a>')
        .attr('href', '#')
        .on('click', function(event) {
          event.stopPropagation();
          event.preventDefault();

          var tmp = $('<span></span>');
          tmp.css({
            "font-family": terminal.element.css('font-family'),
            "font-size":   terminal.element.css('font-size'),
            "font-weight": terminal.element.css('font-weight')
          });
          $('body').append(tmp);
          tmp.text('X');
          terminal.context.charHeight = tmp.height();
          terminal.context.charWidth  = tmp.width();
          terminal.context.height = 20;
          terminal.context.width  = terminal.element[0].clientWidth / terminal.context.charWidth;
          terminal.context.height = Math.round(terminal.context.height - 0.5);
          terminal.context.width  = Math.round(terminal.context.width  - 0.5);
          terminal.context.scroll.start = 0;
          terminal.context.scroll.end   = terminal.context.height - 1;
          // Recalcuate the average cell width because some browsers
          // are trying to be too clever with monospace fonts.
          tmp.text(new Array(terminal.context.width + 1).join("\u00a0"));
          terminal.context.charWidth = tmp.width() / terminal.context.width;
          tmp.remove();

          terminal.element.css({
            "position": "relative"
          });

          terminal.element.innerHeight(terminal.context.height * terminal.context.charHeight + 30);

          terminal.context.caret.css({
            "height": terminal.context.charHeight,
            "width":  terminal.context.charWidth,
            "left":   "0px",
            "top":    "0px"
          });

          terminal.element.append(terminal.context.caret);

          Occam.VT100.addLines(terminal.context.height, terminal.element, terminal.context);

          $(this).remove();
          Occam.VT100.move(0,0,terminal.element,terminal.context);

          terminal.open();
      });
    }

    return this._runLink;
  };

  /*
   * Sends the key to the server.
   */
  Terminal.prototype.sendKey = function(key) {
    var message = {
      "request": "write",
      "terminal": this.terminalId,
      "input": key
    };
    this.ws.send(message);
  };

  /*
   * Runs the terminal.
   */
  Terminal.prototype.open = function() {
    var terminal = this;

    var message = {
      "request":  "open",
      "spawning": terminal.name,
      "data": terminal.data,
      "rows": terminal.context.height,
      "cols": terminal.context.width,
      "terminal": terminal.terminalId
    };

    this.ws.send(message);
  };
};
