class Occam
  def issue_git_service(uuid, service)
    object = Occam::Object.where(:uid => uuid).first()
    if object.nil?
      status 404
      return
    end

    size = request.content_length
    request.body.rewind
    url = request.path

    content_type "application/x-git-#{service}-result"

    stream do |out|
      if Occam::Backend.connected
        input = request.body.read
        Occam::Backend.gitIssueService(uuid, StringIO.new(input), service, url, out, size)
      else
        if request.env["HTTP_CONTENT_ENCODING"] == "gzip"
          require 'zlib'
          gz = Zlib::GzipReader.new(request.body)
          input = gz.read()
        else
          input = request.body.read
        end
        # TODO: gzip encode if HTTP_ACCEPT_ENCODING allows gzip
        #       for this, tag HTTP_CONTENT_ENCODING as "gzip" in response headers
        if params[:backed]
          path = object.gitPath
        else
          path = object.path
        end
        command = "git #{service} --stateless-rpc #{path}"

        IO.popen(command, File::RDWR) do |pipe|
          pipe.write(input)
          while !pipe.eof?
            block = pipe.read(8192) # 8K at a time
            out << block # steam it to the client
          end
        end
        out.close
      end
    end
  end

  def publish_git_head(uuid, path)
    path = File.join(path, "HEAD")

    no_cache
    content_type "text/plain"
    send_file path
  end

  def publish_git_info_refs(uuid, path)
    object = Occam::Object.where(:uid => uuid).first()
    if object.nil?
      status 404
      return
    end

    no_cache

    service = params["service"]
    input = request.body
    input_length = request.content_length
    if input_length.nil? || input_length == 0
      input = nil
    end

    content_type "application/x-#{service}-advertisement"
    if Occam::Backend.connected
      response.body.clear
      Occam::Backend.gitIssueService(uuid, input, service, request.url, response.body)
      response.finish
    else
      if params[:backed]
        path = object.gitPath
      else
        path = object.path
      end
      if service
        if service.match(/^git-/)
          service = service.gsub(/^git-/, "")

          case service
          when "upload-pack"
            def packet_write(line)
              (line.size + 4).to_s(base=16).rjust(4, "0") + line
            end

            response.body.clear
            response.body << packet_write("# service=git-#{service}\n")
            response.body << "0000"
            response.body << `git upload-pack --stateless-rpc --advertise-refs #{path}`
            response.finish
          else
            status 404
          end
        else
          status 404
        end
      else
        content_type "text/plain"
        path = File.join(path, "info", "refs")
        send_file path
      end
    end
  end

  def publish_git_objects_info_file(path, file)
    system = Occam::System.first
    status 404 and return if not system.curate_git

    if file == "packs"
      no_cache
      content_type "text/plain; charset=utf-8"
      path = File.join(path, "objects", "info", "packs")
      send_file path
    elsif file == "alternates" or file == "http-alternates"
      no_cache
      content_type "text/plain"
      path = File.join(path, "objects", "info", file)
      send_file path
    else
      status 404
    end
  end

  def publish_git_objects_pack(path, prefix, suffix)
    # Validate prefix/suffix
    if prefix.match(/^[0-9a-f]{40}$/).nil? or suffix.match(/^pack$|^idx$/).nil?
      status 404
    else
      # Send packed object
      if suffix == "idx"
        content_type "application/x-git-packed-objects-toc"
      else
        content_type "application/x-git-packed-objects"
      end

      forever_cache
      path = File.join(path, "objects", "pack", prefix)
      send_file path
    end
  end

  def publish_objects_file(path, prefix, suffix)
    # Validate prefix/suffix
    if prefix.match(/^[0-9a-f]{2}$/).nil? or suffix.match(/^[0-9a-f]{38}$/).nil?
      status 404
    else
      system = Occam::System.first
      if not system.curate_git
        status 404 and return
      end

      # Send loose object
      forever_cache
      content_type "application/x-git-loose-object"
      path = File.join(path, "objects", prefix, suffix)
      send_file path
    end
  end

  # OCCAM Object git access

  # Returns the HEAD object (get_text_file)
  get '/objects/:uuid/HEAD' do
    status 404 and return if object.nil?

    publish_git_head(object.local_path)
  end

  # Returns the refs object (get_info_refs)
  get '/objects/:uuid/info/refs' do
    publish_git_info_refs(params[:uuid], "")
  end

  get '/objects/:uuid/objects/info/*' do |file|
    publish_git_objects_info_file(object.local_path, file)
  end

  get '/objects/:uuid/objects/pack/pack-*.*' do |prefix, suffix|
    status 404 and return if object.nil?

    publish_git_objects_pack(object.local_path, prefix, suffix)
  end

  get '/objects/:uuid/objects/*/*' do |prefix, suffix|
    status 404 and return if object.nil?

    publish_git_objects_file(object.local_path, prefix, suffix)
  end

  post '/objects/:uuid/git-upload-pack' do
    issue_git_service(params[:uuid], 'upload-pack')
  end

  # Retrieve the object metadata specifically as JSON
  get '/worksets/:workset_uuid/git/:uuid.json' do
    objectShow(params, "application/json")
  end
  get '/worksets/:workset_uuid/:workset_revision/git/:uuid/:revision.json' do
    objectShow(params, "application/json")
  end
  get '/resources/git/:uuid/:revision.json' do
    objectShow(params, "application/json")
  end
  get '/resources/git/:uuid.json' do
    objectShow(params, "application/json")
  end

  # Tabs
  get '/resources/git/:uuid/files/?' do
    params[:backed] = true
    params[:tab] = "files"
    objectShow(params)
  end
  get '/resources/git/:uuid/:backed_revision/files/?' do
    params[:backed] = true
    params[:tab] = "files"
    objectShow(params)
  end

  # Show object
  get '/worksets/:workset_uuid/git/:uuid/?' do
    params[:backed] = true
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/git/:uuid/:revision/?' do
    params[:backed] = true
    objectShow(params)
  end
  get '/resources/git/:uuid/?' do
    params[:backed] = true
    objectShow(params)
  end
  get '/resources/git/:uuid/:backed_revision/?' do
    params[:backed] = true
    objectShow(params)
  end

  # Retrieve the file from the git repository for the object
  get '/worksets/:workset_uuid/:workset_revision/git/:uuid/:revision/tree/*' do
    params[:backed] = true
    params[:tab] = "files"
    objectShow(params, nil, true)
  end
  get '/worksets/:workset_uuid/git/:uuid/tree/*' do
    params[:backed] = true
    params[:tab] = "files"
    objectShow(params, nil, true)
  end
  get '/resources/git/:uuid/:backed_revision/tree/*' do
    params[:backed] = true
    params[:tab] = "files"
    objectShow(params, nil, true)
  end
  get '/resources/git/:uuid/tree/*' do
    params[:backed] = true
    params[:tab] = "files"
    objectShow(params, nil, true)
  end

  # Retrieve the actual data from the git repository for the object
  get '/worksets/:workset_uuid/:workset_revision/git/:uuid/:revision/raw/*' do
    params[:backed] = true
    objectFile(params)
  end
  get '/worksets/:workset_uuid/git/:uuid/raw/*' do
    params[:backed] = true
    objectFile(params)
  end
  get '/resources/git/:uuid/:backed_revision/raw/*' do
    params[:backed] = true
    objectFile(params)
  end
  get '/resources/git/:uuid/raw/*' do
    params[:backed] = true
    objectFile(params)
  end

  # Retrieve the file info from the git repository for the object
  get '/worksets/:workset_uuid/:workset_revision/git/:uuid/:revision/stat/*' do
    params[:backed] = true
    objectFileStat(params)
  end
  get '/worksets/:workset_uuid/git/:uuid/stat/*' do
    params[:backed] = true
    objectFileStat(params)
  end
  get '/resources/git/:uuid/:backed_revision/stat/*' do
    params[:backed] = true
    objectFileStat(params)
  end
  get '/resources/git/:uuid/stat/*' do
    params[:backed] = true
    objectFileStat(params)
  end

  # Uploads a file to an object
  post '/worksets/:workset_uuid/:workset_revision/git/:uuid/:revision/tree/*' do
    params[:backed] = true
    objectUpload(params)
  end

  # Returns the HEAD object (get_text_file)
  get '/resources/git/:uuid/?:backed_revision?/HEAD' do
    params[:backed] = true
    publish_git_head(path)
  end

  # Returns the refs object (get_info_refs)
  get '/resources/git/:uuid/?:backed_revision?/info/refs' do
    params[:backed] = true
    publish_git_info_refs(params[:uuid], "")
  end

  get '/resources/git/:uuid/?:backed_revision?/objects/info/*' do |file|
    params[:backed] = true
    publish_git_objects_info_file(path, file)
  end

  get '/resources/git/:uuid/?:backed_revision?/objects/pack/pack-*.*' do |prefix, suffix|
    params[:backed] = true
    publish_git_objects_pack(path, prefix, suffix)
  end

  get '/resources/git/:uuid/?:backed_revision?/objects/*/*' do |prefix, suffix|
    params[:backed] = true
    publish_git_objects_file(path, prefix, suffix)
  end

  post '/resources/git/:uuid/?:backed_revision?/git-upload-pack' do
    params[:backed] = true
    issue_git_service(params[:uuid], 'upload-pack')
  end

  # Get the occam source code

  # Updates the install.sh to point to this node
  # when the installer is retrieved. That way code
  # is pulled from the node the OCCAM installer is
  # pulled from. And the new node will automatically
  # discover this node.
  def updateOccamInstaller
    # Get our domain
    host = request.host
    port = request.port
    scheme = request.scheme

    url = "#{scheme}://#{host}"
    origin = "#{host}:#{port}"

    if scheme == "http" && port != 80
      url = "#{url}:#{port}"
    elsif scheme == "https" && port != 443
      url = "#{url}:#{port}"
    end

    uuid = '5148417c-528d-11e5-bcf7-dc85debcef4e'

    object = Occam::Object.where(:uid => uuid).first()
    # TODO: backend
    if object.nil?
      occam_install_path = File.join(File.dirname(__FILE__), "..", "occam-install")
      occam_install_sh_path = File.join(occam_install_path, 'install.sh')
      occam_install_tmp_path = File.join(occam_install_path, 'install.tmp')
      File.open(occam_install_sh_path, 'w+') do |new_script|
        new_script.write("ORIGIN=#{origin}\n")
        new_script.write("SCHEME=#{scheme}\n")
        new_script.write("\n")

        File.open(occam_install_tmp_path) do |tmp_script|
          new_script.write(tmp_script.read)
        end
      end
      puts "Building occam-install"

      # Commit and store
      Open3.popen3('git', 'init', :chdir => occam_install_path) do |i, o, e, t|
      end

      Open3.popen3('git', 'add', '*', :chdir => occam_install_path) do |i, o, e, t|
      end

      Open3.popen3('git', 'add', 'install.sh', '-f', :chdir => occam_install_path) do |i, o, e, t|
      end

      Open3.popen3('git', 'commit', '-am', 'Updates origin', :chdir => occam_install_path) do |i, o, e, t|
      end

      Open3.popen3('occam', 'pull', '.', :chdir => occam_install_path) do |i, o, e, t|
      end
    end

    puts "Done #{uuid}"

    uuid
  end

  get '/occam/info/refs' do
    uuid = updateOccamInstaller
    publish_git_info_refs(uuid, "")
  end

  post '/occam/git-upload-pack' do
    uuid = updateOccamInstaller
    issue_git_service(uuid, 'upload-pack')
  end
end
