/* This file implements any dynamic auto-complete feature across the site.
 * The autocomplete dropdown shares the dropdown styling of Occam.Selector.
 * Generally, autocomplete is used for object lookup and account/person lookup.
 *
 * To mark an input as an object autocomplete:
 * - Apply class 'auto-complete' to the input.
 * - Using 'object-type' class will query for object types.
 * - Use the following attributes to build queries:
 *    data-object-type: a string for the object type to filter
 * - You can have a object-type filter input by placing an input.auto-complete.object-type as a sibling
 */

// Initialize any autocomplete input fields
$(function() {
  var autocompletes = $('.auto-complete');

  autocompletes.each(function() {
    var autocomplete = new Occam.AutoComplete($(this));
  });
});

var initOccamAutoComplete = function(Occam) {
  var AutoComplete = Occam.AutoComplete = function(element) {
    this.element = element;
    this.objectTypeSelector = null;

    var typeSelector = this.element.parent().find('.auto-complete.object-type');

    if (typeSelector.length > 0) {
      this.objectTypeSelector = typeSelector;
    }

    var viewsTypeSelector = this.element.parent().find('.auto-complete.views-type');

    if (viewsTypeSelector.length > 0) {
      this.objectViewsTypeSelector = viewsTypeSelector;
    }

    var providesEnvironmentSelector = this.element.parent().find('.auto-complete.provides-environment');

    if (providesEnvironmentSelector.length > 0) {
      this.objectProvidesEnvironmentSelector = providesEnvironmentSelector;
    }

    var providesArchitectureSelector = this.element.parent().find('.auto-complete.provides-architecture');

    if (providesArchitectureSelector.length > 0) {
      this.objectProvidesArchitectureSelector = providesArchitectureSelector;
    }

    this.initialize();
    this.bindEvents();
  };

  /* This function will create the dropdown and attach it to the body.
   */
  AutoComplete.prototype.initialize = function() {
    // Form dropdown section
    this.dropdown = $('<ul class="dropdown" tabindex="1"></ul>');

    // Append dropdown to body
    $('body .content').append(this.dropdown);

    // Form hidden form component for the object id
    var existing = this.element.parent().children("input.hidden[name=object-id]");
    if (existing.length == 0) {
      this.hidden = $('<input type="hidden" class="hidden" aria-hidden="true" name="object-id"></input>');
      this.element.parent().append(this.hidden);
    }
    else {
      this.hidden = existing;
    }
  };

  /* This function issues a search to fill the dropdown with possible objects.
   */
  AutoComplete.prototype.fillDropdown = function() {
    console.log("filldropdown");
    var self = this;

    var dropdownItem = $('<li class="object"></li>');

    var url = '/search';
    var inputElement = this.element;
    var input_type = inputElement.val();

    var types_toggle   = "off";
    var objects_toggle = "on";

    if ($(this).hasClass('object-type')) {
      objects_toggle = "off";
      types_toggle   = "on";
    }

    var obj_type     = this.element.data('object-type');
    var views_type   = this.element.data('views-type');
    var environment  = this.element.data('environment');
    var architecture = this.element.data('architecture');

    // Allow filtering of object types using a sibling input object-type field
    if (self.objectTypeSelector) {
      obj_type = self.objectTypeSelector.val();
    }

    if (self.objectViewsTypeSelector) {
      views_type = self.objectViewsTypeSelector.val();
    }

    if (self.objectProvidesEnvironmentSelector) {
      environment = self.objectProvidesEnvironmentSelector.val();
    }

    if (self.objectProvidesArchitectureSelector) {
      architecture = self.objectProvidesArchitectureSelector.val();
    }

    $.post(url, {
      "search":  input_type,
      "type":    obj_type,
      "views":   views_type,
      "objects": objects_toggle,
      "types":   types_toggle,
      "environment": environment,
      "architecture": architecture
    }, function(data) {
      self.dropdown.children().remove();

      var item = dropdownItem.clone();

      if (types_toggle == "on") {
        data["types"].forEach(function(object) {
          var item = dropdownItem.clone();

          var header = obj_type;
          header = object["object_type"];
          item.append($('<h2 class="type"></h2>'));
          item.children('h2').text(header);
          item.children('h2').addClass('large-icon').addClass(object["object_type"]);

          item.on('mousedown', function(event) {
            /* Set fields to reflect choice */
            self.element.val(object["object_type"]);
          });

          self.dropdown.append(item);

          // Ensure the dropdown is at least a certain size
          self.dropdown.stop(true);
          var dropdownHeight = item.outerHeight(true) * self.dropdown.children('li').length;
          console.log("setting height to " + dropdownHeight);
          self.dropdown.stop(true).css({
            "height": dropdownHeight + "px"
          });
        });
      }
      else {
        data["objects"].slice(0,25).forEach(function(object) {
          var item = dropdownItem.clone();

          var header = obj_type;
          header = object["object_type"];
          if (obj_type === "person") {
            if ("organization" in object) {
              header = object["organization"];
            }
            else {
              header = null;
            }
          }

          if (header !== null) {
            item.append($('<h2 class="type"></h2>'));
            item.children('h2').text(header);
            item.append($('<p></p>'));
            item.children('p').text(object["name"]);
          }
          else {
            item.append($('<h2 class="type"></h2>'));
            item.children('h2').text(object["name"]);
          }

          var icon = null;
          if (obj_type === "person") {
            icon = "/people/" + object["uid"] + "/avatar?size=20";
          }
          if (icon !== null) {
            item.children('h2').css({
              "background-image": "url('" + icon + "')"
            });
          }
          else {
            item.children('h2').addClass('large-icon').addClass(object["object_type"]);
          }

          item.on('mousedown', function(event) {
            /* Set fields to reflect choice */
            self.element.val(object["name"]);
            self.hidden.val(object["uid"]);
            self.element.css({
              "background-image": $(this).children('h2').css('background-image')
            });
          });

          self.dropdown.append(item);

          // Ensure the dropdown is at least a certain size
          self.dropdown.stop(true);

          var dropdownHeight = item.outerHeight(true) * self.dropdown.children('li').length;
          self.dropdown.stop(true).css({
            "height": dropdownHeight + "px"
          });
        });
      }
    }, 'json');
  };

  /* This function will attack the change events that will perform the
   * queries and update/show the dropdown.
   */
  AutoComplete.prototype.bindEvents = function() {
    var self = this;

    this.element
        .on('focus.autocomplete', function() {
          self.dropdown.css({
            "width": $(this).width() + 70
          }).on('blur', function(event) {
          });

          var offset = self.element.offset();

          console.log("setting height to " + dropdownHeight);
          self.dropdown.attr("aria-hidden", "false").css({
            "left": offset.left + "px",
            "top":  (offset.top + self.element.height() + 20) + "px",
            "display": "block",
            "height": "0px"
          });

          var dropdownHeight = self.dropdown.children('li').first().outerHeight(true) *
                               self.dropdown.children('li').length;

          self.dropdown.animate({
            "height": dropdownHeight + "px"
          }, 200);

          self.fillDropdown();
        })
        .on('keyup.autocomplete', function(event) {
          self.fillDropdown();
        })
        .on('blur.autocomplete', function(event) {
          self.dropdown.hide();
          event.stopPropagation();
          event.preventDefault();
        });
  };
};
