# This module represents the routes for Virtual Machine creation.

class Occam
  # This route will yield a task object for the given backend set and the
  # given object metadata. It only needs the object's build/run and environment
  # and architecture information to craft the possible VM object.
  get '/task' do
    # A target environment and architecture
    toEnvironment  = params["toEnvironment"]
    toArchitecture = params["toArchitecture"]

    # The object metadata the node desires to run
    fromEnvironment  = params["fromEnvironment"]
    fromArchitecture = params["fromArchitecture"]

    puts "Determining task for #{fromEnvironment}/#{fromArchitecture} to #{toEnvironment}/#{toArchitecture}"

    if fromEnvironment.blank? or fromArchitecture.blank? or toEnvironment.blank? or toArchitecture.blank?
      status 404
      return
    end

    content_type "application/json"

    # Fire off a run command to generate a task object and return that
    command = "manifest --base64 --environment #{Base64.strict_encode64(toEnvironment)} --architecture #{Base64.strict_encode64(toArchitecture)} --target-environment #{Base64.strict_encode64(fromEnvironment)} --target-architecture #{Base64.strict_encode64(fromArchitecture)}"

    # We will return the task manifest
    Occam::Worker.perform(command)
  end
end
