class Occam
  module SessionHelpers
    def current_account
      if current_person
        current_person.account
      else
        nil
      end
    end

    def current_person
      if session[:person_id]
        person = Person.find_by(:id => session[:person_id])
        if person.active_session_for?(session[:nonce])
          @current_person ||= person
        end
      end
    end

    def logged_in?
      !!current_person
    end
  end

  helpers SessionHelpers
end
