# Object Metadata

The `object.json` file contains various meta-data for an occam object, which can be a simulator, benchmark, trace, etc.
Excluding the source code location, much of this information is not relevant for object usage.
It is for properly citing the origins of the object and its authors.
This metadata can be automatically augmented by other OCCAM nodes to list mirrors such that OCCAM nodes can act as repositories for all source code and other artifacts associated with this object.

Here is an example `object.json` file for [DRAMSim2](https://wiki.umd.edu/DRAMSim2) which you can also find in our [code repository](https://bitbucket.org/occam/occam-dramsim2/src):

```
{
  "type": "simulator",
  "name": "DRAMSim2",

  "install": {
    "git": "git://github.com/dramninjasUMD/DRAMSim2.git"
  },

  "inputs": [
    {
      "type":  "trace",
      "group": "DRAMSim2"
    },
    {
      "type": "benchmark"
    }
  ],

  "build": {
    "using":    "ubuntu-base-2014-06",
    "script":   "build.sh",
    "language": "bash"
  },

  "run": {
    "script":   "launch.py",
    "language": "python",
    "version":  "3.3.0"
  },

  "website": "https://wiki.umd.edu/DRAMSim2",

  "description": "**DRAMSim** is a cycle accurate model of a DRAM memory controller, the DRAM
                  modules which comprise system storage, and the bus by which they communicate.
                  All major components in a modern memory system are modeled as their own
                  respective objects within the source, including: ranks, banks, command queue,
                  the memory controller, etc.\n\nThe overarching goal is to have a simulator
                  that is extremely small, portable, and accurate. The simulator core has a
                  well-defined interface which allows it to be CPU simulator agnostic and
                  should be easily modifiably to work with any simulator.  This core has no
                  external run time or build time dependencies and has been tested with g++ on
                  Linux as well as g++ on Cygwin on Windows.",

  "authors": ["Rosenfeld, P.", "Cooper-Balis, E.", "Jacob, B."],
  "organization": "University of Maryland, College Park",

  "license": "BSD",

  "tags": ["memory", "DRAM", "DDR2", "DDR3", "trace-based", "memory cards",
           "memory architecture", "DRAM architecture"],

  "citation": {
    "author": "Rosenfeld, P. and Cooper-Balis, E. and Jacob, B.",
    "journal": "Computer Architecture Letters",
    "title": "DRAMSim2: A Cycle Accurate Memory System Simulator",
    "year": "2011",
    "month": "jan.-june",
    "volume": "10",
    "number": "1",
    "pages": "16-19",
    "keywords": "DDR2/3 memory system model;DRAMSim2 simulation;DRAMSim2 timing;Verilog model;
                 cycle accurate memory system simulator;trace-based simulation;visualization tool;
                 DRAM chips;memory architecture;memory cards;",
    "doi": "10.1109/L-CA.2011.4",
    "ISSN": "1556-6056"
  }
}
```

## Type

The type of object that is being represented. Some standard object types are:

* simulator
* benchmark

## Authors

An array of free-form strings containing names of authors.
The actual formatting of the names is up to the author.

```
"authors": ["Rosenfeld, P.", "Cooper-Balis, E.", "Jacob, B."]
```

## Citation

A dictionary of citation parameters.
From this OCCAM can generate bibtex, etc formulations of citation meta-data to encourage simulator citation.

```
"citation": {
  "author": "Rosenfeld, P. and Cooper-Balis, E. and Jacob, B.",
  "journal": "Computer Architecture Letters",
  "title": "DRAMSim2: A Cycle Accurate Memory System Simulator",
  "year": "2011",
  "month": "jan.-june",
  "volume": "10",
  "number": "1",
  "pages": "16-19",
  "keywords": "DDR2/3 memory system model;DRAMSim2 simulation;DRAMSim2 timing;Verilog model;
               cycle accurate memory system simulator;trace-based simulation;visualization tool;
               DRAM chips;memory architecture;memory cards;",
  "doi": "10.1109/L-CA.2011.4",
  "ISSN": "1556-6056"
}
```

## Description

A long-form description of what the object does and what it is used for.
Markdown can be used to minimally style text.

```
"description": "**DRAMSim** is a cycle accurate model of a DRAM memory controller, the DRAM
                modules which comprise system storage, and the bus by which they communicate.
                All major components in a modern memory system are modeled as their own
                respective objects within the source, including: ranks, banks, command queue,
                the memory controller, etc.\n\nThe overarching goal is to have a simulator
                that is extremely small, portable, and accurate. The simulator core has a
                well-defined interface which allows it to be CPU simulator agnostic and
                should be easily modifiably to work with any simulator.  This core has no
                external run time or build time dependencies and has been tested with g++ on
                Linux as well as g++ on Cygwin on Windows."
```

In the above, markdown rules are used to bold the term `DRAMSim` and two newlines near the middle
demarcate two paragraphs. For a basic rundown of markdown, [refer to this](https://daringfireball.net/projects/markdown/basics).

## Install

Within the install field is all information about the location of objects related to the retrieval of the object and all assets. This includes and mirrors. OCCAM nodes may add mirrors if such nodes discover mirrors or mirror the object itself.

### Git

The canonical URL to a public git repository that will provide the code for the object.
This will serve as the default place to acquire the newest source or source history.
The build scripts *may* be able to build different versions of the object, therefore
a git repository is preferred over a tar file for the purpose of reproducibility.

```
"install": {
  "git": "git://github.com/dramninjasUMD/DRAMSim2.git"
}
```

### Mirrors

This object is optional and contains one or more non-canonical mirrors for object retrival. It contains any field normally allowed in an install block (git, hg, svn, tar) but with an array of strings for each mirror known.

OCCAM nodes may add their own entries to the mirrors section. OCCAM nodes should not consider the mirrors section to be part of the canonical object for the sake of equivalence.

```
"install": {
  "git": "git://github.com/dramninjasUMD/DRAMSim2.git",
  "mirrors": {
    "git": ["https://occam.cs.pitt.edu/git/DRAMSim2.git",
            "https://occam.cs.cmu.edu/git/DRAMSim2.git"],
    "tar": ["https://occam.cs.pitt.edu/tar/DRAMSim2.git.tar.gz"]
  }
}
```

## Build

This section of metadata describes the process of building the object once scripts have been retrieved and packages within the install section have been downloaded.

### Using

The using field specifies the base object to use when building this object. You can specify the name of an existing object, or, more likely, the name of an OCCAM base object.

```
"build": {
  "using":    "ubuntu-base-2014-06",
  "script":   "build.sh",
  "language": "bash"
}
```

In the case above, the base image `ubuntu-base-2014-06` is used. The base name gives you an indication of the environment to expect. In this example, this is Ubuntu from June 2014. This environment is static. It will never change, and will be maintained. OCCAM will release new base images in this same style every 6 months which will upgrade the libraries and kernel.

### Script

This field specifies the script to run that will start the build. This script should be included along with the object.

### Language

```
"build": {
  "using":    "ubuntu-base-2014-06",
  "script":   "build.py",
  "language": "python",
  "version":  "3.3.0"
}
```

This field specifies the language of the interpreter required to run the given build script. In the example above, it is python 3.3.0. The environment knows to install and build and use this particular version of python to run the script given.

### Version

This field specifies the version of the interpreter given by `language` that will run the given build script given by `script`. In the example listed under `language` above, it would run python version 3.3.0.

## License

A free-form string containing the software license the object uses.
If a object is dual-licensed, the most permissive license should be placed here.

```
"license": "BSD"
```

## Name

The canonical name of the object which is how it will be displayed to people within OCCAM.

```
"name": "DRAMSim2"
```

## Organization

A free-form string containing the organization that develops or maintains the object.

```
"organization": "University of Maryland, College Park"
```

## Tags

An array of tags used to better identify and curate objects.

```
"tags": ["memory", "DRAM", "DDR2", "DDR3", "trace-based", "memory cards", "memory architecture", "DRAM architecture"]
```

## Website

The `URL` to the main website for the development and community of the object.

```
"website": "https://wiki.umd.edu/DRAMSim2"
```
