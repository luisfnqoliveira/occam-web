require_relative "helper"
require_model "object"

describe Occam::Object do
  describe "#initialize" do
  end

  describe "::download_json" do
    before do
      request = mock('Net::HTTP::Request')
      request.stubs(:[]=)
      request.stubs(:content_type=)

      Net::HTTP::Get.stubs(:new).returns(request)

      @response = mock('Net::HTTP::Response')
      @response.stubs(:is_a?).returns(false)
      @response.stubs(:[]).returns(nil)

      http = mock('Net::HTTP')
      http.stubs(:use_ssl)
      http.stubs(:verify_mode)
      http.stubs(:request).returns(@response)

      Net::HTTP.stubs(:new).returns(http)
    end

    it "should return json when we find json" do
      @response.stubs(:is_a?).with(Net::HTTPSuccess).returns(true)
      @response.stubs(:content_type).returns('application/json')
      @response.stubs(:body).returns("{}")

      JSON.expects(:parse).with("{}").returns({})

      Occam::Object.download_json('http://example.org')
    end

    it "should redirect when told to" do
      @response.stubs(:is_a?).with(Net::HTTPRedirection).returns(true)
      @response.stubs(:[]).with('location').returns('http://example.org/somewhere/else')

      class << Occam::Object
        alias_method :__download_json, :download_json
      end

      Occam::Object.expects(:download_json).with('http://example.org/somewhere/else', 'application/json', 9).returns({})

      Occam::Object.__download_json('http://example.org')
    end
  end

  describe "::all_unknown_dependencies" do
    it "should work" do
      download_json = {
      }
      Occam::Object.stubs(:download_json).returns(download_json)
      Occam::Object.all_unknown_dependencies("http://example.org")
    end
  end
end
