/* This file handles the javascript for the object/workset directory. This
 * handles the expansions and dynamic loading of group/experiment listings.
 */

// Initialize any directory views
$(function() {
  Occam.Directory.bind($('body'));
});

var initOccamDirectory = function(Occam) {
  var Directory = Occam.Directory = function(element) {
    this.element = element;

    this.initialize();
    this.bindEvents();
  };

  Directory.bind = function(element) {
    var directories = element.find('.directory.information');

    directories.each(function() {
      var directory = new Occam.Directory($(this));
    });
  };

  Directory.prototype.initialize = function() {
    // Add triangles to all relevant rows that may expand
    this.element.find('tr:not(.add):not(.header) td.expand')
      .css({"cursor": "pointer"})
      .text('\u25b8');
    this.element.find('tr:not(.add):not(.header) td')
      .css({"cursor": "pointer"});
    this.element.find('tr:not(.add):not(.header) td a')
      .on('click', function(e) { e.stopPropagation(); });
    this.element.find('tr:not(.add):not(.header) td input')
      .on('click', function(e) { e.stopPropagation(); });
  };

  Directory.prototype.bindEvents = function() {
    // Bind event to expand hidden sections of the Directory
    this.element.find('tr:not(.add):not(.header) td')
      .on('click', Directory.expandGroup);

    // Bind and initialize workset rows
    this.bindWorksetEvents();

    // Bind and initialize group rows
    this.bindGroupEvents();

    // Bind and initialize experiment rows
    this.bindExperimentEvents();
  };

  Directory.prototype.bindWorksetEvents = function() {
    var worksets = this.element.find('.workset');

    // Add a new table row under each workset that will initially be hidden
    worksets.each(function() {
      var workset = $(this);

      var new_table = $('<table></table>')
        .addClass('directory information');

      var table_row = $('<tr class="row tree"></tr>')
        .attr('aria-hidden', 'true');

      table_row.append($('<td colspan="999"></td>').append(new_table));
      workset.after(table_row);
    });
  };

  /* This function will bind events and initialize rows in the directory
   * that represent Group objects.
   */
  Directory.prototype.bindGroupEvents = function() {
    var groups = this.element.find('.group');

    // Add a new table row under each group that will initially be hidden
    groups.each(function() {
      var group = $(this);

      var new_table = $('<table></table>')
        .addClass('directory information');

      var table_row = $('<tr class="row tree"></tr>')
        .attr('aria-hidden', 'true');

      table_row.append($('<td colspan="999"></td>').append(new_table));
      group.after(table_row);
    });
  };

  /* This function will bind events and initialize rows in the directory
   * that represent Experiment objects.
   */
  Directory.prototype.bindExperimentEvents = function() {
    var experiments = this.element.find('.experiment');

    // Add a new table row under each group that will initially be hidden
    experiments.each(function() {
      var experiment = $(this);

      var new_table = $('<table></table>')
        .addClass('directory information');

      var table_row = $('<tr class="row tree"></tr>')
        .attr('aria-hidden', 'true');

      table_row.append($('<td colspan="999"></td>').append(new_table));
      experiment.after(table_row);
    });
  };

  Directory.expandGroup = function(event) {
    var span = $(this).parent().children('td.expand');
    if (span.length === 0) {
      return;
    }

    span.toggleClass('shown');
    // Get associated list
    var group = span.parent();
    var group_container = span.parent().next();
    var group_list = span.parent().next().children('td').children('table');
    var directory = span.parents('table.directory.base');

    if (span.hasClass('shown')) {
      // Pull in group list, if there are no children
      if (group_list.children('tbody').children('.row').length == 0) {
        var group_id         = group.data('group-uuid');
        var workset_id       = group.data('workset-uuid');
        var group_revision   = group.data('group-revision');
        var workset_revision = group.data('workset-revision');

        if (group.hasClass('workset')) {
          $.getJSON('/worksets/' + workset_id + '/' + workset_revision, function(data) {
            data.contains.forEach(function(dependency) {
              if (dependency.type == "group") {
                var item = group.clone();
                item.removeClass('workset').addClass('group');
                item.find('td.expand').removeClass('shown').text("\u25b8");
                item.find('td').on('click', Directory.expandGroup);
                item.find('a').on('click', function(e) { e.stopPropagation(); });
                item.data('group-uuid', dependency.id);
                item.data('group-revision', dependency.revision);

                var url = "/worksets/" + workset_id + '/' + workset_revision;

                item.find('td.name span').text(dependency.name);
                item.find('td.name a').text(dependency.name).attr('href', url + '/groups/' + dependency.id + '/' + dependency.revision);

                // Workaround a weird jquery bug where it preserves the height (of 0) of a hidden element
                // when showing/hiding.
                item.children('td.name').children('table').css({'height': 'auto'});

                group_list.append(item);

                /* Add sub list */
                item = group.next().clone();
                item.children('td').children('table').children().remove();
                item.hide();
                group_list.append(item);
              }
            });
            data.contains.forEach(function(dependency) {
              if (dependency.type == "experiment") {
                var item = group.clone();
                item.removeClass('workset').addClass('experiment');
                item.find('td.expand').removeClass('shown').text("\u25b8").on('click', Directory.expandGroup);
                item.find('a').on('click', function(e) { e.stopPropagation(); });
                item.data('group-uuid', dependency.id);
                item.data('group-revision', dependency.revision);

                var url = "/worksets/" + workset_id + '/' + workset_revision;

                item.find('td.name span').text(dependency.name);
                item.find('td.name a').text(dependency.name).attr('href', url + '/experiments/' + dependency.id + '/' + dependency.revision);

                // Workaround a weird jquery bug where it preserves the height (of 0) of a hidden element
                // when showing/hiding.
                item.children('td.name').children('table').css({'height': 'auto'});

                group_list.append(item);

                /* Add sub list */
                item = group.next().clone();
                item.children('td').children('table').children().remove();
                item.hide();
                group_list.append(item);
              }
            });
          });
        }
        else if (group.hasClass('group')) {
          $.getJSON('/worksets/' + workset_id + '/' + workset_revision + '/groups/' + group_id + '/' + group_revision, function(data) {
            data.contains.forEach(function(dependency) {
              if (dependency.type == "group") {
                var item = group.clone();
                item.find('td.expand').removeClass('shown').text("\u25b8");
                item.find('td').on('click', Directory.expandGroup);
                item.find('a').on('click', function(e) { e.stopPropagation(); });
                item.data('group-uuid', dependency.id);
                item.data('group-revision', dependency.revision);

                var url = "/worksets/" + workset_id + '/' + workset_revision;

                item.find('td.name a').text(dependency.name).attr('href', url + '/groups/' + dependency.id + '/' + dependency.revision);

                // Workaround a weird jquery bug where it preserves the height (of 0) of a hidden element
                // when showing/hiding.
                item.children('td.name').children('table').css({'height': 'auto'});

                group_list.append(item);

                /* Add sub list */
                item = group.next().clone();
                item.children('td').children('table').children().remove();
                item.hide();
                group_list.append(item);
              }
            });
            data.contains.forEach(function(dependency) {
              if (dependency.type == "experiment") {
                var item = group.clone();
                item.removeClass('group').addClass('experiment');
                item.find('td.expand').removeClass('shown').text("\u25b8");
                item.find('td').on('click', Directory.expandGroup);
                item.find('a').on('click', function(e) { e.stopPropagation(); });
                item.data('group-uuid', dependency.id);
                item.data('group-revision', dependency.revision);

                var url = "/worksets/" + workset_id + '/' + workset_revision;

                item.find('td.name span').text(dependency.name);
                item.find('td.name a').text(dependency.name).attr('href', url + '/experiments/' + dependency.id + '/' + dependency.revision);

                // Workaround a weird jquery bug where it preserves the height (of 0) of a hidden element
                // when showing/hiding.
                item.children('td.name').children('table').css({'height': 'auto'});

                group_list.append(item);

                /* Add sub list */
                item = group.next().clone();
                item.children('td').children('table').children().remove();
                item.hide();
                group_list.append(item);
              }
            });
          });
        }
        else if (group.hasClass('experiment')) {
          $.getJSON('/objects/' + group_id + '/invocations/output', function(data) {
            if (group_revision in data) {
              data[group_revision].forEach(function(output) {
                var dependency = output.object;
                if (dependency && dependency.type == "application/json") {
                  var item = group.clone();
                  item.removeClass('experiment').addClass('output').addClass('newTab');

                  item.find('td.expand').remove();
                  item.find('td').css({'cursor': 'pointer'}).on('click', Directory.addOutput);
                  item.find('a').on('click', function(e) { e.stopPropagation(); });

                  item.data('group-uuid', dependency.id);
                  item.data('group-revision', dependency.revision);

                  var url = "/objects/" +  dependency.id + '/' + dependency.revision;

                  item.find('td.name span').text(dependency.name);
                  item.find('td.name a').text(dependency.name).attr('href', url);

                  // Workaround a weird jquery bug where it preserves the height (of 0) of a hidden element
                  // when showing/hiding.
                  item.children('td.name').children('table').css({'height': 'auto'});

                  group_list.append(item);
                }
              });
            }
          });
        }
      }
      group_container.slideDown(150);
      group_container.attr('aria-hidden', 'false');
      span.text("\u25be");
    }
    else {
      group_container.slideUp(150);
      group_container.attr('aria-hidden', 'true');
      span.text("\u25b8");
    }
    event.stopPropagation();
    event.preventDefault();
  };

  Directory.addOutput = function(event) {
    event.stopPropagation();
    event.preventDefault();

    var span = $(this);

    var object = span.parent();
    var objectName = object.children('.name').children('span').text();

    var directory = span.parents('table.directory.base');
    var outputTabs = directory.parents('ul.tab-panels.outputs').prev();
    var configTabs = outputTabs.parents('ul.tab-panels').first().children('li.tab-panel.configuration');

    // Add new tab with output data
    var tabStrip = new Occam.Tabs(outputTabs);
    tabStrip.addTab(objectName, function(panel) {
      panel.addClass('results-data');
      panel.data('object-id', object.data('group-uuid'));
      panel.data('object-revision', object.data('group-revision'));

      var output = new Occam.DataViewer(panel);

      // For each configuration tab... add datapoints
      configTabs.each(function() {
        var configuration = Occam.Configuration.load($(this));
        configuration.dataPoints().each(function() {
          var dataPoint = $(this);
          var labels = configuration.dataPointLabelFor(dataPoint);
          var dataLabel = labels[0];
          var dataKey   = labels[1];

          output.addTarget(dataLabel, dataKey, configuration, dataPoint);
        });
      });

      output.load(function() {
        // Output pane has rendered the data
      });
    });
  };
};
