require_relative 'helper'

describe "signup", :type => :feature do
  before :each do
  end

  it "should sign me up when I specify a valid username/password" do
    visit '/people/new'

    fill_in 'username', :with => 'wilkie'
    fill_in 'password', :with => 'foobar'

    click_button 'signup'
    current_path.must_match /^\/people\/[^\/]+$/
  end
end
