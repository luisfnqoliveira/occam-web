require_relative 'helper'

describe Occam do
  describe "Worksets Controller" do
    def create_owned_workset(options={}, person=nil, collaborator=nil)
      data = {
        :name => "foo",
        :uid  => "aaaa1234567890-1234-2345-34567890",
        :revision => "12345678abcdefg",
        :tags => ";;",
      }.merge!(options)

      workset = Occam::Workset.create(data)

      Occam::Authorship.create(:workset => workset,
                               :person  => person)

      info = {}
      data.each do |k, v|
        if k == :uid
          k = :id
        elsif k == :tags
          v = (v or "").to_s.split(';')[1..-1] || []
        end
        info[k.to_s] = v
      end

      info["authors"] = [person.uid]

      if collaborator
        Occam::Collaboratorship.create(:workset => workset,
                                       :person  => collaborator)

        info["collaborators"] = [collaborator.uid]
      end

      workset.stubs(:objectInfo).returns(info)
      workset.stubs(:fullRevision).returns(data[:revision])
      workset.stubs(:parentRevision).returns(data[:revision])
      workset.stubs(:childRevisions).returns([data[:revision]])

      Occam::Workset.stubs(:find_by).with(:uid => workset.uid).returns(workset)
      Occam::Workset.stubs(:where).with(:uid => workset.uid).returns([workset])

      workset
    end

    before do
      Occam.any_instance.stubs(:markdown)
      Occam.any_instance.stubs(:render)

      Occam::Workset.any_instance.stubs(:retrieveJSON).returns({})
      Occam::Workset.any_instance.stubs(:retrieveFile).returns("")
    end

    describe "POST /worksets" do
      before do
        @uid = "abcd1234567890-1234-2345-34567890"
        Occam::Person.any_instance.stubs(:addWorkset).returns(@uid)
      end

      it "should return 404 if user is not logged in" do
        post '/worksets', { }

        last_response.status.must_equal 404
      end

      it "should redirect to workset upon success" do
        login_as('wilkie')

        post '/worksets', {
          "name" => 'foo'
        }

        last_response.location.must_equal(
          "http://example.org/worksets/#{@uid}"
        )
      end

      it "should return 302 upon success" do
        login_as('wilkie')

        post '/worksets', {
          "name" => 'foo'
        }

        last_response.status.must_equal 302
      end
    end

    describe "GET /worksets/tags" do
      it "should return a json content type" do
        get '/worksets/tags'

        content_type.must_match /application\/json/
      end

      it "should return json containing the tags" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        workset = create_owned_workset({
          :name => "foo",
          :tags => ";a;b;",
          :uid  => "aaaa1234567890-1234-2345-34567890",
        }, a)
        workset = create_owned_workset({
          :name => "foo",
          :tags => ";a;c;",
          :uid  => "bbbb1234567890-1234-2345-34567890",
        }, a)
        workset = create_owned_workset({
          :name => "foo",
          :tags => ";b;c;",
          :uid  => "cccc1234567890-1234-2345-34567890",
        }, a)

        get '/worksets/tags'

        JSON::parse(last_response.body).must_include "a"
      end

      it "should return json containing the tags matching given term" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        workset = create_owned_workset({
          :name => "foo",
          :tags => ";apple;",
          :uid  => "aaaa1234567890-1234-2345-34567890",
        }, a)
        workset = create_owned_workset({
          :name => "foo",
          :tags => ";banana;",
          :uid  => "bbbb1234567890-1234-2345-34567890",
        }, a)
        workset = create_owned_workset({
          :name => "foo",
          :tags => ";snapple;",
          :uid  => "cccc1234567890-1234-2345-34567890",
        }, a)

        get '/worksets/tags?term=app'

        JSON::parse(last_response.body).must_include "snapple"
      end

      it "should return json not containing tags which do not match given term" do
        Occam::Workset.create(:name => "foo", :tags => ";apple;")
        Occam::Workset.create(:name => "bar", :tags => ";banana;")
        Occam::Workset.create(:name => "baz", :tags => ";snapple;")

        get '/worksets/tags?term=app'

        JSON::parse(last_response.body).wont_include "banana"
      end
    end

    describe "GET /worksets/:uuid/fork" do
    end

    describe "POST /worksets/:uuid" do
      it "should return 404 when workset is not found" do
        Occam.any_instance.stubs(:markdown)

        post '/worksets/asdf', {}

        last_response.status.must_equal 404
      end

      it "should return 404 if the logged in user isn't owner/collaborator" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        workset = Occam::Workset.create(:name => "foo",
                                        :uid  => "abcd1234567890-1234-2345-34567890")
        authorship = Occam::Authorship.create(:workset => workset,
                                              :person  => a)

        login_as('jane')

        post "/worksets/#{workset.uid}", {
          "name" => "bar"
        }

        last_response.status.must_equal 404
      end

      it "should return 404 if nobody is logged in" do
        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        workset = Occam::Workset.create(:name => "foo",
                                        :uid  => "abcd1234567890-1234-2345-34567890")
        authorship = Occam::Authorship.create(:workset => workset,
                                              :person  => a)

        post "/worksets/#{workset.uid}", {
          "name" => "bar"
        }

        last_response.status.must_equal 404
      end

      it "should return 302 when successful" do
        a = login_as('wilkie')

        workset = create_owned_workset({
          :name => "foo"
        }, a)

        post "/worksets/#{workset.uid}", {
          "name" => "bar"
        }

        last_response.status.must_equal 302
      end

      it "should redirect to workset when successful" do
        a = login_as('wilkie')

        workset = create_owned_workset({
          :name => "foo"
        }, a)

        post "/worksets/#{workset.uid}", {
          "name" => "bar"
        }

        last_response.location.must_equal(
          "http://example.org/worksets/#{workset.uid}/#{workset.revision}"
        )
      end

      it "should allow collaborators to change the name" do
        b = Occam::Person.create!(:username => "jane",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        a = login_as('wilkie')

        workset = create_owned_workset({
          :name => "foo"
        }, b, a)

        workset.expects(:update_info).with(has_entry("name", "bar"))

        post "/worksets/#{workset.uid}", {
          "name" => "bar"
        }
      end

      it "should allow a collaborator to change the tags" do
        b = Occam::Person.create!(:username => "jane",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        a = login_as('wilkie')

        workset = create_owned_workset({
          :tags  => ";a;b;",
        }, b, a)

        workset.expects(:update_info).with(has_entry("tags", ";c;d;"))

        post "/worksets/#{workset.uid}", {
          "tags" => ";c;d;"
        }
      end

      it "should allow a collaborator to change the private/public status" do
        b = Occam::Person.create!(:username => "jane",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        a = login_as('wilkie')

        workset = create_owned_workset({
          :private => 0
        }, b, a)

        post "/worksets/#{workset.uid}", {
          :private => "1"
        }

        Occam::Workset.find_by(:uid => workset.uid).private.must_equal 1
      end

      it "should allow the owner to change the name" do
        a = login_as('wilkie')

        workset = create_owned_workset({
          :name => "foo"
        }, a)

        workset.expects(:update_info).with(has_entry("name", "bar"))

        post "/worksets/#{workset.uid}", {
          "name" => "bar"
        }
      end

      it "should allow the owner to change the tags" do
        a = login_as('wilkie')

        workset = create_owned_workset({
          :tags => ";a;b;"
        }, a)

        workset.expects(:update_info).with(has_entry("tags", ";c;d;"))

        post "/worksets/#{workset.uid}", {
          "tags" => ";c;d;"
        }
      end

      it "should allow the owner to change the private/public status" do
        a = login_as('wilkie')

        workset = create_owned_workset({
          :private => 0
        }, a)

        post "/worksets/#{workset.uid}", {
          :private => "1"
        }

        Occam::Workset.find_by(:uid => workset.uid).private.must_equal 1
      end
    end

    describe "GET /worksets/:uuid" do
      it "should return 404 when the workset isn't found" do
        Occam.any_instance.stubs(:markdown)

        get '/worksets/asdf'

        last_response.status.must_equal 404
      end

      it "should return 404 when the workset can't be viewed" do
        owner = Occam::Person.create!(:username => "jane",
                                      :uid      => "abcd1234567890-1234-2345-34567890")

        login_as('wilkie')

        workset = Occam::Workset.create(:name => "foo",
                                        :private => 1,
                                        :uid  => "abcd1234567890-1234-2345-34567890")

        Occam::Authorship.create(:workset => workset,
                                 :person  => owner)

        get "/worksets/#{workset.uid}"

        last_response.status.must_equal 404
      end

      it "should return 200 when private workset is viewed by the owner" do
        b = Occam::Person.create!(:username => "jane",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        a = login_as('wilkie')

        workset = create_owned_workset({
          :private => 1
        }, a, b)

        get "/worksets/#{workset.uid}"

        last_response.status.must_equal 200
      end

      it "should return 200 when private workset is viewed by collaborator" do
        b = Occam::Person.create!(:username => "jane",
                                  :uid      => "abcd1234567890-1234-2345-34567890")

        a = login_as('wilkie')

        workset = create_owned_workset({
          :private => 1
        }, b, a)

        get "/worksets/#{workset.uid}"

        last_response.status.must_equal 200
      end

      it "should render worksets/show.haml" do
        owner = login_as('wilkie')

        workset = create_owned_workset({}, owner)

        Occam.any_instance.expects(:render).with(
          anything,
          :"worksets/show",
          anything
        )

        get "/worksets/#{workset.uid}"
      end

      it "should pass the workset to the view" do
        owner = Occam::Person.create!(:username => "jane",
                                      :uid      => "abcd1234567890-1234-2345-34567890")

        workset = create_owned_workset({}, owner)

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:workset => workset)
        )

        get "/worksets/#{workset.uid}"
      end

      it "should pass the owning person to the view" do
        owner = Occam::Person.create!(:username => "jane",
                                      :uid      => "abcd1234567890-1234-2345-34567890")

        workset = create_owned_workset({}, owner)

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:person => owner)
        )

        get "/worksets/#{workset.uid}"
      end

      it "should pass the list of collaborators to the view" do
        owner = Occam::Person.create!(:username => "jane",
                                      :uid      => "abcd1234567890-1234-2345-34567890")

        a = Occam::Person.create!(:username => "wilkie",
                                  :uid      => "dcba1234567890-1234-2345-34567890")

        workset = create_owned_workset({}, owner, a)

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_of_type(:collaborators => Array)
        )

        get "/worksets/#{workset.uid}"
      end
    end
  end
end
