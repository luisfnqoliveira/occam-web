# TODO: workset.can_edit?(...) / can_view?(...) authorizations

class Occam
  # Get a list of all runs
  get '/runs' do
  end

  # Get run information explicitly as json
  get '/runs/:id.json' do
    run = Occam::Run.find_by(:id => params[:id])
    if run.nil?
      status 404
      return
    end

    experiment = run.experiment
    workset    = run.workset

    if workset.nil?
      status 404
    else
      # Return run metadata
      content_type 'application/json'
      run.to_json
    end
  end

  # Get run information
  get '/runs/:id' do
    run = Occam::Run.find_by(:id => params[:id])
    if run.nil?
      status 404
      return
    end

    experiment = run.experiment
    workset    = run.workset

    if workset.nil?
      status 404
    elsif workset.can_edit?(current_person)
      # We can query by type
      format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

      case format
      when 'text/html'
        render :haml, :"runs/show", :locals => {
          :run => run,
          :workset => workset,
          :experiment => experiment
        }
      when 'application/json'
        # Return run metadata
        content_type 'application/json'
        run.to_json
      end
    end
  end

  # Get job information explicitly as json
  get '/runs/:id/jobs/:job_id.json' do
    run = Occam::Run.find_by(:id => params[:id])
    if run.nil?
      status 404
      return
    end

    job = run.jobs.where(:id => params[:job_id]).first()
    if job.nil?
      status 404
      return
    end

    experiment = run.experiment
    workset    = run.workset

    if workset.nil?
      status 404
    else
      # Return run metadata
      content_type 'application/json'
      job.to_json
    end
  end

  # Get job information
  get '/runs/:id/jobs/:job_id' do
    run = Occam::Run.find_by(:id => params[:id])
    if run.nil?
      status 404
      return
    end

    job = run.jobs.where(:id => params[:job_id]).first()
    if job.nil?
      status 404
      return
    end

    experiment = run.experiment
    workset    = run.workset

    if workset.nil?
      status 404
    elsif workset.can_edit?(current_person)
      # We can query by type
      format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

      case format
      when 'text/html'
        render :haml, :"jobs/show", :locals => {
          :run => run,
          :job => job,
          :workset => workset,
          :experiment => experiment
        }
      when 'application/json'
        # Return run metadata
        content_type 'application/json'
        job.to_json
      end
    end
  end

  # Cancel the run
  get '/runs/:id/cancel' do
    run = Occam::Run.find_by(:id => params[:id])
    if run.nil?
      status 404
      return
    end

    experiment = run.experiment
    workset    = run.workset

    if workset.nil?
      status 404
    else
      if workset.can_edit?(current_account)
        run.cancel

        if request.preferred_type('text/html')
          # Redirect to the workset page (if asking for html)
          if group
            redirect "/worksets/#{workset.id}/groups/#{group.id}/experiments/#{experiment.id}"
          else
            redirect "/worksets/#{workset.id}/experiments/#{experiment.id}"
          end
        elsif request.preferred_type('application/json')
          # If asking for json, return success or fail
          content_type 'application/json'
          {
            :result => "success"
          }.to_json
        else
          status 404
        end
      else
        status 406
      end
    end
  end

  # Start the run
  get '/runs/:id/run' do
    run        = Occam::Run.find_by(:id => params[:id])
    experiment = run.experiment
    group      = experiment.group
    workset    = experiment.workset

    if workset.nil?
      status 404
    else
      if workset.can_edit?(current_account)
        run.run

        if request.preferred_type('text/html')
          # Redirect to the workset page (if asking for html)
          if group
            redirect "/worksets/#{workset.id}/groups/#{group.id}/experiments/#{experiment.id}"
          else
            redirect "/worksets/#{workset.id}/experiments/#{experiment.id}"
          end
        elsif request.preferred_type('application/json')
          # If asking for json, return success or fail
          content_type 'application/json'
          {
            :result => "success"
          }.to_json
        else
          status 404
        end
      else
        status 406
      end
    end
  end

  # Edit an existing run
  post '/runs/:id' do
    run = Occam::Run.find_by(:id => params[:id])
    if run.nil?
      status 404
      return
    end

    experiment = run.experiment
    workset    = run.workset

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)
      if params.has_key? "archived"
        run.archived = params["archived"].to_i
        run.save
      end
      if not request.referrer.blank?
        redirect request.referrer
      else
        redirect "/runs/#{run.id}"
      end
    else
      # Unauthorized
      status 404
    end
  end
end
