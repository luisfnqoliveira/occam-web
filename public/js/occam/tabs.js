/*
 * This module handles Tab strips on the site. It allows you to wrap a ul.tabs
 * element and provides functions to add a tab, set the current tab, etc.
 */

$(function() {
  // Initialize all Tab Strips
  Occam.Tabs.bind($('body'));
});

var initOccamTabs = function(Occam) {
  'use strict';

  /*
   * This constructor creates an object that represents a tabstrip. You pass in
   * the jQuery element for the tabstrip to attach the object to the given
   * element on the page. If more than one element are passed, the first is
   * used.
   */
  var Tabs = Occam.Tabs = function(element) {
    if (element) {
      this.element = element.first();
      this.panels = element.parent().children('ul.tab-panels').first();
      if (this.panels.length == 0) {
        this.panels = element.parent().parent().children('ul.tab-panels').first();
      }
    }
    else {
      throw("Requires at least one element to be passed");
    }
  };

  Tabs.bind = function(element) {
    element.find('ul.tabs').each(function() {
      (new Occam.Tabs($(this))).bindEvents();
    });
  };

  /*
   * This method is the event callback that will reveal the bound tab.
   */
  Tabs.revealTab = function(event) {
    // Change active tab
    $(this).parent().children('.active').removeClass('active');
    $(this).addClass('active');

    // Find the parent of both the tabs and tab-panels to find tab-panels
    var tabPanels = $(this).parents('.tabs').parent().find('.tab-panels');

    // Hide tab
    tabPanels.first().children('.tab-panel.active').removeClass('active').attr('aria-hidden', 'true');

    // Reveal tab
    tabPanels.first().children('.tab-panel:eq(' + $(this).index() + ')').addClass('active').attr('aria-hidden', 'false').trigger('resize');
  };

  /*
   * This method will ensure that tab events are bound to this tabstrip. You
   * can give a jQuery tab element which will have its events bound individually
   * or, when called without arguments, the events will be bound to all tabs on
   * the strip.
   */
  Tabs.prototype.bindEvents = function(tab) {
    if (tab) {
      tab.on('click.reveal-tab', Tabs.revealTab);

      tab.children('a').on('click', function(event) {
        event.preventDefault();
      });
    }
    else {
      var self = this;
      self.element.children('li.tab').each(function() {
        self.bindEvents($(this));
      });
    }

    return this;
  };

  /*
   * This method adds a tab at the given index with the given name. If no index
   * is given, the tab is appended to the end of the tabstrip (the right-hand
   * side assuming a left-to-right rendering)
   */
  Tabs.prototype.addTab = function(name, b, c) {
    var callback = b;
    var atIndex = b;

    if (arguments.length > 2) {
      callback = c;
    }
    else {
      atIndex = null;
    }

    var index = atIndex || this.element.children().length;
    if (atIndex == 0) {
      index = 0;
    }

    var tab      = $('<li class="tab"><a></a></li>');
    tab.children('a').text(name);

    var tabPanel = $('<li class="tab-panel"></li>');

    this.element.append(tab);
    this.panels.append(tabPanel);

    this.bindEvents(tab);

    if (callback) {
      callback(tabPanel);
    }

    return this;
  };

  /*
   * This method removes the tab at the given index.
   */
  Tabs.prototype.removeTab = function(atIndex) {
    this.element.children('li.tab:nth-child(' + atIndex + ')').remove();
    this.panels.children('li.tab-panel:nth-child(' + atIndex + ')').remove();

    return this;
  };

  /*
   * This method returns the jQuery element of the tab at the given index.
   */
  Tabs.prototype.tabAt = function(atIndex) {
    return this.element.children('li.tab:nth-child(' + atIndex + ')');
  };

  /*
   * This method returns the jQuery element of the tab-panel at the given index.
   */
  Tabs.prototype.tabPanelAt = function(atIndex) {
    return this.panels.children('li.tab-panel:nth-child(' + atIndex + ')');
  };

  /* This method gives you the number of tabs
   */
  Tabs.prototype.tabCount = function() {
    return this.element.children('li.tab').length;
  };
};

