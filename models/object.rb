class Occam
  class Object < ActiveRecord::Base
    ALLOWED_COMMANDS = [
      'set',
      'view',
      'attach',
      'detach',
      'append',
      'configure'
    ]

    TEXT_MEDIA_TYPES = ['application/x-python',
                        'application/python',
                        'application/x-ruby',
                        'application/ruby',
                        'application/x-perl',
                        'application/perl',
                        'application/sh',
                        'application/x-sh',
                        'application/x-tex',
                        'application/tex',
                        'application/x-javascript',
                        'application/javascript',
                        'application/json']

    require 'json'
    require_relative '../lib/git'

    # Fields

    # published - DateTime when object was originally published
    # updated   - DateTime when object was updated
    def update_timestamps
      now = Time.now.utc
      self[:published] ||= now if !persisted?
      self[:updated]     = now
    end
    before_save :update_timestamps

    # id        - Unique identifier.

    # name      - The name of the object.

    # object_type - Object's type (simulator, benchmark, etc)
    def type
      self.object_type
    end

    def type=(value)
      self.object_type = value
    end

    # Yields the object's type at the current revision as something that can
    # be used as an HTML class.
    def typeClass
      self.object_type_safe
    end

    def self.typeClass(type)
      type.gsub('/', '-')
    end

    # Return a list of all known object types
    def self.types
      Occam::Object.all.select(:object_type).distinct.map(&:object_type)
    end

    # An Object can represent one of many built-in special things:
    has_one :person,
            :foreign_key => :occam_object_id
    has_one :group_object,
            :foreign_key => :occam_object_id,
            :class_name => "Group"
    has_one :experiment,
            :foreign_key => :occam_object_id
    has_one :workset,
            :foreign_key => :occam_object_id

    def self.findPerson(uuid, revision=nil)
      obj = Occam::Object.find_by(:uid => uuid)

      if obj
        obj = obj.person
        if revision
          obj.revision = revision
        end
      end

      obj
    end

    def self.findObject(uuid, revision=nil)
      obj = Occam::Object.find_by(:uid => uuid)

      if obj
        case obj.object_type
        when "experiment"
          obj = obj.experiment
        when "group"
          obj = obj.group_object
        when "workset"
          obj = obj.workset
        when "person"
          obj = obj.person
        end
        if revision
          obj.revision = revision
        end
      end

      obj
    end

    def self.findWorkset(uuid, revision = nil)
      obj = Occam::Object.find_by(:uid => uuid)

      if obj
        obj = obj.workset
        if revision
          obj.revision = revision
        end
      end

      obj
    end

    def self.findGroup(uuid, revision=nil)
      obj = Occam::Object.find_by(:uid => uuid)

      if obj
        obj = obj.group_object
        if revision
          obj.revision = revision
        end
      end

      obj
    end

    def self.findExperiment(uuid, revision=nil)
      obj = Occam::Object.find_by(:uid => uuid)

      if obj
        obj = obj.experiment
        if revision
          obj.revision = revision
        end
      end

      obj
    end

    # dependencies (through Dependency)
    has_many :dependencies, :foreign_key => :dependant_id

    # inputs
    has_many :inputs,
             :class_name  => "Occam::ObjectInput",
             :foreign_key => :occam_object_id

    # Yield all unique input types
    def input_types
      self.described_inputs
    end

    def indirect_input_types(object_type=nil)
      # For every type, find existing objects elsewhere that can generate them.
      # Sometimes, a simulator may want a trace, but it is more likely you
      # will just attach a trace generator instead. So this is for attaching
      # 'Generator' type objects to objects that take generic inputs.
      types = []
      self.described_inputs(object_type).each do |input|
        # for every input, look up all object_outputs that yield it.
        types.concat(input.indirect_objects.select(:object_type).distinct)
      end
      types
    end

    # Yield all objects that can output a particular type/group pair.
    def self.outputs_type(object_type, subtype)
      Occam::Object.where(:id => Occam::ObjectOutput.where(:object_type => object_type, :subtype => subtype).select(:occam_object_id))
    end

    def inputs_info(object_type = nil)
      info = self.objectInfo

      (info['inputs'] || []).select do |input|
        (input['type'] == "occam/runnable" && info.has_key?('run')) || (object_type.nil? || object_type == input['type'])
      end
    end

    # Yield all inputs, optionally all with the given type.
    def described_inputs(object_type = nil)
      info = self.objectInfo

      (info['inputs'] || []).select do |input|
        object_type.nil? || object_type == input['type']
      end.map do |input|
        Occam::ObjectInput.new(:object_type_safe => Occam::Object.slugFor(input['type']), :object_type => input['type'], :subtype => input['subtype'], :fifo => input['fifo'] ? 1 : 0)
      end
    end

    # Yield all outputs, optionally all with the given type.
    def described_outputs(object_type = nil)
      info = self.objectInfo

      (info['outputs'] || []).select do |output|
        object_type.nil? || object_type == output['type']
      end.map do |output|
        Occam::ObjectOutput.new(:object_type_safe => Occam::Object.slugFor(output['type']), :object_type => output['type'], :subtype => output['subtype'], :fifo => output['fifo'] ? 1 : 0, :name => output['name'])
      end
    end

    # outputs
    has_many :outputs,
             :class_name  => "Occam::ObjectOutput",
             :foreign_key => :occam_object_id

    # Yield all unique output types
    def output_types
      self.outputs.select(:object_type).distinct.map(&:object_type)
    end

    # Returns the first inactive dependency (or nil if none)
    def first_inactive_dependency
      self.dependencies.each do |d|
        if !d.depends_on.active
          return d.depends_on
        end
      end

      nil
    end

    # Returns true if all dependencies are active
    def dependencies_active?
      self.first_inactive_dependency.nil?
    end

    # tags      - The semicolon separated list of tags.

    def objectInfoTags
      info = self.objectInfo()
      ret = info['tags'] or []
      if not ret.is_a? Array
        return []
      end
    end

    # active    - Whether or not this object has been moderated
    def active=(value)
      # Coerse to integer
      if value.is_a? TrueClass
        value = 1
      elsif value.is_a? FalseClass
        value = 0
      end

      self[:active] = value
    end

    def active
      self[:active] == 1
    end

    # built     - Whether or not the object has been built
    def built=(value)
      # Coerse to integer
      if value.is_a? TrueClass
        value = 1
      elsif value.is_a? FalseClass
        value = 0
      end

      self[:built] = value
    end

    def built
      self[:built] == 1
    end

    # Returns a query of all inactive Objects. These are Objects that
    # await administrator approval.
    def self.all_inactive(type = nil)
      ret = Object.where(:active => 0)
      if not type.nil?
        ret = ret.where(:object_type => type)
      end
      ret
    end

    # Returns a query of all active and available Objects. This means
    # they have been moderated (if necessary) by an administrator and they
    # have been successfully built.
    def self.all_active
      # There is no point in not having 'built' specified or placed in a
      # second function. You cannot have active: false and built: true
      # for instance.
      Object.where(:active => 1, :built => 1)
    end

    def self.names(type = nil)
      ret = self.all.select(:name, :id)
      if not type.nil?
        ret = ret.where(:object_type => type)
      end
      ret
    end

    def self.all_tags(type, with = "")
      # TODO: obviously, normalize tags
      self.all_active.where(:object_type => type).to_a.map{|ex| ex.tags[1..-2].split(";")}.flatten.uniq.select{|tag| tag.match(Regexp.escape(with))}
    end

    def self.all_tags_json(with = "")
      self.all_tags(with).to_json
    end

    def self.with_tag(type, tag)
      Object.all_active.where(:object_type => type).where('tags LIKE ?', "%;#{tag};%")
    end

    def self.search(term)
      Object.where('name LIKE ?', "%#{term}%").select([:name, :description, :id, :revision])
    end

    def self.search_adv(name, tags, description, website, organization, license, authors)
      query = Object.where('name LIKE ?', "%#{name}%")
      tags = tags.split ;
      tags.each do |tag|
        query = query.where('tags LIKE ?', "%#{tag}%")
      end
      query = query.where('description LIKE ?', "%#{description}")
      query = query.where('organization LIKE ?', "%#{organization}")
      query = query.where('license LIKE ?', "%#{license}")
      query = query.where('authors LIKE ?', "%#{authors}")
      query = query.select([:name, :description, :id, :revision])
    end

    # Gathers any Objects that can view this object
    def viewers
      # TODO: add capability tags
      # TODO: handle revisions?

      Occam::Object.where(:id => Occam::Viewer.where(:views_type => self.object_type, :views_subtype => self.subtype).select(:occam_object_id))
    end

    def runners
      # TODO: handle providers with null environment or architecture

      Occam::Object.where(:id => Occam::Provider.where(:environment => self.environment, :architecture => self.architecture).select(:occam_object_id))
    end

    # Pull object record information from the given url in script_path
    def import(url=nil, json=nil)
      self
    end

    def local?
      @local
    end

    # Override to_a to split tag strings to arrays
    def to_hash
      hash = self.objectInfo

      # Correct for anything or delete things here

      hash
    end

    def worksetBelongsTo(limit = 10)
      # TODO: cache this result maybe

      # First look for 'workset' key to short circuit
      if self.objectInfo.has_key? 'workset'
        workset = Occam::Object.findWorkset(self.objectInfo['workset']['id'])
        if workset
          return workset
        end
      end

      if limit <= 0
        return nil
      end

      if self.object_type == "workset"
        return self
      end

      workset = nil

      object_info = self.objectInfo
      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        uuid = belongs_to['id']
        type = belongs_to['type']

        if type == 'workset'
          workset = Occam::Object.findWorkset(uuid)
        else
          parent = Occam::Object.find_by(:uid => uuid)
          if parent
            workset = parent.worksetBelongsTo(limit-1)
          end
        end
      end

      workset
    end

    def belongsTo
      parent = nil

      object_info = self.objectInfo
      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        uuid = belongs_to['id']

        parent = Occam::Object.findObject(uuid)
      end

      parent
    end

    def to_json(*args)
      self.to_hash.to_json
    end

    def git
      if !!self.resource or @git == false
        if self.git?
          @git ||= Occam::Git.new(self.gitPath, self.revision)
        else
          @git = false
          @git
        end
      else
        if @backed
        else
          @git ||= Occam::Git.new(self.path, self.revision)
        end
      end
    end

    def parentRevision
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if self.git == false
        nil
      elsif Occam::Backend.connected
        Occam::Backend.parentRevision(self.uid, self.revision)
      else
        self.git.parent(self.revision)
      end
    end

    def childRevisions
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if self.git == false
        []
      elsif Occam::Backend.connected
        Occam::Backend.childRevisions(self.uid, self.revision)
      else
        self.git.children(self.revision)
      end
    end

    def fullRevision
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if Occam::Backend.connected
        Occam::Backend.fullRevision(self.uid, self.revision)
      else
        Git.fullRevision(self.path, self.revision)
      end
    end

    def eachFile(filepath="", options={}, &block)
      filepath ||= ""

      if Occam::Backend.connected
        # TODO: handle filtering and ordering
        pretext = "/objects"
        Occam::Backend.eachFile(self.uid, self.revision, filepath, pretext, &block)
      else
        git = self.git
        if self.zip?
          if options[:order] && options[:order] == :group
            self.zip.eachFile(filepath, :trees, &block)
            self.zip.eachFile(filepath, :files, &block)
          else
            self.zip.eachFile(filepath, :files, &block)
          end
          return
        end

        if git == false
          return
        end

        # TODO: handle filtering

        if filepath == "" || self.fileIsGroup?(filepath, git)
          if options[:order] && options[:order] == :group
            git.eachFile(filepath, :trees, &block)
            git.eachFile(filepath, :files, &block)
          else
            git.eachFile(filepath, &block)
          end
        end
      end
    end

    # Returns true when this object represents a stored resource.
    def resource?
      !!self.resource
    end

    # Returns true when this object represents a zip file.
    def zip?
      self.resource == "application/zip"
    end

    # Returns the path to the zip file data represented by this object.
    def zipPath
      path = self.path('store')
      path = File.join(path, self.revision, 'data')
      path
    end

    # Returns true if the file indicated by path exists within this object.
    def exists?(path)
      # The file info will be empty (that is, {}) when no file is found.
      info = self.retrieveFileInfo(path)

      !info.empty?
    end

    def zip
      if self.zip?
        @zip ||= Occam::Zip.new(self.zipPath)
      else
        return nil
      end
    end

    # Returns true when this object represents a git repository.
    def git?
      self.resource == "git"
    end

    # Returns true when this object represents a stored file.
    def file?
      self.resource == "file" || self.zip?
    end

    def objectInfo
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if !!self.resource
        # Resource Object
        @objectInfo ||= {
          'name'     => self.name,
          'type'     => self.type,
          'resource' => self.resource,
          'id'       => self.uid,
          'revision' => self.revision,
        }
      else
        # Normal Object, object info is stored by revision
        @objectInfo ||= self.retrieveJSON("object.json")
        if @objectInfo['id'] != self.uid
          info = @objectInfo
          @objectInfo = {}
          (info['includes'] || []).each do |derivative|
            if derivative['id'] == self.uid
              new_info = info.clone()

              new_info.delete 'includes'
              new_info.delete 'inputs'
              new_info.delete 'outputs'
              new_info.delete 'install'
              new_info.delete 'build'
              new_info.delete 'run'
              new_info.delete 'configurations'
              new_info.delete 'contains'

              new_info.merge!(derivative)
              @objectInfo = new_info
              break
            end
          end
        end
      end
      @objectInfo
    end

    def revision=(*args)
      # When the revision is changed, we should invalidate our git repo
      # and our cached depiction of the object info.
      @git = nil
      @objectInfo = nil
      @hasDatapoints = nil

      super(*args)
    end

    # Retrieves the invocation data for this object
    def retrieveInvocationData(category = 'output', revision=self.revision)
      if Occam::Backend.connected
        ret = Occam::Backend.retrieveInvocationData(self.uid, category)
      else
        # Pull it directly
        filePath = File.join(self.path('invocation'), "#{category}.json")
        begin
          puts "Invocation file opening at #{filePath}"
          File.open(filePath, 'r') do |f|
            ret = JSON.parse(f.read)
          end
        rescue
          ret = {}
        end
      end

      ret ||= {}

      if revision.nil?
        ret
      else
        ret[revision]
      end
    end

    def retrieveJSON(path, git=nil)
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      if git.nil?
        git = self.git
      end

      if git == false
        {}
      elsif Occam::Backend.connected
        Occam::Backend.retrieveJSON(self.uid, self.revision, path)
      else
        if zip?
          zip.retrieveJSON(path)
        else
          git.retrieveJSON(path)
        end
      end
    end

    def retrieveFileInfo(path)
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      # TODO: allow a max byte size to return for some views to show parts of files
      if Occam::Backend.connected
        pretext = "/objects"
        if @backed
          pretext = "/resources/git"
        end
        Occam::Backend.retrieveFileInfo(self.uid, self.revision, path, pretext)
      else
        if self.zip?
          self.zip.retrieveFileInfo(path)
        elsif self.git?
          self.git.retrieveFileInfo(path)
        else
          info = {}
          # TODO: Get filesize?
          info[:size] = 0
          info[:name] = "data"
          info[:stat] = 775
          info[:type] = "file"
          return info
        end
      end
    end

    def retrieveFile(path, git=nil)
      # We either directly read it from the git store locally, or make a request
      # to serve it from another OCCAM node.
      # TODO: allow a max byte size to return for some views to show parts of files
      if git.nil?
        git = self.git
      end

      if Occam::Backend.connected
        pretext = "/objects"
        if @backed
          pretext = "/resources/git"
        end
        Occam::Backend.retrieveFile(self.uid, self.revision, path, pretext)
      elsif self.zip?
        zip.retrieveFile(path)
      elsif self.git?
        git.retrieveFile(path)
      elsif self.resource?
        Occam::Filesystem.retrieveData(self.uid, self.revision)
      elsif !git.nil?
        git.retrieveFile(path)
      else
        ""
      end
    end

    def datapoints?
      if @hasDatapoints.nil?
        @hasDatapoints = false
        self.configurations.each_with_index do |configuration, i|
          schema = self.configurationSchema(i)

          def hasDatapoints?(hash)
            hash.each do |k, v|
              if v.is_a?(Hash)
                if v.has_key?("type") && !v['type'].is_a?(Hash)
                  if v['type'] == "datapoints"
                    return true
                  end
                else
                  if hasDatapoints?(v)
                    return true
                  end
                end
              end
            end

            false
          end

          @hasDatapoints ||= hasDatapoints?(schema)
        end
      end

      @hasDatapoints
    end

    def configurations
      self.objectInfo['configurations'] || []
    end

    def configurationSchema(index)
      configuration = self.configurations[index]
      ret = {}
      if configuration
        if configuration['schema'].is_a? String
          ret = self.retrieveJSON(configuration['schema'])
        else
          ret = configuration['schema']
        end
      end
      ret
    end

    def self.download_json(url, content_type = 'application/json', limit = 10)
      # Just try and pull json

      uri = URI(url)
      request = Net::HTTP::Get.new(uri.request_uri)
      request['Accept'] = content_type
      request.content_type = content_type

      http = Net::HTTP.new(uri.hostname, uri.port)
      if uri.scheme == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end

      response = http.request(request)

      # Did we get a response? (allow redirects)
      if response.is_a?(Net::HTTPRedirection) && limit > 0
        location = response['location']
        Occam::Object.download_json(location, content_type, limit - 1)
      elsif response.is_a?(Net::HTTPSuccess) && response.content_type == "application/json"
        JSON.parse(response.body)
      else # Otherwise, assume git:
        Occam::Git.description(url)
      end
    end

    def self.all_unknown_dependencies(url, json=nil)
      json ||= Occam::Object.download_json(url)
      json["type"] ||= "simulator"

      unknowns = Occam::Object.unknown_dependencies(json)

      deps = []

      unknowns.each do |unknown|
       # url = unknown["git"]
       # json = Occam::Object.download_json(url)
       # obj  = Occam::Object.import(url, json)
       # data = Occam::Object.all_unknown_dependencies(url, json)
       # deps.concat data
       # deps.append obj
      end

      deps
    end

    def self.import_all(url, json=nil)
      json ||= Occam::Object.download_json(url)

      uuid = json['id']

      command = "pull #{url}"

      Occam::Worker.perform(command)

      obj = Occam::Object.where(:uid => uuid).first
    end

    # Pulls an object from a OCCAM json description file.
    def self.import(url, json=nil)
      json ||= Occam::Object.download_json(url)
      json["type"] ||= "simulator"
      uid = json['id']

      # Pull out existing object or create a new one
      object = Object.where(:object_type => json["type"]).where(:uid => uid).first || Object.new(nil, :objectInfo => json, :uid => uid)
      object.uid = uid
      object.import(url, json)
    end

    def self.unknown_dependencies(json)
      ret = []

      if json.has_key? "dependencies"
        json["dependencies"].each do |dependency|
          object_type = dependency["type"] || "simulator"

          if dependency.has_key? "id"
            object = Object.where(:object_type => object_type).where(:uid => dependency["id"]).first
          end

          if object.nil?
            ret << dependency
          end
        end
      end

      ret
    end

    # Given a json object description, return all realized dependencies.
    def self.dependencies(json)
      ret = []

      if json.has_key? "dependencies"
        json["dependencies"].each do |dependency|
          # TODO: Other code syndication options
          object_type = dependency["type"] || "simulator"
          if dependency.has_key? "git"
            object = Object.where(:object_type => object_type).where(:script_path => dependency["git"]).first
          end

          if object.nil? && dependency.has_key?("name")
            object = Object.where(:object_type => object_type).where(:name => dependency["name"]).first
          end

          unless object.nil?
            ret << Dependency.new(:depends_on => object)
          end
        end
      end

      ret
    end

    def self.slugFor(object_type)
      object_type.tr('/', '-')
    end

    def authors
      info = self.objectInfo
      ret = []

      (info['authors'] or []).each do |author|
        if author.is_a? Hash
          id = author['id'] or nil
        else
          id = author
        end

        if not id.nil?
          person = Occam::Object.findPerson(id)
          if person
            ret << person
          else
            ret << author
          end
        end
      end

      ret
    end

    def collaborators
      info = self.objectInfo
      ret = []

      (info['collaborators'] or []).each do |author|
        if author.is_a? Hash
          id = author['id'] or nil
        else
          id = author
        end

        if not id.nil?
          person = Occam::Object.findPerson(id)
          if person
            ret << person
          else
            ret << author
          end
        end
      end

      ret
    end

    def file
      # Retrieves the file attached to this object
      objectInfo = self.objectInfo()
      if objectInfo.has_key? 'file'
        self.retrieveFile(objectInfo['file'])
      else
        ""
      end
    end

    def fileAsJSON
      # Retrieves the file attached to this object as JSON
      objectInfo = self.objectInfo()
      if objectInfo.has_key? 'file'
        self.retrieveJSON(objectInfo['file'])
      else
        {}
      end
    end

    def schema
      # Retrieves the schema attached to this object as JSON
      objectInfo = self.objectInfo()
      if objectInfo.has_key? 'schema'
        self.retrieveJSON(objectInfo['schema'])
      else
        {}
      end
    end

    def gitPath
      # Return the path of the object's git backed store in the local object store
      base_path = File.realpath(Occam::Config.configuration['paths']['git'])
      uid = self.uid
      if self.owner_uid
        uid = self.owner_uid
      end

      code = uid
      code_1 = code[0..1]
      code_2 = code[2..3]
      File.join(base_path, code_1, code_2, uid)
    end

    def path(subType = 'objects')
      # Return the path of the object in the local object store
      rel_path = Occam::Config.configuration['paths'][subType]
      if not File.exists?(rel_path)
        Dir.mkdir(rel_path)
      end
      base_path = File.realpath(rel_path)

      uid = self.uid
      if self.owner_uid
        uid = self.owner_uid
      end

      code = uid
      code_1 = code[0..1]
      code_2 = code[2..3]
      File.join(base_path, code_1, code_2, uid)
    end

    # Returns any available configurators for this object.
    def configurators
      Occam::Object.where(:object_type => "configurator", :configures => self.uid)
    end

    def self.basic_type_search(query)
      # Searchs for a partial type match
      Occam::Object.where('LOWER(object_type) LIKE LOWER(?)', "%#{query}%").select(:object_type, :object_type_safe).distinct
    end

    def self.basic_name_search(query)
      # Searches for a partial name or a full UUID
      Occam::Object.where('LOWER(name) LIKE LOWER(?) OR uid=?', "%#{query}%", query).select(:object_type, :name, :uid, :object_type_safe, :organization, :revision)
    end

    def self.basic_tag_search(query)
      # TODO: return only an array of tags??
      Occam::Object.where('tags LIKE ?', ";%#{query}%;").select(:object_type, :name, :uid, :object_type_safe)
    end

    # Returns the bibtex formatted version of the citation, if one exists
    def bibtex
      objectInfo = self.objectInfo()
      citation = objectInfo['citation']
      if citation
        # TODO: safety of object name for bibtex purposes
        ret = "@#{(citation['type'] || "article").upcase}{#{self.name},\n"
        citation.each do |k,v|
          if k != "type"
            if v.is_a? Array
              v = v.join(';')
            end
            # TODO: escape semicolons and curly braces??
            ret << "#{k}={#{v}},\n"
          end
        end
        ret << "}"
      else
        nil
      end
    end

    def groups
      info = self.objectInfo()
      ret = []

      (info['contains'] or []).each do |dependency|
        if dependency['type'] == 'group'
          group = Occam::Object.findGroup(dependency['id'])

          if group
            if dependency['revision']
              group.revision = dependency['revision']
            end
            ret << group
          end
        end
      end

      ret
    end

    def experiments
      info = self.objectInfo()
      ret = []

      (info['contains'] or []).each do |dependency|
        if dependency['type'] == 'experiment'
          experiment = Occam::Object.findExperiment(dependency['id'])

          if experiment
            if dependency['revision']
              experiment.revision = dependency['revision']
            end
            ret << experiment
          end
        end
      end

      ret
    end

    # This takes a workset (which indicates permissions), an occam command, and
    # a list of arguments and issues it to the backend for this object.
    # It will only allow commands listed in ALLOWED_COMMANDS and will only allow
    # certain --options and will encode the arguments for input sanitization.
    def performAction(workset, command, arguments)
      if not ALLOWED_COMMANDS.include? command
        return nil
      end

      command = "#{command} --base64 "

      arguments.each do |argument|
        if argument["key"] && argument["key"].is_a?(String) && !argument["key"].match(/^--[a-z-]+$/).nil?
          command << "#{argument["key"]} "
        end

        if argument["values"] && argument["values"].is_a?(Array)
          argument["values"].each do |value|
            command << "#{Base64.strict_encode64(value)} "
          end
        end
      end

      # Amend object id/revision
      command << "--to #{Base64.strict_encode64(self.uid)} "
      command << "--revision #{Base64.strict_encode64(self.revision)} "

      # Amend workset id/revision
      command << "--within #{Base64.strict_encode64(workset.uid)} "
      command << "--within-revision #{Base64.strict_encode64(workset.revision)} "

      # Send the command out
      Occam::Worker.perform(command).strip
    end

    # Uploads the given file data to the file name given for this object.
    # Returns the new revision.
    def uploadFile(workset, name, io, to=nil)
      # On worker/storage nodes, actually do the store and update
      # On front-end nodes, pass this off to a back-end server
      if Occam::Backend.connected
        Occam::Backend.uploadFile(self.uid, self.revision, workset.uid, workset.revision, name, io, to)
      else
        # The web-server does this logic because it is hard to do piping correctly.

        # Return the new revision
        # occam clone --temporary --to #{uid} --revision #{revision}
        command = "clone --base64 --temporary --store --linked --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)}"

        # Returns path
        path = Occam::Worker.perform(command).strip

        # Place file in path and add/commit that file
        # TODO: name fixing for when same name exists
        filepath = path
        if to
          filepath = File.join(filepath, to)

          if not File.exists?(filepath)
            Dir.mkdir(filepath)
          end
        end

        filepath = File.join(filepath, name)

        File.open(filepath, 'w+b') do |f|
          f.write io.read
        end

        Dir.chdir(path) do
          `git add #{filepath}`
          `git commit #{filepath} -m "File upload of #{filepath}"`
          revision = `occam commit`
        end
      end

      revision
    end

    # Infer a file's type from the extension.
    def fileType(path)
      type = self.mimeType(path)
      if type
        type.content_type
      else
        "application/octet-stream"
      end
    end

    def pathResource(path)
      if (path.nil? || path == "" || path == "/")
        return nil, path
      end

      # Gather resources
      objectInfo = self.objectInfo
      installs = objectInfo['install'] || []

      # If there are no resources, then no path can be within one
      if installs.empty?
        return nil, path
      end

      # Split path up in parts and see if the substrings match any 'to' field
      # in a resource.
      curPath = ""
      pathParts = path.split('/')
      pathParts.each_with_index do |part, i|
        curPath = curPath + part
        installs.each do |install|
          if (install['to'] || 'package') == curPath
            # Return the resource metadata and the path within the resource
            # that corresponds to the requested file
            if install['type'] == "git"
              redirectPath = (pathParts[i+1..-1] || []).join('/')
            else
              redirectPath = (pathParts[i..-1] || []).join('/')
            end
            return install, redirectPath
          elsif install['type'] == "application/zip"
            # For zip files, look at the unpack directory
            if (install['actions'] || {}).has_key?('unpack')
              destination = File.dirname(install['to'] || 'package')
              destination = File.absolute_path(File.join("/", destination, install['actions']['unpack']))
              if destination.start_with?("/")
                destination = destination[1..-1]
              end
              if destination == ""
                destination = "."
              end

              if (destination == "." && i == 0) || destination == curPath
                # Check inside the zip file
                innerPath = (pathParts[i+1..-1] || []).join('/')
                if (destination == "." && i == 0)
                  if innerPath == ""
                    innerPath = part
                  else
                    innerPath = File.join(part, innerPath)
                  end
                end
                resourceObj = Occam::Object.where(:uid => install['id']).first
                if !resourceObj.nil?
                  resourceObj.revision = install['revision']
                  if resourceObj.zip?
                    if resourceObj.exists?(innerPath)
                      return install, innerPath
                    end
                  end
                end
              end
            end
          end
        end
        curPath = curPath + "/"
      end

      return nil, path
    end

    def fileIsGroup?(path, git=nil)
      if (path.nil? || path == "" || path == "/")
        return true
      end

      if Occam::Backend.connected
        # TODO: handle filtering and ordering
        info = self.retrieveFileInfo(path)
        if info
          info[:type] == "tree"
        else
          nil
        end
      else
        if git.nil?
          git = self.git
        end

        if git != false
          info = self.retrieveFileInfo(path)
          info[:type] == "tree"
        else
          nil
        end
      end
    end

    def mimeType(path)
      if path.nil?
        return MIME::Types.type_for('bin').first
      end

      extname = File.extname(path)[1..-1]
      type = nil

      if extname
        type = MIME::Types.type_for(extname).first
      end

      if ["Makefile", "README", ".gitignore"].include? File.basename(path)
        type = MIME::Types.type_for('txt').first
      end

      type
    end

    def fileIsText?(path)
      if path.nil?
        return false
      end

      type = self.mimeType(path)

      if type && type.media_type == "text"
        true
      elsif type && TEXT_MEDIA_TYPES.include?(type.content_type)
        true
      elsif type && type.ascii?
        true
      else
        self.object_type == "text/plain"
      end
    end

    def fileIsImage?(path)
      if path.nil?
        return false
      end

      extname = File.extname(path)[1..-1]
      type = nil

      if extname
        type = MIME::Types.type_for(extname).first
      end

      if type && type.media_type == "image"
        true
      else
        false
      end
    end

    def fileIsBinary?(path)
      if path.nil?
        return false
      end

      extname = File.extname(path)[1..-1]
      type = nil

      if extname
        type = MIME::Types.type_for(extname).first
      end

      if type && type.binary?
        true
      else
        false
      end
    end

    # Returns a list of commits
    def commits(forPath=nil)
      if Occam::Backend.connected
        Occam::Backend.commits(self.uid, self.revision, forPath)
      elsif self.git != false
        self.git.commits(forPath)
      else
        nil
      end
    end

    def initialize(revision, options = {}, *args)
      @backed = false
      @hasDatapoints = nil

      setObjectInfoTo = nil
      if options.has_key? :objectInfo
        @git = false
        @objectInfo = options[:objectInfo]
        setObjectInfoTo = @objectInfo
        options.delete :objectInfo
      end

      super options, *args

      if setObjectInfoTo
        @objectInfo = setObjectInfoTo
      end
    end

    # Creates an unpersisted Occam::Object to wrap an object info hash
    def self.fromObjectInfo(objectInfo)
      hash = {
        :uid              => objectInfo['id'],
        :name             => objectInfo['name'],
        :object_type      => objectInfo['type'],
        :object_type_safe => self.slugFor(objectInfo['type']),
        :subtype          => objectInfo['subtype'],
        :description      => objectInfo['description'],
        :organization     => objectInfo['organization'],

        :objectInfo       => objectInfo
      }

      if objectInfo.has_key? 'revision'
        revision = objectInfo['revision']
        hash[:revision] = objectInfo['revision']
      end

      Occam::Object.new(revision, hash)
    end

    # Yields the Object for the backed object (git repository, etc)
    def backed(revision=nil)
      revision ||= "HEAD"

      ret = Occam::Object.where(:uid => self.uid).first
      gitPath = self.gitPath
      objectInfo = self.objectInfo
      if Backend.connected
        ret.instance_eval do
          @git = false
          @backed = true
          self[:revision] = revision
          @objectInfo = objectInfo
        end
      elsif File.exists?(gitPath)
        ret.instance_eval do
          @git = Occam::Git.new(gitPath, revision)
          @backed = true
          self[:revision] = @git.revision
          @objectInfo = objectInfo
        end
      end

      ret
    end

    # Authorizations
    def can_view?(person)
      if (self.objectInfo()['type'] == 'workset')
        return self.workset.can_view?(person)
      end

      workset = self.worksetBelongsTo

      if workset.nil?
        return self.private
      end

      self.worksetBelongsTo.can_view?(person)
    end

    def can_edit?(person)
      if (self.objectInfo()['type'] == 'workset')
        return self.workset.can_edit?(person)
      end
      self.worksetBelongsTo != nil && self.worksetBelongsTo.can_edit?(person)
    end

    # Returns a workflow object that represents the workflow of this object
    # Useful for visualizing a VM's structure and looking at its provenance.
    def toWorkflow
      workflow = {}
      workflow['connections'] = []
      workflow['tail'] = []

      def rakeConnections(connections, destination, inputs, parentIndex)
        inputs.each do |object|
          # Get this connection's index
          index = connections.length

          connection = {}
          connection['index'] = index
          connection['object'] = object
          connection['connections'] = []
          connection['object_realized'] = Occam::Object.where(:uid => connection['object']['id']).first
          if parentIndex > -1
            # Point to parent
            connection['to'] = parentIndex
          end

          # Add to global workflow connection list
          connections << connection

          destination << connection

          if object.has_key?('input')
            rakeConnections(connections, connection['connections'], object['input'], index)
          end
        end
      end

      if self.objectInfo.has_key?('input')
        rakeConnections(workflow['connections'], workflow['tail'], self.objectInfo['input'], -1)
      end

      workflow
    end

    def provenanceToWorkflow
      workflow = {}
      workflow['connections'] = []
      workflow['tail'] = []

      def rakeGenerators(connections, destination, generator, parentIndex)
        if generator.nil?
          return
        end

        def addConnection(connections, destination, object, index)
          if object
            connection = {}
            connection['index'] = index
            connection['object'] = object
            connection['connections'] = []
            connection['object_realized'] = Occam::Object.where(:uid => connection['object']['id']).first
            connection['to'] = index - 1

            # Add to global workflow connection list
            connections << connection

            destination << connection

            return connection['connections']
          end

          return destination
        end

        if generator.has_key?('task') && generator.has_key?('object')
          destination = addConnection(connections, destination, generator['object'],   parentIndex + 1)
          destination = addConnection(connections, destination, generator['task'], parentIndex + 2)

          # TODO: I'm always worried about recursion and limits and infinite looping
          if generator['object'].has_key?('generator')
            rakeGenerators(connections, destination, generator['object']['generator'], parentIndex + 2)
          elsif generator['task'].has_key?('generator')
            rakeGenerators(connections, destination, generator['task']['generator'], parentIndex + 2)
          end
        else
          destination = addConnection(connections, destination, generator, parentIndex + 1)
          if generator.has_key?('generator')
            rakeGenerators(connections, destination, generator['generator'], parentIndex + 1)
          end
        end
      end

      workflow['tail'] << {
        'index'  => 0,
        'object' => self.objectInfo,
        'connections' => [],
        'object_realized' => self,
        'to' => -1
      }
      workflow['tail'][0]['revision'] = self.revision

      workflow['connections'] << workflow['tail'][0]

      if self.objectInfo.has_key?('generator')
        rakeGenerators(workflow['connections'], workflow['tail'][0]['connections'], self.objectInfo['generator'], 0)
      end

      workflow
    end

    # Adds a page to the 'pages' element
    def addPage(name, workset)
      command = "append --base64 #{Base64.strict_encode64("pages")} #{Base64.strict_encode64("{\"title\": \"#{name}\", \"items\":[]}")} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)} -i #{Base64.strict_encode64("json")} "

      # Amend workset id/revision
      command << "--within #{Base64.strict_encode64(workset.uid)} "
      command << "--within-revision #{Base64.strict_encode64(workset.revision)} "

      Occam::Worker.perform(command).strip
    end

    # Deletes the page from the 'pages' element
    def removePage(pageIndex, workset)
      command = "set --base64 #{Base64.strict_encode64("pages.#{pageIndex}")} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)} "

      # Amend workset id/revision
      command << "--within #{Base64.strict_encode64(workset.uid)} "
      command << "--within-revision #{Base64.strict_encode64(workset.revision)} "

      Occam::Worker.perform(command).strip
    end

    # Finds the association for the object using system-wide associations or
    # defaults. You can pass along an object hash or an Occam::Object.
    # Returns the Occam::Association or nil if nothing was found.
    def self.associationFor(object)
      # TODO: Could certainly optimize this lookup
      if object.is_a? Hash
        type = object['type']
        subtype = object['subtype']
      else
        type = object.object_type
        subtype = object.subtype
      end

      associations = Occam::Association.where(:views_type => type,
                                              :views_subtype => subtype)
      if associations.length > 0
        return associations.first
      end

      associations = Occam::Association.where(:views_type => type)
      if associations.length > 0
        return associations.first
      end

      if object.is_a? Hash
        object = Occam::Object.fromObjectInfo(object)
      end

      viewers = object.viewers
      if viewers.length > 0
        return Occam::Association.new(:person_id       => nil,
                                      :views_type      => type,
                                      :views_subtype   => subtype,
                                      :occam_object_id => viewers.first.id)
      end

      nil
    end
  end
end
