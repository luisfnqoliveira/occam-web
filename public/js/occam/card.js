/* This file handles the general UI functionality of the website's Card
 * system.
 */

// Initialize any "cards" on the current page
$(function() {
  var cards = $('.content .card');

  cards.each(function() {
    new Occam.Card($(this));
  });
});

var initOccamCard = function(Occam) {
  var Card = Occam.Card = function(card, rebind) {
    this.card = card;

    if (rebind === undefined) {
      rebind = false;
    }

    if (card.hasClass("bound") && !rebind) {
      return this;
    }

    if (card.hasClass("separator")) {
      this.applySeparatorEvents();
    }

    if (card.hasClass("collapsable")) {
      this.applyCollapsableEvents();
    }

    card.addClass("bound");
  };

  /* This method applies events and logic to cards which can be collapsed and
   * expanded.
   */
  Card.prototype.applyCollapsableEvents = function() {
    var self = this;

    // Put children into a separate container
    if (self.card.children('.collapsed-card-container').length == 0) {
      var container = $('<div></div>').addClass("collapsed-card-container").attr('aria-hidden', 'false');
      self.card.children().each(function() {
        if ($(this).index() > 0 && !$(this).hasClass("help")) {
          container.append($(this));
        }
      });

      // Now we have a container of everything expect the header and the
      // help bubble!

      // Append that container to the card
      self.card.append(container);
    }

    // For that first header (h2 or h3), restyle it so it looks and acts
    // interactable. Add interactive events.
    var cardHeader = self.card.children('h2:first-child, h3:first-child');
    cardHeader.css({
      "cursor": "pointer"
    }).on('click.expand-card', function(event) {
      var span = $(this).children('span.expand');
      span.toggleClass('shown');
      // Get associated description div
      var card = span.parent().parent();

      var container = card.children('.collapsed-card-container');

      if (span.hasClass('shown')) {
        card.removeClass('collapsed');
        container.slideDown(250, function() {
          container.attr('aria-hidden', container.attr('aria-hidden') == "true" ? "false" : "true");
        });
        span.text("\u25be");
      }
      else {
        card.addClass('collapsed');
        container.slideUp(250, function() {
          container.attr('aria-hidden', container.attr('aria-hidden') == "true" ? "false" : "true");
        });
        span.text("\u25b8");
      }

      event.stopPropagation();
      event.preventDefault();
    })

    // Prepend a little arrow indicator next to the header
    if (cardHeader.children('span.expand').length == 0) {
      cardHeader.prepend($('<span>\u25be</span>').addClass('expand shown').css({
      }));
    }

    if (self.card.hasClass('collapsed') && self.card.children('h2:first-child, h3:first-child').children('.expand.shown').length > 0) {
      self.card.children('h2:first-child, h3:first-child').trigger('click');
    }
  };

  /* This method applies events and logic to cards which act as separators
   * between two other cards
   */
  Card.prototype.applySeparatorEvents = function() {
    var self = this;

    var clickY = 0;
    var moving = false;

    var page = self.card.prev();
    if (page.attr('aria-hidden') == 'true') {
      page = page.prev();
    }
    page.find('.separator-resize').each(function() {
      var height = page.height();
      height -= $(this).position().top;
      $(this).css({
        "height": height
      });
    });

    self.card.on('mousedown', function(event) {
      event.stopPropagation();
      event.preventDefault();

      if (event.which == 1) {
        clickY = event.pageY;
        moving = true;

        var draggingSurface = $('<div class="dragging-area"></div>').css({
          "width": "100%",
          "height": "100%",
          "z-index": "99999",
          "position": "fixed",
          "left": "0",
          "top": "0",
          "cursor": "ns-resize"
        });
        $('body').append(draggingSurface);

        var separator = $(this);
        separator.addClass('resizing');
        var page = separator.prev();
        if (page.attr('aria-hidden') == 'true') {
          page = page.prev();
        }
        var elements = page.find('.separator-resize');

        $(document).on('mousemove.paper-resize-widget', function(event) {
          event.stopPropagation();
          event.preventDefault();

          var deltaY = clickY - event.pageY;
          clickY = event.pageY;

          page.css({
            "height": page.height() - deltaY
          });

          elements.each(function() {
            $(this).css({
            "height": $(this).height() - deltaY
            });
          });
        });

        $(document).on('mouseup.paper-resize-widget', function(event) {
          event.stopPropagation();
          event.preventDefault();

          if (event.which == 1) {
            $(document).unbind('mousemove.paper-resize-widget');
            $(document).unbind('mouseup.paper-resize-widget');
            separator.removeClass('resizing');

            draggingSurface.remove();
          }
        });
      }
    });
  };
};
