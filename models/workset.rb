class Occam
  class Workset < ActiveRecord::Base
    require 'json'

    # Fields

    # id        - Unique identifier.

    # name      - The name of the experiment.

    # tags      - The semicolon separated list of tags.

    # Worksets are authored by many people
    has_many :authorships
    has_many :authors, :class_name => "Occam::Person",
                       :through => :authorships,
                       :source  => :person

    # People can collaborate on worksets
    has_many :collaboratorships
    has_many :collaborators, :class_name => "Occam::Person",
                             :through => :collaboratorships,
                             :source  => :person

    # Returns true when the given person is collaborating (includes owner)
    def is_collaborator?(person)
      !!(person and (self.authors.map(&:uid).include?(person.uid) or self.collaborators.map(&:uid).include?(person.uid)))
    end

    def is_author?(person)
      !!(person and self.authors.map(&:uid).include?(person.uid))
    end

    # We can allow a reviewer access to a workset
    has_many :review_capabilities

    def can_review?(revision=nil)
      revision ||= self.revision
      if self.can_view?(nil)
        true
      else
        self.review_capabilities.where(:revision => revision).any?
      end
    end

    def allow_review(person)
      # TODO: disallow people who cannot edit this workset from creating review tokens
      review = self.review_capabilities.where(:revision => self.revision)
      if review.empty?
        self.review_capabilities.create(:person => person, :revision => self.revision)
      end
    end

    # private - Boolean: whether or not the experiment is locked to creators/collaborators

    # Returns the LocalLink's for the given Person
    def local_links(person)
      if person
        Occam::LocalLink.where(:person_id => person.id, :workset_id => self.id)
      else
        []
      end
    end

    # Determines whether or not the person given can see this workset
    def can_view?(person)
      if self.private == 1
        self.is_collaborator? person
      else
        true
      end
    end

    # Determines whether or not the person given can edit this workset
    def can_edit?(person)
      self.is_author? person
    end

    # Many jobs can be spawned within a workset
    has_many :jobs

    # Has an object representation
    belongs_to :object,
               :foreign_key => :occam_object_id

    def update_info(info)
      # Encode all options as base64 for the sake of sanitization and so we
      # don't worry about escaping things
      def expandDataMap(hash, key="")
        # TODO: handle dots in base64
        hash.map do |k, v|
          new_key = key + "." + k
          if key == ""
            new_key = k
          end

          if v.is_a? Hash
            expandDataMap(v, new_key)
          else
            "#{Base64.strict_encode64(new_key.to_s)} #{Base64.strict_encode64(v.to_s)}"
          end
        end.join(" ")
      end

      data = expandDataMap(info)

      command = "set --base64 #{data} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)}"
      Occam::Worker.perform(command)

      self
    end

    def fork(name, person)
      # TODO: sanitize
      command = "clone #{name} --to #{self.uid} --revision #{self.revision} --store --as #{person.uid}"
      new_id = Occam::Worker.perform(command).strip

      Occam::Object.findWorkset(new_id)
    end

    # Removes the given item from the workset's 'contains' list
    def removeItem(itemIndex)
      command = "set --base64 #{Base64.strict_encode64("contains.#{itemIndex}")} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)}"

      Occam::Worker.perform(command).strip
    end

    # Adds a new group with the given name to the workset's 'contains' list
    def addGroup(name)
      command = "new --base64 #{Base64.strict_encode64("group")} #{Base64.strict_encode64(name)} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)}"

      Occam::Worker.perform(command).strip
    end

    # Adds a new experiment with the given name to the workset's 'contains' list
    def addExperiment(name)
      command = "new --base64 #{Base64.strict_encode64("experiment")} #{Base64.strict_encode64(name)} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)}"

      Occam::Worker.perform(command).strip
    end

    # Adds a new object with the given name/type to the workset's 'contents' list
    def addObject(name, type="object")
      type = type || "object"

      command = "new --base64 #{Base64.strict_encode64(type)} #{Base64.strict_encode64(name)} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)}"

      Occam::Worker.perform(command)
    end

    # Rewrite object helper method to give us a static version of the object.
    alias_method :object_current, :object
    def object
      @object ||= self.object_current
    end

    def method_missing(*args, &block)
      if self.object
        self.object.send(*args, &block)
      else
        super(*args, &block)
      end
    end
  end
end
