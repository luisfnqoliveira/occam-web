class Occam
  get '/search' do
    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    case format
    when 'application/json'
      {}.to_json
    when 'text/html'
      render :haml, :"search/index", :locals => {
      }
    end
  end

  post '/search' do
    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    query = params["search"]

    objects = []
    types   = []

    if params["types"] == "on"
      types = Object.basic_type_search(query)
    end

    if params["objects"].nil? && params["types"].nil? || params["objects"] == "on"
      objects = Object.basic_name_search(query)

      if not params["type"].blank?
        objects = objects.where(:object_type => params["type"])
      end

      if not params["views"].blank?
        objects = objects.where(:id => Occam::Viewer.where(:views_type => params["views"]).select(:occam_object_id))
      end

      providerQuery = nil

      if not params["environment"].blank?
        providerQuery = (providerQuery || Occam::Provider).where(:environment => params["environment"])
      end

      if not params["architecture"].blank?
        providerQuery = (providerQuery || Occam::Provider).where(:architecture => params["architecture"])
      end

      if providerQuery
        objects = objects.where(:id => providerQuery.select(:occam_object_id))
      end

      objects.limit(25)
    end

    case format
    when 'application/json'
      {
        "types" => types,
        "objects" => objects,
      }.to_json
    when 'text/html'
      render :haml, :"search/results", :locals => {
        :types   => types,
        :objects => objects
      }
    end
  end
end
