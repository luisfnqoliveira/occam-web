class Occam
  # Experiment tabs
  get '/worksets/:workset_uuid/experiments/:uuid/contents/?' do
    params[:tab] = "contents"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/contents/?' do
    params[:tab] = "contents"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/experiments/:uuid/run/?' do
    params[:tab] = "run"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/run/?' do
    params[:tab] = "run"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/experiments/:uuid/output/?' do
    params[:tab] = "output"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/output/?' do
    params[:tab] = "output"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/experiments/:uuid/workflow/?' do
    params[:tab] = "workflow"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/workflow/?' do
    params[:tab] = "workflow"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/experiments/:uuid/metadata/?' do
    params[:tab] = "metadata"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/metadata/?' do
    params[:tab] = "metadata"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/experiments/:uuid/files/?' do
    params[:tab] = "files"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/files/?' do
    params[:tab] = "files"
    objectShow(params)
  end

  # History View
  get '/worksets/:workset_uuid/experiments/:uuid/history/?' do
    objectHistory(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/history/?' do
    objectHistory(params)
  end
  get '/worksets/:workset_uuid/experiments/:uuid/history.json' do
    objectHistory(params, 'application/json')
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/history.json' do
    objectHistory(params, 'application/json')
  end

  # Show experiment configuration
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/connections/:connection_index/configurations/:configuration_index/?' do
    objectConfigurationShow(params)
  end
  get '/worksets/:workset_uuid/experiments/:uuid/connections/:connection_index/configurations/:configuration_index/?' do
    objectConfigurationShow(params)
  end

  # Show object
  get '/worksets/:workset_uuid/experiments/:uuid/?' do
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/?' do
    objectShow(params)
  end

  # Show a specific experiment's tabulated results at the given revision
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/graphs/new' do
    experiment = Occam::Object.findExperiment(params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]

    if params["revision"]
      experiment.revision = params["revision"]
    end

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      object_info = experiment.objectInfo

      workset = Occam::Object.findWorkset(params[:workset_uuid])
      if workset.nil?
        status 404
        return
      end

      workset.revision = params[:workset_revision]

      if object_info.has_key? 'belongsTo'
        belongs_to = object_info['belongsTo']

        code = belongs_to[belongs_to.length - 36..-1]
        type = belongs_to[0..-38]

        if type == 'workset'
          workset = workset || Occam::Object.findWorkset(belongs_to)
        elsif type == 'group'
          parent = Occam::Object.findGroup(belongs_to)
        end
      end

      if parent and not workset
        # TODO: oh god
      end

      outputs = experiment.retrieveInvocationData['output'] || []
      outputs = outputs.map do |output|
        output['object']
      end.select do |output|
        output['type'] == "application/json"
      end

      render :haml, :"results/graph_builder", :locals => {
        :object_info   => object_info,
        :head_view     => false,
        :local         => false,
        :help          => params["help"],
        :person        => workset.authors.first, # TODO: fix
        :workset       => workset,
        :group         => parent,
        :recipe        => nil,
        :experiment    => experiment,
        :job           => nil, # TODO: it is a set of jobs
        :job_info      => nil,
        :workflow      => nil,
        :forked_from   => nil,
        :forked_us     => nil,
        :authors       => [],
        :collaborators => [],
        :revision      => revision,
        :graphs        => [],
        :groups        => [],
        :outputs       => outputs,
        :experiments   => []
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Renders a form to fork an experiment to some workset.
  get '/experiments/:uuid/:revision/fork' do
    experiment = Occam::Object.findExperiment(params[:uuid])
    uuid  = params[:uuid]

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      revision = params[:revision]
      experiment.revision = revision

      render :haml, :"experiments/fork", :locals => {
        :errors           => nil,
        :current_revision => experiment.revision,
        :revision         => revision,
        :help             => params["help"],
        :recipe           => nil,
        :workset          => nil,
        :worksets         => current_person.worksets,
        :experiment       => experiment
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Forks an experiment (or updates form for no-javascript use)
  post '/experiments/:uuid/:revision/fork' do
    experiment = Occam::Object.findExperiment(params[:uuid])
    uuid = params[:uuid]

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      revision = params[:revision]
      experiment.revision = revision

      workset = nil
      group = nil

      to = Occam::Object.findObject(params["to"])
      if to.is_a? Occam::Workset
        workset = to
      elsif to.is_a? Occam::Group
        group = to
      end

      if params["update"]
        worksets = nil
        groups   = nil

        if workset.nil? && group.nil?
          worksets = current_person.worksets
        elsif workset.nil?
          groups = group.groups
        else
          groups = workset.groups
        end

        render :haml, :"experiments/fork", :locals => {
          :errors           => nil,
          :current_revision => experiment.revision,
          :revision         => revision,
          :help             => params["help"],
          :recipe           => nil,
          :worksets         => worksets,
          :groups           => groups,
          :to               => group || workset,
          :experiment       => experiment
        }
      else
        # Fork to given workset/group etc
        experiment = experiment.fork(params["name"], group || workset, current_person)
        workset = to.worksetBelongsTo()
        if experiment.nil?
          # TODO: Error
        else
          redirect "/worksets/#{workset.uid}/experiments/#{experiment.uid}"
        end
      end
    else
      # Unauthorized
      status 406
    end
  end

  # Edit a specific experiment configuration at the given revision
  post '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/configurations/:index' do
    experiment = Occam::Object.findExperiment(params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]

    if params["revision"]
      experiment.revision = params["revision"]
    end

    workset = Occam::Object.findWorkset(params[:workset_uuid])
    if workset.nil?
      status 404
      return
    end

    if experiment.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_person)

      object_info = experiment.objectInfo

      if experiment.revision == revision
        redirectBase = true
      else
        redirectBase = false
      end

      if workset.revision == params[:workset_revision]
        redirectWorksetBase = true
      else
        redirectWorksetBase = false
      end

      workset.revision = params[:workset_revision]

      connections = experiment.workflow["connections"]

      applies = params.keys.select do |key|
        key.start_with? "apply-recipe"
      end.first

      if applies
        recipe_index = applies[13..-1]

        connection_index, configuration_index = recipe_index.split('-').map(&:to_i)

        recipe_index = params["recipe-#{recipe_index}"].to_i

        # Apply the recipe
        # Ignore all other data
        puts "connection: #{connection_index}"
        puts "configurat: #{configuration_index}"
        puts "recipe_ind: #{recipe_index}"

        connection = connections[connection_index.to_i]
        object     = connection["object_realized"]
        info       = object.objectInfo
        recipes    = info['configurations'][configuration_index]['recipes']
        recipe_info = recipes[recipe_index]
        recipe_data = object.retrieveJSON(recipe_info['file'])

        # Form the data parameter with that recipe
        data = {
          connection_index => {
            configuration_index => recipe_data
          }
        }
      else
        data = params["data"]

        Configuration.decode_base64(data)
      end

      data ||= {}

      files = []
      if params["files"]
        files = params["files"]

        Configuration.decode_base64(files, level = 1)
      end

      # Update configuration
      data.each do |connection_index, connection_data|
        # Pull object information
        connection = connections[connection_index.to_i]
        object     = connection["object_realized"]
        connection_data.each do |configuration_index, configuration_data|
          experiment.revision = experiment.configure(connection_index, configuration_data, workset, [])
        end
      end

      # Configure files
      if not files.empty?
        files.each do |connection_index, connection_files|
          experiment.revision = experiment.configure(connection_index, {}, workset, connection_files.keys)
        end
      end

      redirect_url = "/worksets/"

      if redirectWorksetBase
        redirect_url << "#{workset.uid}/"
      else
        redirect_url << "#{workset.uid}/#{workset.revision}/"
      end

      redirect_url << "experiments/"

      if redirectBase
        redirect_url << uuid
      else
        redirect_url << "#{uuid}/#{revision}"
      end

      redirect(redirect_url)
    else
      # Unauthorized
      status 406
    end
  end

  # Run the experiment
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/queue_job' do
    experiment = Occam::Object.findExperiment(params[:uuid])
    revision   = params[:revision]
    uuid       = params[:uuid]

    if experiment && params["revision"]
      experiment.revision = params["revision"]
    end

    if experiment.nil?
      status 404
    # Authorize
    elsif experiment.can_view?(current_person)
      object_info = experiment.objectInfo

      workset = Occam::Object.findWorkset(params[:workset_uuid])
      if workset.nil?
        status 404
        return
      end

      workset.revision = params[:workset_revision]

      # Queue work
      experiment.run(workset, current_person)

      redirect "/worksets/#{workset.uid}/#{workset.revision}/experiments/#{experiment.uid}/#{experiment.revision}/run"
    else
      # Unauthorized
      status 406
    end
  end
end
