# These routes download 'file' resources and raw data from the data store
# The data store is generally in the 'store' path (~/.occam/store/xx/yy/xxyy...

class Occam
  options '/resources/file/:uuid/:revision/raw/?' do
    handleCORS()
    if !Occam::Filesystem.dataExists?(params[:uuid], params[:revision])
      status 404
    end
  end
  get '/resources/file/:uuid/:revision/raw/?' do
    content_type "application/octet-stream"

    embed = false
    if params.has_key? "embed"
      embed = true
    end

    if embed
      headers["X-Frame-Options"] = ""
    end

    handleCORS()
    Occam::Filesystem.retrieveData(params[:uuid], params[:revision])
  end
end
