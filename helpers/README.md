This directory holds methods that will be visible to the HTML (haml) pages that add helpful, sophisticated rendering of HTML or for advanced logic about permissions, etc.

* `accept.rb` - Controller helper that allows us to make routes conditional on their accepted content type.
* `cache.rb` - Controller helper that gives us a quick an easy way to control very generic caching rules on some routes.
* `configurator.rb` - The renderer that turns configuration schemata into HTML forms.
* `results.rb` - The renderer that turns output JSON into structured HTML.
* `roles.rb` - Controller helper that allows certain routes to be conditional on whether or not the logged in Person has a particular role.
* `schema_helper.rb` - The rendered that turns a schema into structured HTML for viewing.
* `session.rb` - Controller/View helper related to the logged-in account. For one: `current_person` which is a method that can yield the currently logged in Person.
