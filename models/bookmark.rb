class Occam
  class Bookmark < ActiveRecord::Base
    # Fields

    # occam_object_id - The object that we bookmarked
    belongs_to :object, :foreign_key => :occam_object_id

    # person_id       - The person that owns the bookmark
    belongs_to :person

    # revision        - The revision (if any) that was specifically bookmarked

    # published       - DateTime when object was originally published
    # updated         - DateTime when object was updated
    def update_timestamps
      now = Time.now.utc
      self[:published] ||= now if !persisted?
      self[:updated]     = now
    end
    before_save :update_timestamps
  end
end
