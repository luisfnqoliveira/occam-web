/* Object Builder source.
 */

/* This class represents a mutable version of
 * a potential OCCAM object.
 */
function ObjectBuilder() {
}

/* Yields the object notation that can be
 * imported or used to build/run the object.
 */
ObjectBuilder.prototype.build = function() {
  var hash = {};

  if (this._name != undefined) {
    hash.name = this._name;
  }

  if (this._type != undefined) {
    hash.type = this._type;
  }

  if (this._website != undefined) {
    hash.website = this._website;
  }

  if (this._authors != undefined) {
    hash.authors = this._authors;
  }

  if (this._tags != undefined) {
    hash.tags = this._tags;
  }

  if (this._license != undefined) {
    hash.license = this._license;
  }

  if (this._inputs != undefined) {
    hash.inputs = this._inputs;
  }

  if (this._install != undefined) {
    hash.install = this._install;
  }

  if (this._build != undefined) {
    hash.build = this._build;
  }

  return hash;
};

/* Either sets or retrieves the name of the object.
 */
ObjectBuilder.prototype.name = function(value) {
  if (value == undefined) {
    return this._name;
  }
  else {
    this._name = value;
    return this;
  }
};

/* Either sets or retrieves the type of the object.
 */
ObjectBuilder.prototype.type = function(value) {
  if (value == undefined) {
    return this._type;
  }
  else {
    this._type = value;
    return this;
  }
};

/* Either sets or retrieves the organization of the object.
 */
ObjectBuilder.prototype.organization = function(value) {
  if (value == undefined) {
    return this._organization;
  }
  else {
    this._organization = value;
    return this;
  }
};

/* Either sets or retrieves the website of the object.
 */
ObjectBuilder.prototype.website = function(value) {
  if (value == undefined) {
    return this._website;
  }
  else {
    this._website = value;
    return this;
  }
};

/* Either sets or retrieves the license of the object.
 */
ObjectBuilder.prototype.license = function(value) {
  if (value == undefined) {
    return this._license;
  }
  else {
    this._license = value;
    return this;
  }
};

/* Either sets or retrieves the authors of the object.
 */
ObjectBuilder.prototype.authors = function(value) {
  if (value == undefined) {
    return this._authors;
  }
  else {
    this._authors = value;
    return this;
  }
};

/* Adds a author to the object's list of authors. If the author already exists, the author
 * will not be added.
 */
ObjectBuilder.prototype.addAuthor = function(value) {
  if (this._authors == undefined) {
    this._authors = [];
  }

  this._authors[this._authors.length] = value;
  return this;
};

/* Removes the given author from the object's list of authors. If the author does not
 * exist, then nothing happens.
 */
ObjectBuilder.prototype.removeAuthor = function(value) {
  if (this._authors == undefined) {
    this._authors = [];
  }

  var index = this._authors.indexOf(value);
  if (index > -1) {
    this._authors.splice(index, 1);
  }

  return this;
}

/* Either sets or retrieves the tags of the object.
 */
ObjectBuilder.prototype.tags = function(value) {
  if (value == undefined) {
    return this._tags;
  }
  else {
    this._tags = value;
    return this;
  }
};

/* Adds a tag to the object's list of tags. If the tag already exists, the tag
 * will not be added.
 */
ObjectBuilder.prototype.addTag = function(value) {
  if (this._tags == undefined) {
    this._tags = [];
  }

  this._tags[this._tags.length] = value;
  return this;
};

/* Removes the given tag from the object's list of tags. If the tag does not
 * exist, then nothing happens.
 */
ObjectBuilder.prototype.removeTag = function(value) {
  if (this._tags == undefined) {
    this._tags = [];
  }

  var index = this._tags.indexOf(value);
  if (index > -1) {
    this._tags.splice(index, 1);
  }

  return this;
};

ObjectBuilder.prototype.inputs = function() {
  return this._inputs;
};

ObjectBuilder.prototype.addInput = function(value) {
  return this;
};

/* Removes the given input from the object's list of inputs.
 */
ObjectBuilder.prototype.removeInput = function(value) {
  return this;
};

/* Yields the object's install metadata.
 */
ObjectBuilder.prototype.install = function() {
  return this._install;
};

/* Adds the given input to the object's list of install repos.
 */
ObjectBuilder.prototype.addInstallRepo = function(type, url, to) {
  if (this._install == undefined) {
    this._install = [];
  }

  install_data = {
    "to": to
  };
  install_data[type] = url;

  this._install[this._install.length] = install_data;

  return this;
};

/* Removes the given input from the object's list of install repos.
 */
ObjectBuilder.prototype.removeInstallRepo = function(type, url, to) {
  if (this._install == undefined) {
    this._install = [];
  }

  var index = this._install.indexOf({
    "type": type,
    "url": url,
    "to": to
  });
  if (index > -1) {
    this._install.splice(index, 1);
  }

  return this;
};

/* Either sets or retrieves the website of the object.
 */
ObjectBuilder.prototype.buildUsing = function(value) {
  if (this._build == undefined) {
    this._build = {};
  }

  if (value == undefined) {
    return this._build.using;
  }
  else {
    this._build.using = value;
    return this;
  }
};

ObjectBuilder.prototype.buildLanguage = function(value) {
  if (this._build == undefined) {
    this._build = {};
  }

  if (value == undefined) {
    return this._build.language;
  }
  else {
    this._build.language = value;
    return this;
  }
};

ObjectBuilder.prototype.buildScript = function(value) {
  if (this._build == undefined) {
    this._build = {};
  }

  if (value == undefined) {
    return this._build.script;
  }
  else {
    this._build.script = value;
    return this;
  }
};

ObjectBuilder.prototype.buildVersion = function(value) {
  if (this._build == undefined) {
    this._build = {};
  }

  if (value == undefined) {
    return this._build.version;
  }
  else {
    if (value == "") {
      delete this._build.version;
    }
    else {
      this._build.version = value;
    }
    return this;
  }
};

$(function() {
  // Collapse all cards
  $('.card.collapsable > h2 > span.expand').trigger('click');

  // Don't submit the form
  $('form').submit(function(event) {
    event.preventDefault();
    event.stopPropagation();

    return false;
  });

  // Rewrite text with label
  $('input.label').on('change keyup', function(event) {
    var new_label = $(this).val();
    if (new_label == "") {
      new_label = "Unnamed";
    }
    $(this).parent().parent().parent().parent().children('label').text(new_label);
  });

  var apply_events_to_remove_button = function(element) {
    // Animate color
    element.on('mouseenter', function(event) {
      $(this).animate({
        "background-color": "#ff5c5c"
      }, 150);
    }).on('mouseout', function(event) {
      $(this).stop(true).animate({
        "background-color": "#ffaaaa"
      }, 150);
    }).on('click', function(event) {
      $(this).parent().animate({
        "opacity": 0.0
      }, 250, function() {
        $(this).remove();
      });
    });
  };

  var apply_events_to_add_button = function(element) {
    // Animate color
    element.on('mouseenter', function(event) {
      $(this).animate({
        "background-color": "hsl(120, 50%, 50%)"
      }, 150);
    }).on('mouseout', function(event) {
      $(this).stop(true).animate({
        "background-color": "#b3e6b3"
      }, 150);
    }).on('click', function(event) {
      // When adding values:
      var list = $(this).parent().parent();
      if (list.hasClass('values')) {
        // Get the text for this item
        var value = $(this).next().val().trim();
        if (value == "") {
          value = "Unnamed";
        }

        // Append a list item before with the text of the input
        var list_item = $('<li></li>').addClass('value field');
        var list_button = $('<div class="remove"></div>');
        list_item.append(list_button);
        list_item.append($('<label></label>').text(value));
        apply_events_to_remove_button(list_button);
        $(this).parent().before(list_item);
      }
      else if (list.hasClass('validations')) {
        // Get the type for this item
        var value = $(this).next().find("option:selected").text().trim();

        // Append a list item before with the text of the input
        var list_item = $('<li></li>').addClass('value field');
        var list_button = $('<div class="remove"></div>');
        list_item.append(list_button);
        list_item.append($('<label></label>').text(value));
        list_item.append($('<input type="text"></input>'));
        list_item.append($('<div class="dots"></div>'));
        apply_events_to_remove_button(list_button);
        $(this).parent().before(list_item);
      }
      else if (list.hasClass("configuration-group")) {
        // Get the text for this item
        var value = $(this).next().val().trim();
        if (value == "") {
          value = "Unnamed";
        }

        // Add a new key
        var list_item = $('<li></li>');
        var list_button = $('<div class="remove"></div>');
        list_item.append(list_button);
        list_item.append($('<label></label>').text(value));
        list_item.append($('<span class="expand">[+]</span>'));
        var type_select = $('<select></select>');
        type_select.append($('<option>int</option>'));
        type_select.append($('<option>float</option>'));
        type_select.append($('<option>string</option>'));
        type_select.append($('<option>enum</option>'));
        list_item.append($('<div class="select"></div>').append(type_select));
        list_item.append($('<div class="dots"></div>'));
        apply_events_to_remove_button(list_button);
        $(this).parent().before(list_item);
      }
    });
  };

  $('li > .add').each(function() {
    apply_events_to_add_button($(this));
  });

  $('li > .remove').each(function() {
    apply_events_to_remove_button($(this));
  });

  // Make the Enumerated Values section appear only when the 'enum' type is selected
  $('ul.configuration-group > li > .select > select').on('change', function() {
    if ($(this).children("option:selected").text().trim() == "enum") {
      // Ensure enum section is visible
      $(this).parent().parent().children('.parameters').find('li.field.enumerable_values').slideDown(150);
    }
    else {
      // Ensure enum section is hidden
      $(this).parent().parent().children('.parameters').find('li.field.enumerable_values').slideUp(150);
    }
  });

  // Instead of submitting the form, just click the add button.
  $('li > .add + *').on('keydown', function(event) {
    if (event.which == 13) {
      $(this).prev().trigger('click');
      $(this).val('');
    }
  });

  var build_link = $('<a href="#">Compose</a>').on('click', function(event) {
    event.preventDefault();
    event.stopPropagation();

    // Build object
    var builder = new ObjectBuilder();
    builder.name($('input#name').val().trim())
           .type($('input#type').val().trim())
           .license($('input#license').val().trim());

    // Add all object input data
    $('ul.inputs li.input').each(function() {
    });

    // Add all install repositories
    $('ul.repos li.repo').each(function() {
      var type = $(this).find('.type p').text().trim();
      var url  = $(this).find('.url  p').text().trim();
      var to   = $(this).find('.to   p').text().trim();

      builder.addInstallRepo(type, url, to);
    });

    // Add build metadata
    builder.buildLanguage($('select#build_language').children("option:selected").text().trim());
    builder.buildVersion($('input#build_version').val().trim());
    builder.buildUsing("ubuntu-base-2014-06");
    builder.buildScript("build." + $('select#build_language option:selected').data("extension"));

    console.log(builder.build());
    console.log(JSON.stringify(builder.build()));
  });

  $('.card').first().append(build_link);

  $('.card.terminal').attr('aria-hidden', 'false');
  $('.terminal').attr('aria-hidden', 'false');

  var ws = null;
  if (ws === null) {
    ws = startWebsocket();
  }
  ws.attachTerminal("build", "", $('.build-test.terminal'));

  var old_link_handler = $('.build-test').children('a').on('click');

  $('.build-test').children('a').unbind('click');

  $('.build-test').children('a').on('click', function(event) {
    event.preventDefault();
    event.stopPropagation();

    // Build object
    var builder = new ObjectBuilder();
    builder.name($('input#name').val().trim())
           .type($('input#type').val().trim())
           .license($('input#license').val().trim());

    // Add all object input data
    $('ul.inputs li.input').each(function() {
    });

    // Add all install repositories
    $('ul.repos li.repo').each(function() {
      var type = $(this).find('.type p').text().trim();
      var url  = $(this).find('.url  p').text().trim();
      var to   = $(this).find('.to   p').text().trim();

      builder.addInstallRepo(type, url, to);
    });

    // Add build metadata
    builder.buildLanguage($('select#build_language').children("option:selected").text().trim());
    builder.buildVersion($('input#build_version').val().trim());
    builder.buildUsing("ubuntu-base-2014-06");
    var build_filename = "build." + $('select#build_language option:selected').data("extension");
    builder.buildScript(build_filename);

    ws.updateTerminal("build", builder.build());

    $(this).remove();

    ws.openTerminal("build", {"object.json": builder.build(), "buildFilename": build_filename, "buildCode": $('#build_code').val()}, $(this).parent());
  });
});
