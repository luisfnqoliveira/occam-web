class Occam
  # Lists the providers that exist on the system
  # Right now this is more of a debugging route than a real one.
  get '/providers' do
    render :haml, :"providers/index", :locals => {
      :providers => Occam::Provider.all
    }
  end
end
