class Occam
  class Experiment < ActiveRecord::Base
    require 'json'
    require 'pp'
    require 'digest'

    # Fields

    # id        - Unique identifier.

    # runs      - The individual workflow passes.
    has_many :runs

    # Has an object representation
    belongs_to :object,
               :foreign_key => :occam_object_id

    # Many jobs can be spawned from an experiment
    has_many :jobs

    # private - Boolean: whether or not the experiment is locked to creators/collaborators

    # Cancel the experiment by unqueuing all attached jobs
    def cancel
      self.runs.each do |run|
        run.cancel
      end
    end

    # Run the experiment by spawning run jobs
    def run(workset, person)
      # TODO: sanitize
      options = ""

      command = "run --dispatch --to #{self.uid} --revision #{self.revision} --within #{workset.uid} --within-revision #{workset.revision} --as #{person.uid}"

      Occam::Worker.perform(command)
    end

    def status
      # Assume it is finished
      status = :finished

      order = [:unqueued, :queued, :running, :failed, :finished]

      self.runs.map do |run|
        job_status = run.status

        if order.index(job_status) < order.index(status)
          status = job_status
        end
      end

      status
    end

    def fork(name, to, person)
      # TODO: sanitize
      command = "clone #{name} --to #{self.uid} --revision #{self.revision} --within #{to.uid} --within-revision #{to.revision} --store --as #{person.uid}"
      new_id = Occam::Worker.perform(command).strip

      experiment = Occam::Object.findExperiment(new_id)
      experiment
    end

    def self.basic_search(term)
      Experiment.where('name LIKE ?', "%#{term}%").select([:name, :id])
    end

    def self.search_sim(name, tags, simulator, forked)
      query = Experiment.where('name LIKE ?', "%#{name}%")
      tags = tags.split ;
      tags.each do |tag|
        query = query.where('tags LIKE ?', "%#{tag}%")
      end
      if simulator > 0
        query = query.where('simulator_id = ?', "#{simulator}")
      end
     #query = query.where('person_id LIKE ?', "%#{person}%")
      if forked == 1
        query = query.where('forked_from_id IS NOT NULL')
      elsif forked == 2
        query = query.where('forked_from_id IS NULL')
      end
      query = query.select([:name, :id])
    end

    def self.search(params)
      data = {}
      if params["simulator_id"] != "___any___"
        simulator = Object.where(:type => :simulator).find_by(:id => params["simulator_id"].to_i)

        if params["data"] && params["data"].is_a?(Array)
          data = params["data"].first

          schema = simulator.input_schema
          Occam::Configuration.type_check(data, schema, nil, true)
        end
      end

      query = Experiment
      query = query.where('name LIKE ?', "%#{params["name"]}%") if params["name"] && params["name"] != ""

      tags = params["tags"] || ""

      tags = tags.split(';')
      tags.each do |tag|
        query = query.where('tags LIKE ?', "%;#{tag};%")
      end

      query = query.where('simulator_id = ?', "#{simulator.id}") if simulator
      query = query.where('forked_from_id IS NOT NULL') if params["forked"]

      # Configuration Facet Search
      # Find configurations in the document store and add the final sql query for those ids
      unless data.empty?
        configurations = Occam::Configuration.search(data)
        if configurations.count > 0
          query = query.where('configuration_document_id IN (?)', configurations.to_a.map{|e| e.id.to_s})
        else
          return []
        end
      end

      query = query.select([:name, :id])
    end

    def attach(object, workset, connection_index, created_object_type=nil, created_object_group=nil)
      # TODO: sanitize
      options = ""
      if created_object_group
        options = options + "--group #{created_object_group} "
      end

      if created_object_type
        command = "attach #{connection_index >= 0 ? connection_index : ""} #{options} --create #{created_object_type} --to #{self.uid} --revision #{self.revision} --within #{workset.uid} --within-revision #{workset.revision}"
      else
        command = "attach #{connection_index >= 0 ? connection_index : ""} #{options} --id #{object.uid} --object-revision #{object.revision} --to #{self.uid} --revision #{self.revision} --within #{workset.uid} --within-revision #{workset.revision}"
      end

      Occam::Worker.perform(command)
    end

    def detach(workset, connection_index)
      # TODO: sanitize
      options = ""
      command = "detach #{connection_index} #{options} --to #{self.uid} --revision #{self.revision} --within #{workset.uid} --within-revision #{workset.revision}"
      Occam::Worker.perform(command)
    end

    def update_info(info, workset)
      # Encode all options as base64 for the sake of sanitization and so we
      # don't worry about escaping things
      def expandDataMap(hash, key="")
        # TODO: handle dots in base64
        hash.map do |k, v|
          new_key = key + "." + k
          if key == ""
            new_key = k
          end

          if v.is_a? Hash
            expandDataMap(v, new_key)
          else
            "\"#{Base64.strict_encode64(new_key.to_s)}\" \"#{Base64.strict_encode64(v.to_s)}\""
          end
        end.join(" ")
      end

      data = expandDataMap(info)

      command = "set --base64 #{data} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)} --within #{Base64.strict_encode64(workset.uid)} --within-revision #{Base64.strict_encode64(workset.revision)}"
      Occam::Worker.perform(command)

      self
    end

    def configure(connection_index, data, workset, files=[])
      # Encode all options as base64 for the sake of sanitization and so we
      # don't worry about escaping things
      def expandDataMap(hash, key="")
        # TODO: handle dots in base64
        hash.map do |k, v|
          new_key = key + "." + k
          if key == ""
            new_key = k
          end

          if v.is_a? Hash
            expandDataMap(v, new_key)
          else
            "\"#{Base64.strict_encode64(new_key.to_s)}\" \"#{Base64.strict_encode64(v.to_s)}\""
          end
        end.join(" ")
      end

      data = expandDataMap(data)

      files_opts = ""
      files_opts = files.map{|path| "--file #{Base64.strict_encode64(path)}"}.join(" ")

      command = "configure --base64 #{Base64.strict_encode64(connection_index.to_s)} #{data} --to #{Base64.strict_encode64(self.uid)} --revision #{Base64.strict_encode64(self.revision)} --within #{Base64.strict_encode64(workset.uid)} --within-revision #{Base64.strict_encode64(workset.revision)} --remove-files #{files_opts}"
      Occam::Worker.perform(command).strip
    end

    def workflow
      info = self.objectInfo

      workflow = info['workflow'] || {}
      workflow['connections'] = workflow['connections'] || []

      workflow['connections'].each_with_index do |connection, index|
        if connection.has_key? 'object'
          connection['index'] = index
          connection['input'] = {}
          if connection['object']['id']
            connection['object_realized'] = Occam::Object.where(:uid => connection['object']['id']).first
            if connection['object'].has_key? 'revision'
              connection['object_realized'].revision = connection['object']['revision']
            end
          end
          connection['connections'] = []
        end
      end

      workflow['connections'].each_with_index do |connection, index|
        if connection.has_key? 'object'
          connection['connections'] = workflow['connections'].select do |sub_connection|
            sub_connection['to'] == connection['index']
          end

          # Determine input connection
          connection['connections'].each do |sub_connection|
            if connection['object_realized']
              if not (connection['input_types'] || []).include?(sub_connection['object']['type'])
                input_info = connection['object_realized'].inputs_info(sub_connection['object']['type']).first
                if input_info && input_info.has_key?("max")
                  connection['input_max'] = (connection['input_max'] || 0) + (input_info['max'] || 0)
                  connection['input_types'] = (connection['input_types'] || []).push(sub_connection['object']['type'])
                end
              end
            end
          end
        end
      end

      workflow
    end

    def tail_connections
      workflow = self.workflow
      workflow['tail'] = workflow['connections'].select do |connection|
        (connection['to'] || -1) == -1
      end
    end

    # Rewrite object helper method to give us a static version of the object.
    alias_method :object_current, :object
    def object
      @object ||= self.object_current
    end

    def method_missing(*args, &block)
      if self.object
        self.object.send(*args, &block)
      else
        super(*args, &block)
      end
    end
  end
end
