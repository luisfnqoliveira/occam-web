require_relative "helper"
require_model "object"
require_model "workset"
require_model "person"
require_model "review_capability"

describe Occam::ReviewCapability do
  before :each do
    Time.stubs(:now).returns(Time.utc(2000, 1, 1, 0, 0, 0))

    object = Occam::Object.new(nil, :name => "Jane Smith", :uid => "person1", :objectInfo => {})
    object.save
    @person = Occam::Person.create(:username => "jane",
                                   :occam_object_id => object.id)

    object = Occam::Object.new(nil, :name => "Jim Smash", :uid => "person2", :objectInfo => {})
    object.save
    @author = Occam::Person.create(:username => "jim",
                                   :occam_object_id => object.id)

    object = Occam::Object.new(nil, :name => "workset", :objectInfo => {}, :private => 1, :revision => "CURRENT_REVISION")
    object.save
    @review_workset = Occam::Workset.create(:occam_object_id => object.id)

    @review_workset.review_capabilities.create(:person => @author,
                                               :revision => "CURRENT_REVISION")

    @review_capability = @review_workset.review_capabilities.first
  end

  describe "#update_timestamps" do
    before :each do
      Time.stubs(:now).returns(Time.utc(2010, 1, 1, 0, 0, 0))
    end

    it "should set the published time upon creation" do
      @review_capability.published.year.must_equal 2000
    end

    it "should not change the published time upon save" do
      @review_capability.save!
      @review_capability.published.year.must_equal 2000
    end
  end
end
