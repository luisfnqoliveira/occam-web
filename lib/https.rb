class Occam
  # This module implements the creation of a self-signed certificate for
  # providing nodes with HTTPS access for the pulling of resources.
  module HTTPS
    require 'openssl'

    CRT_FILENAME = "key/server.crt"
    KEY_FILENAME = "key/server.key"

    # Creates an RSA SSL key for the server.
    def self.createKey
      # TODO: configurations
      # TODO: pull out organization info from the System tag

      name = "/C=US/ST=Pennsylvania/L=Pittsburgh/O=University of Pittsburgh/OU=Unit/CN=deadreckoning.no-ip.org"

      ca  = OpenSSL::X509::Name.parse(name)
      key = OpenSSL::PKey::RSA.new(2048)
      crt = OpenSSL::X509::Certificate.new

      crt.version = 2
      crt.serial  = Random.new.rand(100000)
      crt.subject = ca
      crt.issuer  = ca
      crt.public_key = key.public_key
      crt.not_before = Time.now
      crt.not_after  = Time.now + (1 * 365 * 24 * 3600) # 1yr

      ef  = OpenSSL::X509::ExtensionFactory.new
      ef.subject_certificate = crt
      ef.issuer_certificate  = crt
      crt.extensions = [
        ef.create_extension("basicConstraints", "CA:TRUE", true),
        ef.create_extension("subjectKeyIdentifier", "hash")
      ]
      crt.add_extension ef.create_extension("authorityKeyIdentifier",
                                            "keyid:always,issuer:always")
      crt.sign key, OpenSSL::Digest::SHA256.new

      # Store key

      # create key directory
      if not File.exists?("key")
        Dir.mkdir("key")
      end

      # server.key
      File.open(KEY_FILENAME, 'w+') do |f|
        f.write(key.to_pem)
      end

      # server.crt
      File.open(CRT_FILENAME, 'w+') do |f|
        f.write(crt.to_pem)
      end
    end

    # If the key does not exist, create the key
    def self.ensureKey
      if not File.exists?("key/server.crt")
        Occam::HTTPS.createKey
      end
    end
  end
end
