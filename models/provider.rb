class Occam
  class Provider < ActiveRecord::Base
    # Fields

    # occam_object_id - the Object that this provider points toward
    belongs_to :object,
               :foreign_key => :occam_object_id

    # environment  - If specified, the environment this object provides
    # architecture - If specified, the architecture this object provides
    # capability   - If specified, the capability this object provides
  end
end
