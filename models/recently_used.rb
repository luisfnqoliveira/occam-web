class Occam
  # Records the dependency between one Object and another
  class RecentlyUsed < ActiveRecord::Base
    self.table_name = "recently_used"

    # Fields
    #
    # id

    # Associations
    belongs_to :person
    belongs_to :object,
               :foreign_key => :occam_object_id
  end
end
