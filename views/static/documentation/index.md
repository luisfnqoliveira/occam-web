# OCCAM System Documentation

Welcome to OCCAM.
Here you will find instructions and documentation on how to integrate OCCAM into your research.
If you are a simulator developer, look at our [Getting Started](getting_started) page.
On the left, you will find navigation for specific parts of the OCCAM specification.

## Introduction for Simulator Developers

OCCAM provides a sophisticated and standard interface for simulator interaction.
It does not just queue and manage running simulations, but also provides a means of configuring
and validating user input, and generating sets of experiments automatically.
OCCAM can also automatically generate graphs and visualizations from the resulting data.

To do this, OCCAM needs to know what data the simulator expects and generates and the form of that data.
Each simulator is described separately with a set of meta-data.

* `object.json`: An [Object Description](object) describes the basic meta-data for an object (simulator, benchmark, etc), such as name, authors, and a summary of what it does.
* `input_schema.json`: The [Input Schema](input_schema) specifies the configuration input.
* `output_schema.json`: The [Output Schema](output_schema) handles the representation of any possible resulting data.

The OCCAM system will then generate for each experiment an input file based upon the user input called [`input.json`](input).
The object/simulator may then read that input file natively, or must provide an auxiliary script to create the native configuration files it needs.

Generally, the object/simulator must provide a run script that will run an experiment using the `input.json`. Our dispatcher will run that script.

An object/simulator must also provide a complete automated script to build the object.

To see a working example of the files that provide a simulator in OCCAM, we have code repositories for several simulators:

* [DRAMSim2](https://bitbucket.org/occam/occam-dramsim2/src)
* [Sniper](https://bitbucket.org/occam/occam-sniper/src)
* [Manifold](https://bitbucket.org/occam/occam-manifold/src)
