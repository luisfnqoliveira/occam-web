class Occam
  get '/resources/store/:uuid/:revision' do
    # Return the path of the object in the local object store
    obj = Occam::Object.findObject(params[:uuid], params[:revision])
    file_path = obj.path('store')
    file_path = File.join(file_path, obj.revision, "data")
    send_file file_path
  end
end
