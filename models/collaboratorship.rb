class Occam
  # Records the dependency between one Object and another
  class Collaboratorship < ActiveRecord::Base
    # Fields
    #
    # id

    belongs_to :person
    belongs_to :workset
  end
end
