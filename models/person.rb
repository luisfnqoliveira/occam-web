class Occam
  class Person < ActiveRecord::Base
    require 'securerandom'
    require 'digest'
    require 'open-uri'
    require 'openssl'

    DEFAULT_GRAVATAR = "wilkie+occam@xomb.org"

    # A Person is attached to an Account, sometimes
    has_one :account

    # Fields

    # id              - Unique identifier.

    # username        - The name preferred for usernames

    # email           - The email for this person.

    # public_key      - The RSA public key

    # Returns the SSH key from the public key (PEM)
    def ssh_public_key
      key = SSHKey.new(self.public_key)
      key.ssh_public_key
    end

    # Returns the SSH2 key from the public key (PEM)
    def ssh2_public_key
      key = SSHKey.new(self.public_key)
      key.ssh2_public_key
    end

    # Returns an actively loaded RSA key from the public_key
    def rsa_public_key
      OpenSSL::PKey::RSA.new self.public_key
    end

    # People can have bookmarks
    has_many :bookmarks

    def bookmarked?(object)
      if object.respond_to? :object
        object = object.object
      end
      self.bookmarks.where(:occam_object_id => object.id).any?
    end

    def bookmarkFor(object)
      if object.respond_to? :object
        object = object.object
      end
      self.bookmarks.where(:occam_object_id => object.id).first
    end

    # People have a set of recently used objects
    has_many :recentlyUsed
    has_many :recentlyUsedObjects, :class_name  => "Occam::Object",
                                   :through     => :recentlyUsed,
                                   :source      => :object,
                                   :foreign_key => :occam_object_id

    # People can author worksets
    has_many :authorships
    has_many :worksets, :through => :authorships

    # People can collaborate on worksets
    has_many :collaboratorships
    has_many :collaborations, :class_name => "Occam::Workset",
                              :through    => :collaboratorships,
                              :source     => :workset

    # People can have more than one active session
    has_many :sessions

    # People have many runs of workflows
    has_many :runs

    # Has an object representation
    belongs_to :object,
               :foreign_key => :occam_object_id

    def dashboard_runs
      query = '"runs"."person_id" = :person_id AND (("runs"."id" IN (:status)) OR ("runs"."archived" = 0))'
      Occam::Run.where(query,
                       :person_id => self.id,
                       :status => Occam::Job.where(:status => "running").select(:run_id).distinct)
                .order('published DESC')
                .limit(10)
    end

    def running_runs
      # Find runs that have at least one Job with a 'running' status
      self.runs.where(:id => Occam::Job.where(:status => "running").select(:run_id).distinct)
    end

    def done_runs
      # Find runs where there is at least one job that isn't finished
      self.runs.where(:id => Occam::Job.where("NOT (:status)", :status => "finished").select(:run_id).distinct)
    end

    # People have jobs on the queue
    has_many :jobs

    # Determine the canonical display name
    def name
      if !self.object.name.blank?
        self.object.name
      else
        self.username
      end
    end

    # Determine the avatar image url
    def avatar_url(size)
      if !self.email.blank?
        "https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(self.email.strip.downcase)}?size=#{size}&d=#{URI::encode("https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(DEFAULT_GRAVATAR)}?size=#{size}")}"
      else
        Occam::Person.default_avatar(size)
      end
    end

    def self.default_avatar(size)
      if size >= 96
        "/images/person_large.png"
      else
        "/images/person.png"
      end
    end

    def addWorkset(name)
      # Spawn job
      command = "new --base64 #{Base64.strict_encode64("workset")} #{Base64.strict_encode64(name)} --as #{Base64.strict_encode64(self.uid)} --internal"
      Occam::Worker.perform(command).strip
    end

    def active_session_for?(nonce)
      self.sessions.where(:nonce => nonce).any?
    end

    def deactivate_session_for(nonce)
      self.sessions.where(:nonce => nonce).map(&:destroy)
    end

    def activate_session
      session = Occam::Session.create(:nonce => SecureRandom.hex(32))
      self.sessions << session
      self.save

      session
    end

    # Yields the Occam::Association (if any) for the given object
    # It will first look for associations for this Person and then
    # default to any without a tagged Person (System/Administrator
    # associations) and then return nil if none are found.
    # It will also look for ones with both a matching type and subtype
    # before listing those that only match by type.
    # You can pass an object info hash or an Occam::Object.
    def associationFor(object)
      # TODO: Could certainly optimize this lookup
      if object.is_a? Hash
        type = object['type']
        subtype = object['subtype']
      else
        type = object.object_type
        subtype = object.subtype
      end
      associations = Occam::Association.where(:views_type => type,
                                              :views_subtype => subtype,
                                              :person_id => self.id)
      if associations.length > 0
        return associations.first
      end

      associations = Occam::Association.where(:views_type => type,
                                              :views_subtype => subtype)
      if associations.length > 0
        return associations.first
      end

      associations = Occam::Association.where(:views_type => type,
                                              :person_id => self.id)
      if associations.length > 0
        return associations.first
      end

      associations = Occam::Association.where(:views_type => type)
      if associations.length > 0
        return associations.first
      end

      # Create a default Association, if possible
      if object.is_a? Hash
        object = Occam::Object.fromObjectInfo(object)
      end

      viewers = object.viewers
      if viewers.length > 0
        return Occam::Association.new(:person_id       => nil,
                                      :views_type      => type,
                                      :views_subtype   => subtype,
                                      :occam_object_id => viewers.first.id)
      end

      nil
    end

    def method_missing(*args, &block)
      if self.object
        self.object.send(*args, &block)
      else
        super(*args, &block)
      end
    end
  end
end
