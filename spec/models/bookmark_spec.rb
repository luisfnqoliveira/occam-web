require_relative "helper"
require_model "object"
require_model "person"
require_model "bookmark"

describe Occam::Bookmark do
  before :each do
    Time.stubs(:now).returns(Time.utc(2000, 1, 1, 0, 0, 0))

    @simulator = Occam::Object.new(nil, :uid => "SIMULATOR_UUID", :name => "simulator", :object_type => "simulator", :object_type_safe => "simulator", :objectInfo => {}, :private => 0, :revision => "CURRENT_REVISION")
    @simulator.save

    object = Occam::Object.new(nil, :name => "Jane Smith", :uid => "person1", :objectInfo => {})
    object.save
    @person = Occam::Person.create(:username => "jane",
                                   :occam_object_id => object.id)

    @bookmark = Occam::Bookmark.create(:person => @person,
                                       :object => @simulator)
  end

  describe "#update_timestamps" do
    before :each do
      Time.stubs(:now).returns(Time.utc(2010, 1, 1, 0, 0, 0))
    end

    it "should set the published time upon creation" do
      @bookmark.published.year.must_equal 2000
    end

    it "should set the updated time upon creation" do
      @bookmark.updated.year.must_equal 2000
    end

    it "should not change the published time upon save" do
      @bookmark.save!
      @bookmark.published.year.must_equal 2000
    end

    it "should change the updated time upon save" do
      @bookmark.save!
      @bookmark.updated.year.must_equal 2010
    end
  end
end
