class Occam
  require 'base64'

  # Presenter helpers
  def handleCORS(origin=nil)
    # Allow Same-Origin CORS Requests for AJAX components
    if origin == "*"
      domain = "*"
    else
      origin = origin || request["Origin"] || request.env["HTTP_ORIGIN"] || "null"
      domain = "#{request.scheme}://#{request.host_with_port}"
    end

    if ["null", domain].include? origin
      headers "Access-Control-Allow-Origin" => origin,
              "Access-Control-Allow-Headers" => "x-requested-with, range, accept-ranges, accept, Accept-Ranges, Content-Type",
              "Access-Control-Expose-Headers" => "Accept-Ranges, Content-Encoding, Content-Length, Content-Range, Content-Type, Allow",
              "Access-Control-Allow-Methods" => "GET,OPTIONS",
              "Origin" => origin
    end
  end

  def objectURL(object, path=nil, options={})
    @params ||= {}
    if @params[:params] && !options.empty?
      options = determineObjects(@params[:params].merge(options)).merge(options)
    else
      options = @params.merge(options)
    end

    if path
      if not path.start_with? "/"
        path = "/#{path}"
      end
    end

    object  = options[:object]
    workset = options[:workset]

    case object.objectInfo()['type']
    when 'experiment'
      type = "experiments"
    when 'group'
      type = "groups"
    when 'person'
      type = "people"
    when 'workset'
      type = "worksets"
    else
      type = "objects"
    end

    query = []

    if @params[:params] && @params[:params].has_key?("review")
      query << "review"
    end

    if @params[:params].has_key?("embed")
      query << "embed"
    end

    if options[:query]
      if !options[:query].is_a?(Array)
        options[:query] = [options[:query]]
      end
      query = query.concat(options[:query])
    end

    if query.any?
      query = "?#{query.join('&')}"
    else
      query = ""
    end

    if object
      if object.objectInfo()['type'] == "workset"
        workset = object
      end
      if workset
        if workset != object
          if options[:full] || options[:params][:revision] || options[:params][:workset_revision]
            "/worksets/#{workset.uid}/#{workset.revision}/#{type}/#{object.uid}/#{object.revision}#{path}#{query}"
          else
            "/worksets/#{workset.uid}/#{type}/#{object.uid}#{path}#{query}"
          end
        else
          if options[:full] || options[:params][:revision] || options[:params][:workset_revision]
            "/worksets/#{workset.uid}/#{workset.revision}#{path}#{query}"
          else
            "/worksets/#{workset.uid}#{path}#{query}"
          end
        end
      else
        if options[:full] || options[:params][:revision]
          "/#{type}/#{object.uid}/#{object.revision}#{path}#{query}"
        else
          "/#{type}/#{object.uid}#{path}#{query}"
        end
      end
    end
  end

  def determineObjects(params)
    if params.is_a?(Hash) && !params.has_key?('captures')
      return params
    end

    ret = {}

    ret[:params] = params

    if params[:uuid]
      object = Occam::Object.where(:uid => params[:uuid])
    else
      object = nil
    end

    if params[:object_type]
      object = object.where(:object_type_safe => params[:object_type])
    end

    object = object.first

    if object.nil?
      status 404
      return ret
    end

    case object.objectInfo()['type']
    when "workset"
      object = object.workset
    when "experiment"
      object = object.experiment
    when "person"
      object = object.person
    when "group"
      object = object.group_object
    end

    if params[:backed]
      object = object.backed(params[:backed_revision])
    end

    ret[:storedRevision] = object.revision
    if object.resource?
      ret[:storedRevision] = params[:revision]
    end

    if params[:revision]
      object.revision = params[:revision]
    end

    if params[:workset_uuid]
      workset = Occam::Object.findWorkset(params[:workset_uuid])

      if workset.nil?
        status 404
        return ret
      end

      if params[:workset_revision]
        workset.revision = params[:workset_revision]
      end

      ret[:workset] = workset
    elsif object.objectInfo()['type'] == "workset"
      workset = object
      ret[:workset] = workset
    end

    # Authorizations
    # TODO: security: timing attack on this check can find where some
    #       worksets/experiments are. Maybe you could overwhelm their
    #       UUIDs before they go public?
    if params.has_key?("review") && workset && workset.can_review?
    elsif not object.can_view?(current_person)
      status 404
      return ret
    end

    ret[:object] = object

    if params[:splat]
      ret[:path] = File.join(*params[:splat])
      ret[:path].chomp!('/')
      if ret[:path] == ""
        ret.delete :path
      end
    end

    ret
  end

  # Based on the incoming parameters, find the object and workset if specified.
  # Returns nil if any of the objects requested cannot be found or viewed.
  def gatherObjects(params)
    @params = determineObjects(params)
  end

  def objectIndex(params_or_options = nil)
    # We can query by type
    format = request.preferred_type(['text/html', 'application/json'])

    case format
    when 'text/html'
      object_list = Occam::Object.all
      if params["by_type"]
        object_list = object_list.where(:object_type => params["by_type"])
      end
      if params["by_tag"]
        object_list = object_list.where('tags LIKE ?', "%;#{params["by_tag"]};%")
      end
      render :haml, :"objects/index", :locals => {
        :objects => object_list
      }
    when 'application/json'
      # Return object metadata
      content_type 'application/json'
      object_list = Occam::Object.all
      if params["by_type"]
        object_list = object_list.where(:object_type => params["by_type"])
      end
      object_list.to_json
    end
  end

  def objectInvocations(params_or_options = nil)
    objects = gatherObjects(params_or_options)

    if objects
      object  = objects[:object]
      workset = objects[:workset]

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)

      data = object.retrieveInvocationData(params[:params][:category], params[:params][:revision])

      if data.nil?
        status 404
        return
      end

      content_type "application/json"
      data.to_json
    end
  end

  # Downloads the data blob for this object.
  def objectData(params_or_options = nil, options_verb = false)
    objects = gatherObjects(params_or_options)

    if params[:params].has_key? "embed"
      embed = true
    end

    if objects
      object  = objects[:object]
      workset = objects[:workset]

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)
      if embed
        headers["X-Frame-Options"] = ""
      end

      data = nil

      if !options_verb
        data = object.retrieveData()
      end

      handleCORS('*')

      if options_verb
        return
      end

      if data.nil?
        status 404
        return
      end

      data
    end
  end

  # Downloads the file given by the params for that object and workset.
  def objectFile(params_or_options = nil, options_verb = false)
    objects = gatherObjects(params_or_options)

    if params[:params].has_key? "embed"
      embed = true
    end

    headers["X-Frame-Options"] = ""

    if options_verb
      headers "Allow" => "GET,OPTIONS"
    end

    if objects
      object  = objects[:object]
      workset = objects[:workset]

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)
      # Also, allow this through the backend. So put this in the Object/etc model
      path = objects[:path]

      data         = nil
      content_type = nil
      fileInfo     = nil

      # Allows embedding in an iframe on another domain
      if embed
        headers["X-Frame-Options"] = ""
      end

      handleCORS('*')

      if path
        # Redirect to resources if path is within a linked resource
        resource, subpath = object.pathResource(path)

        if resource
          section = "raw"
          redirectURL = "/objects/#{resource['id']}/#{resource['revision']}/#{section}/#{subpath}"
          redirect redirectURL, 301
          return
        end

        content_type = object.fileType(path)
        fileInfo = object.retrieveFileInfo(path)

        if not fileInfo
          status 404
          return
        end

        if !options_verb
          data = object.retrieveFile(path)
        end
      end

      if options_verb
        return
      end

      if data.nil?
        status 404
        return
      end

      case File.extname(path)
      when ".svg"
        content_type "image/svg+xml"
      when ".xml"
        content_type "text/xml"
      when ".json"
        content_type "application/json"
      else
        content_type object.fileType(path)
      end

      cache_control :public

      data
    end
  end

  # Downloads the file given by the params for that object and workset.
  def objectFileStat(params_or_options = nil)
    objects = gatherObjects(params_or_options)

    if objects
      object  = objects[:object]
      workset = objects[:workset]

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)
      # Also, allow this through the backend. So put this in the Object/etc model
      path = objects[:path]

      data         = nil
      content_type = nil
      fileInfo     = nil

      handleCORS('*')

      if path
        # Redirect to resources if path is within a linked resource
        resource, subpath = object.pathResource(path)

        if resource
          section = "stat"
          redirect "/objects/#{resource['id']}/#{resource['revision']}/#{section}/#{subpath}", 301
          return
        end

        content_type = object.fileType(path)
        fileInfo = object.retrieveFileInfo(path)

        if fileInfo.nil?
          status 404
          return
        end

        content_type "application/json"
        cache_control :public

        fileInfo.to_json
      else
        status 404
      end
    end
  end

  # Renders the object's configuration form
  def objectConfigurationShow(params_or_options = nil, as_content_type = nil, tree = false)
    objects = gatherObjects(params_or_options)

    if objects
      object  = objects[:object]
      workset = objects[:workset]

      format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

      if as_content_type
        format = as_content_type
      end

      handleCORS()

      case format
      when 'application/json'
        content_type "application/json"
      else
        object_info = object.objectInfo

        connection_index    = (params[:params][:connection_index] || 0).to_i
        configuration_index = (params[:params][:configuration_index] || 0).to_i

        # If this is a configuration from a connection (from an experiment) then
        # also pass along the current values so they are rendered as the
        # defaults.
        if params[:params].has_key? "connection_index"
          experiment = object

          connection = experiment.workflow['connections'][connection_index]

          object = connection['object_realized']

          configuration = object.configurations[configuration_index]
          defaults = experiment.retrieveJSON("config/#{connection_index}/#{configuration['file']}")
        end

        # If this is from a paper object, we can pass along the current value and the
        if params[:params].has_key? "page_index"
          paper = object

          pageIndex = (params[:params]["page_index"] || 0).to_i
          itemIndex = (params[:params]["item_index"] || 0).to_i

          pages = (object_info['pages'] || [])
          if pageIndex >= pages.length
            status 404
            return
          end
          page = pages[pageIndex]

          items = (page['items'] || [])
          if itemIndex >= items.length
            status 404
            return
          end

          item = items[itemIndex]

          if !item.has_key?("data") || !item["data"].has_key?("object")
            status 404
            return
          end
          object = Occam::Object.findObject(item['data']['object']['id'])

          if object.nil?
            status 404
            return
          end

          configuration = object.configurations[configuration_index]
          defaults      = item['data']['configuration'][configuration['name']]
        end

        schema = object.configurations[configuration_index]

        render :haml, :"objects/_configuration", :layout => !request.xhr?, :locals => {
          :reviewing      => @params[:params] && @params[:params].has_key?("review"),
          :object         => object,
          :object_info    => object_info,
          :workset        => workset,
          :storedRevision => objects[:storedRevision],
          :revision       => object.revision,
          :index          => connection_index,
          :configuration  => schema,
          :configuration_index => configuration_index,
          :defaults       => defaults,
          :tab            => params[:params][:tab],
          :help           => params[:params]["help"]
        }
      end
    end
  end

  # Renders the object history view
  def objectHistory(params_or_options = nil, as_content_type = nil)
    objects = gatherObjects(params_or_options)

    if objects
      object  = objects[:object]
      workset = objects[:workset]

      if workset.nil?
        workset = object.worksetBelongsTo
      end

      format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

      if as_content_type
        format = as_content_type
      end

      handleCORS()

      query = []
      embedded = false

      if @params[:params] && @params[:params].has_key?("review")
        query << "review"
      end

      if @params[:params].has_key?("embed")
        embedded = true
        query << "embed"
      end

      if query.any?
        query = "?#{query.join('&')}"
      else
        query = ""
      end

      case format
      when 'application/json'
        content_type "application/json"

        commits = object.commits || []
        commits.to_json
      when 'text/html'
        object_info = object.objectInfo

        if @params[:params] && @params[:params].has_key?("review")
          object_info.delete 'authors'
          object_info.delete 'collaborators'
          object_info.delete 'citation'
          object_info.delete 'organization'
          object_info.delete 'website'
        end

        # TODO: stream this file (Add a retrieveStream to Git object that returns
        # the output stream object or can take a stream.)
        commits = object.commits || []

        render :haml, :"objects/history", :locals => {
          :embedded       => embedded,
          :reviewing      => @params[:params] && @params[:params].has_key?("review"),
          :commits        => commits,
          :object         => object,
          :object_info    => object_info,
          :workset        => workset,
          :storedRevision => objects[:storedRevision],
          :revision       => object.revision,
          :tab            => params[:params][:tab],
          :help           => params[:params]["help"]
        }
      when path_content_type
        content_type path_content_type
        data
      else
        # TODO: not certain the http code for rejecting upon http accept
        status 404
      end
    end
  end

  # Renders the object/show view with the object and the path to a file
  # within the object (if path is provided in params)
  def objectShow(params_or_options = nil, as_content_type = nil, tree = false, embed = false)
    objects = gatherObjects(params_or_options)

    if objects
      object  = objects[:object]
      workset = objects[:workset]

      if params[:params].has_key? "embed"
        embed = true
      end

      if workset.nil?
        workset = object.worksetBelongsTo
      end

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)
      # Also, allow this through the backend. So put this in the Object/etc model
      path = objects[:path]

      data         = nil
      content_type = nil
      fileInfo     = nil

      path_content_type = "text/html"

      if embed
        headers["X-Frame-Options"] = ""
      end

      if path
        # Redirect to resources if path is within a linked resource
        resource, subpath = object.pathResource(path)

        if resource
          section = "raw"
          if tree
            section = "tree"
          end
          # TODO: maybe pass along ?embed
          redirect "/objects/#{resource['id']}/#{resource['revision']}/#{section}/#{subpath}", 301
          return
        end

        path_content_type = object.fileType(path)
        fileInfo = object.retrieveFileInfo(path)

        if not fileInfo
          status 404
          return
        end

        if object.fileIsText?(path)
          data = object.retrieveFile(path)
        end
      end

      format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml', path_content_type])

      if as_content_type
        format = as_content_type
      end

      handleCORS()

      viewers = object.viewers
      runners = object.runners

      if current_person
        association = current_person.associationFor(object)
      else
        association = Occam::Object.associationFor(object)
      end

      if association.nil?
        viewer = viewers.first
      else
        viewer = association.object
      end

      runner = runners.first

      case format
      when 'application/json'
        content_type "application/json"

        if tree && object.fileIsGroup?(path)
          files = []

          object.eachFile(path) do |file|
            files.append(file)
          end

          {
            "files" => files
          }.to_json
        else
          if path_content_type == "application/json"
            data
          else
            info = object.objectInfo
            info['revision'] = object.revision
            info.to_json
          end
        end
      when 'text/html'
        object_info = object.objectInfo

        if @params[:params] && @params[:params].has_key?("review")
          object_info.delete 'authors'
          object_info.delete 'collaborators'
          object_info.delete 'citation'
          object_info.delete 'organization'
          object_info.delete 'website'
        end

        pagination = {}
        if params[:params]["pageIndex"]
          pagination[:pageIndex] = params[:params]["pageIndex"].to_i
        else
          pagination[:pageIndex] = 1
        end
        if params[:params]["pageItems"]
          pagination[:pageItems] = params[:params]["pageItems"].to_i
        else
          pagination[:pageItems] = 30
        end

        if pagination[:pageItems] < 1
          pagination[:pageItems] = 1
        end
        if pagination[:pageItems] > 30
          pagination[:pageItems] = 30
        end

        if embed
          render :haml, :"objects/show", :layout => :embed, :locals => {
            :embedded        => true,
            :reviewing       => @params[:params] && @params[:params].has_key?("review"),
            :viewers         => viewers,
            :runners         => runners,
            :preferredViewer => viewer,
            :preferredRunner => runner,
            :object          => object,
            :object_info     => object_info,
            :workset         => workset,
            :data_mime_type  => path_content_type,
            :storedRevision  => objects[:storedRevision],
            :revision        => object.revision,
            :data            => data,
            :path            => path,
            :fileInfo        => fileInfo,
            :pagination      => pagination,
            :tab             => params[:params][:tab],
            :help            => params[:params]["help"]
          }
        else
          render :haml, :"objects/show", :locals => {
            :embedding       => false,
            :reviewing       => @params[:params] && @params[:params].has_key?("review"),
            :viewers         => viewers,
            :runners         => runners,
            :preferredViewer => viewer,
            :preferredRunner => runner,
            :object          => object,
            :object_info     => object_info,
            :workset         => workset,
            :data_mime_type  => path_content_type,
            :storedRevision  => objects[:storedRevision],
            :revision        => object.revision,
            :data            => data,
            :path            => path,
            :fileInfo        => fileInfo,
            :pagination      => pagination,
            :tab             => params[:params][:tab],
            :help            => params[:params]["help"]
          }
        end
      when path_content_type
        content_type path_content_type
        data
      else
        # TODO: not certain the http code for rejecting upon http accept
        status 404
      end
    end
  end

  def objectUpload(params_or_options)
    objects = gatherObjects(params_or_options)

    if objects
      object  = objects[:object]
      workset = objects[:workset]

      path = objects[:path]

      if objects[:params]["files"]
        objects[:params]["files"].each do |file|
          new_revision = object.uploadFile(workset, file[:filename], file[:tempfile], path)
          object.revision = new_revision
        end
      elsif objects[:params]["directory_name"]
        new_revision = object.uploadFile(workset, ".gitignore", StringIO.new, File.join(path, objects[:params]["directory_name"]))
        object.revision = new_revision
      end

      if request.referrer
        # TODO: This isn't good enough when the revision changes and we were not on a HEAD view
        redirect request.referrer
      else
        redirect objectURL(object, path, {
          :object   => object,
          :revision => object.revision
        })
      end
    end
  end

  get '/objects/?' do
    objectIndex(params)
  end

  # List invocation information
  get '/objects/:uuid/invocations/:category' do
    objectInvocations(params)
  end
  get '/objects/:uuid/:revision/invocations/:category' do
    objectInvocations(params)
  end

  # Retrieve history
  get '/objects/:uuid/:revision/history' do
    objectHistory(params)
  end

  get '/objects/:uuid/:revision/history.json' do
    objectHistory(params, 'application/json')
  end

  # Removes an object from a workset
  delete '/worksets/:uuid/:revision/contents/:item_index' do
    objects = determineObjects(params)

    if objects
      workset = objects[:object]

      if workset && workset.can_edit?(current_person)
        revisions = workset.removeItem(params[:item_index]).split("\n")
        new_revision = revisions[0]
        new_workset_revision = revisions[-1]

        # TODO: referrer
        redirect "/worksets/#{workset.uid}/#{new_workset_revision}"
      else
        status 404
      end
    else
      status 404
    end
  end

  # Removes an object from a group
  delete '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/contents/:item_index' do
    objects = determineObjects(params)

    if objects
      object  = objects[:object]
      workset = objects[:workset]

      if object && object.is_a?(Group) && workset && workset.can_edit?(current_person)
        revisions = object.removeItem(params[:item_index], workset).split("\n")
        new_revision = revisions[0]
        new_workset_revision = revisions[-1]

        redirect "/worksets/#{workset.uid}/#{new_workset_revision}/groups/#{object.uid}/#{new_revision}"
      else
        status 404
      end
    else
      status 404
    end
  end

  # Adds a page to an object
  post '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/pages' do
    objects = determineObjects(params)

    if objects
      object  = objects[:object]
      workset = objects[:workset]

      if workset && workset.can_edit?(current_person)
        revisions = object.addPage(params['name'], workset).split("\n")
        new_revision = revisions[0]
        new_workset_revision = revisions[-1]
        redirect "/worksets/#{workset.uid}/#{new_workset_revision}/objects/#{object.uid}/#{new_revision}/pages"
      else
        status 404
      end
    else
      status 404
    end
  end

  # Deletes a page from an object
  delete '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/pages/:page_index' do
    objects = determineObjects(params)

    if objects
      object  = objects[:object]
      workset = objects[:workset]

      if workset && workset.can_edit?(current_person)
        revisions = object.removePage(params[:page_index], workset).split("\n")
        new_revision = revisions[0]
        new_workset_revision = revisions[-1]
        redirect "/worksets/#{workset.uid}/#{new_workset_revision}/objects/#{object.uid}/#{new_revision}/pages"
      else
        status 404
      end
    else
      status 404
    end
  end

  # Perform actions
  post '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/history' do
    # Retrieve command JSON
    begin
      input = JSON.load(request.body)
    rescue
      # Parsing failure
      status 400
      return
    end

    # Gather objects / worksets
    options = determineObjects(params)
    object  = options[:object]
    workset = options[:workset]

    if object.nil? || workset.nil? || !workset.can_edit?(current_person)
      status 404
      return
    end

    # Examples of inputs:
    # {
    #   "command": "new",
    #   "arguments": [{
    #     "key": "--name",
    #     "values": ["foobar"]
    #   }]
    # }
    #
    # {
    #   "command": "configure",
    #   "arguments": [{
    #     "values": ["key_1", "foo",
    #                "key_2", "bar"]
    #   }]
    # }

    command   = input["command"]
    arguments = input["arguments"]

    revisions = object.performAction(workset, command, arguments)

    if revisions.nil? || revisions.strip.blank?
      status 404
      return
    end

    revisions = revisions.strip.split("\n")

    revision        = revisions[0]
    worksetRevision = revisions[-1]

    content_type 'application/json'
    ret = {
      :objectRevision  => revision,
      :worksetRevision => worksetRevision
    }.to_json

    ret
  end

  # Lists object pages
  get '/objects/:uuid/page-:page_id' do
    params[:tab] = "page-#{params[:page_id]}"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/page-:page_id' do
    params[:tab] = "page-#{params[:page_id]}"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/objects/:uuid/page-:page_id' do
    params[:tab] = "page-#{params[:page_id]}"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/page-:page_id' do
    params[:tab] = "page-#{params[:page_id]}"
    objectShow(params)
  end

  # Resolves datasets within the given posted
  post '/objects/:uuid/:revision/configurations/data' do
    # pull down posted configurations
    begin
      input = JSON.load(request.body)
    rescue
      status 404
      return
    end

    options = determineObjects(params)
    object  = options[:object]
    workset = options[:workset]

    if object.nil?
      status 404
      return
    end

    # look at datasets in all configuration objects
    object.configurations.each_with_index do |configuration, i|
      name = configuration['name'] || configuration['label']
      schema = object.configurationSchema(i)
      puts "Looking at configuration #{name}"

      # Crawl configuration and subInput
      def crawl(subInput, key, hash)
        return if subInput.nil? || (subInput.is_a?(Hash) && !subInput.has_key?(key))

        if hash.has_key?('type') && !hash['type'].is_a?(Hash)
          # Value
          if hash['type'] == 'array'
            # Array of values
            subInput[key].each_with_index do |element, index|
              crawl(subInput[key], index, hash['element'])
            end
            subInput.delete(key) if subInput[key].empty?
          elsif hash['type'] == 'datapoints'
            # Resolve the data points
            puts "Resolving data points for #{key}"

            return if !subInput[key].is_a?(Array)

            datapoints = subInput[key]
            subInput[key] = {
              "values" => []
            }

            datapoints.each do |datapoint|
              # Retrieve data object
              dataObject = Occam::Object.findObject(datapoint['object']['id'], datapoint['object']['revision'])
              return if dataObject.nil?
              data = dataObject.fileAsJSON
              schema = dataObject.schema

              # Retrieve data values for the given nesting
              nesting = datapoint['nesting']
              def gatherData(ret, schema, subData, nesting, index, key=nil)
                return if subData.nil?
                if index >= nesting.length
                  value = subData
                  if schema['type'] == 'float'
                    value = subData.to_f
                  elsif schema['type'] == 'int'
                    value = subData.to_i
                  end
                  if !ret.has_key?('units') && schema.has_key?('units')
                    ret['units'] = schema['units']
                  end
                  if !ret.has_key?('name') && (schema.has_key?('label') || key)
                    ret['name'] = schema['label'] || key
                  end
                  ret['values'] << value
                end

                key = nesting[index]

                if key.is_a? String
                  gatherData(ret, schema[key], subData[key], nesting, index+1, key)
                elsif key.is_a? Array
                  key.each do |i|
                    if i.is_a? Array
                      # Range
                      # TODO: security: unbounded loop
                      #       solutions: cache result
                      if (i.length > 1)
                        if i[0] >= 0 && i[1] < subData.length
                          (i[0]..i[1]).each do |j|
                            gatherData(ret, schema[0], subData[j], nesting, index+1, key)
                          end
                        end
                      end
                    else
                      gatherData(ret, schema[0], subData[i.to_i], nesting, index+1, key)
                    end
                  end
                end
              end
              gatherData(subInput[key], schema, data, nesting, 0)
            end

            # Delete empty datapoints
            subInput.delete(key) if subInput[key]['values'].empty?
          else
            # Remove the key
            subInput.delete(key) if subInput.has_key?(key)
          end
        else
          # Group
          hash.each do |k, v|
            # Sub Group
            if v.is_a? Hash
              crawl(subInput[key], k, hash[k])
              if subInput[key].empty?
                subInput.delete key
              end
            end
          end
        end
      end
      crawl(input, name, schema)
    end

    content_type 'application/json'
    input.to_json
  end

  # Views configurations
  get '/objects/:uuid/configurations/:configuration_index' do
    objectConfigurationShow(params)
  end
  get '/objects/:uuid/:revision/configurations/:configuration_index' do
    objectConfigurationShow(params)
  end

  # Lists object outputs
  get '/objects/:uuid/output' do
    params[:tab] = "output"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/output' do
    params[:tab] = "output"
    objectShow(params)
  end

  # Views object
  get '/objects/:uuid/view' do
    params[:tab] = "view"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/view' do
    params[:tab] = "view"
    objectShow(params)
  end

  # Lists object provenance
  get '/objects/:uuid/provenance' do
    params[:tab] = "provenance"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/provenance' do
    params[:tab] = "provenance"
    objectShow(params)
  end

  # Lists object pages
  get '/objects/:uuid/pages' do
    params[:tab] = "pages"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/pages' do
    params[:tab] = "pages"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/objects/:uuid/pages' do
    params[:tab] = "pages"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/pages' do
    params[:tab] = "pages"
    objectShow(params)
  end

  # Lists object metadata
  get '/objects/:uuid/metadata' do
    params[:tab] = "metadata"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/metadata' do
    params[:tab] = "metadata"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/objects/:uuid/metadata' do
    params[:tab] = "metadata"
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/metadata' do
    params[:tab] = "metadata"
    objectShow(params)
  end

  # List object files
  get '/objects/:uuid/files' do
    params[:tab] = "files"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/files' do
    params[:tab] = "files"
    objectShow(params)
  end

  # List run terminal
  get '/objects/:uuid/run/?' do
    params[:tab] = "run"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/run/?' do
    params[:tab] = "run"
    objectShow(params)
  end

  # List build terminal
  get '/objects/:uuid/build' do
    params[:tab] = "build"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/build' do
    params[:tab] = "build"
    objectShow(params)
  end

  # List console terminal
  get '/objects/:uuid/console' do
    params[:tab] = "console"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/console' do
    params[:tab] = "console"
    objectShow(params)
  end

  # List citation information
  get '/objects/:uuid/citation' do
    params[:tab] = "citation"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/citation' do
    params[:tab] = "citation"
    objectShow(params)
  end

  # Retrieve the object metadata specifically as JSON
  get '/worksets/:workset_uuid/objects/:uuid.json' do
    objectShow(params, "application/json")
  end
  get '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision.json' do
    objectShow(params, "application/json")
  end
  options '/objects/:uuid/:revision.json' do
    objectShow(params, "application/json")
  end
  get '/objects/:uuid/:revision.json' do
    objectShow(params, "application/json")
  end
  get '/objects/:uuid.json' do
    objectShow(params, "application/json")
  end

  # Show object
  get '/worksets/:workset_uuid/objects/:uuid/?' do
    objectShow(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/?' do
    objectShow(params)
  end
  get '/objects/:uuid/?' do
    objectShow(params)
  end
  get '/objects/:uuid/:revision/?' do
    objectShow(params)
  end

  # Retrieve the file from the git repository for the object
  get '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/tree/*' do
    params[:tab] = "files"
    objectShow(params, nil, true)
  end
  get '/worksets/:workset_uuid/objects/:uuid/tree/*' do
    params[:tab] = "files"
    objectShow(params, nil, true)
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/tree/*' do
    params[:tab] = "files"
    objectShow(params, nil, true)
  end
  get '/worksets/:workset_uuid/experiments/:uuid/tree/*' do
    params[:tab] = "files"
    objectShow(params, nil, true)
  end
  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/tree/*' do
    params[:tab] = "files"
    objectShow(params, nil, true)
  end
  get '/worksets/:workset_uuid/groups/:uuid/tree/*' do
    params[:tab] = "files"
    objectShow(params, nil, true)
  end
  get '/objects/:uuid/:revision/tree/*' do
    params[:tab] = "files"
    objectShow(params, nil, true)
  end
  get '/objects/:uuid/tree/*' do
    params[:tab] = "files"
    objectShow(params, nil, true)
  end

  # Retrieve the actual data from the git repository for the object
  get '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/raw/*' do
    objectFile(params)
  end
  get '/worksets/:workset_uuid/objects/:uuid/raw/*' do
    objectFile(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/raw/*' do
    objectFile(params)
  end
  get '/worksets/:workset_uuid/experiments/:uuid/raw/*' do
    objectFile(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/raw/*' do
    objectFile(params)
  end
  get '/worksets/:workset_uuid/groups/:uuid/raw/*' do
    objectFile(params)
  end
  options '/objects/:uuid/:revision/raw/*' do
    objectFile(params, true)
  end
  get '/objects/:uuid/:revision/raw/*' do
    objectFile(params)
  end
  options '/objects/:uuid/raw/*' do
    objectFile(params, true)
  end
  get '/objects/:uuid/raw/*' do
    objectFile(params)
  end

  # Retrieve the file info from the git repository for the object
  get '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/stat/*' do
    objectFileStat(params)
  end
  get '/worksets/:workset_uuid/objects/:uuid/stat/*' do
    objectFileStat(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/experiments/:uuid/:revision/stat/*' do
    objectFileStat(params)
  end
  get '/worksets/:workset_uuid/experiments/:uuid/stat/*' do
    objectFileStat(params)
  end
  get '/worksets/:workset_uuid/:workset_revision/groups/:uuid/:revision/stat/*' do
    objectFileStat(params)
  end
  get '/worksets/:workset_uuid/groups/:uuid/stat/*' do
    objectFileStat(params)
  end
  get '/objects/:uuid/:revision/stat/*' do
    objectFileStat(params)
  end
  get '/objects/:uuid/stat/*' do
    objectFileStat(params)
  end

  # Uploads a file to an object
  post '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/tree/*' do
    objectUpload(params)
  end

  # Object view tab
  get '/objects/:uuid/view' do
    params[:tab] = "view"
    objectShow(params)
  end
  get '/objects/:uuid/:revision/view' do
    params[:tab] = "view"
    objectShow(params)
  end

  # List all object types
  get '/objects' do
    # We can query by type
    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    case format
    when 'text/html'
      # TODO: render
    when 'application/json'
      # Return object metadata
      content_type 'application/json'
      object_list = Occam::Object.all
      if params["by_type"]
        object_list = object_list.where(:object_type => params["by_type"])
      end
      object_list.to_json
    end
  end

  # List information about the particular object history, if found
  get '/objects/:uuid/schema' do
    object_type = params[:object_type]
    uuid        = params[:uuid]

    if object_type == "workset"
      return redirect "/worksets/#{uuid}"
    elsif object_type == "experiment"
      return redirect "/experiments/#{uuid}"
    elsif object_type == "group"
      return redirect "/groups/#{uuid}"
    end

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      # Redirect to latest/base revision
      full_revision = object.fullRevision

      if full_revision.nil?
        status 404
      end

      # TODO: which redirect code?? (non-cached)
      redirect "/objects/#{object_type}/#{uuid}/#{full_revision}/schema"
    end
  end

  # List information about the particular object history, if found
  get '/objects/:uuid/file' do
    object_type = params[:object_type]
    uuid        = params[:uuid]

    if object_type == "workset"
      return redirect "/worksets/#{uuid}/file"
    elsif object_type == "experiment"
      return redirect "/experiments/#{uuid}/file"
    elsif object_type == "group"
      return redirect "/groups/#{uuid}/file"
    end

    object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first

    if object.nil?
      status 404
    else
      # Redirect to latest/base revision
      full_revision = object.fullRevision

      if full_revision.nil?
        status 404
      end

      # TODO: which redirect code?? (non-cached)
      redirect "/objects/#{object_type}/#{uuid}/#{full_revision}/file"
    end
  end

  # Retrieve the full revision for this partial revision
  get '/objects/:uuid/:revision/full_revision' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    if params[:object_type] == "workset"
      object = Occam::Workset.where(:uid => uuid).first
    elsif params[:object_type] == "person"
      object = Occam::Person.where(:uid => uuid).first
    elsif params[:object_type] == "group"
      object = Occam::Group.where(:uid => uuid).first
    elsif params[:object_type] == "experiment"
      object = Occam::Experiment.where(:uid => uuid).first
    else
      object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first
    end

    if object.nil?
      status 404
    else
      object.revision = revision

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)
      data = object.fullRevision

      if not data
        status 404
      else
        content_type "application/json"
        {
          "revision" => data
        }.to_json
      end
    end
  end

  # Retrieve the parent revision for this object
  get '/objects/:uuid/:revision/parent_revision' do
    object_type = params[:object_type]
    revision    = params[:revision]
    uuid        = params[:uuid]

    if params[:object_type] == "workset"
      object = Occam::Workset.where(:uid => uuid).first
    elsif params[:object_type] == "person"
      object = Occam::Person.where(:uid => uuid).first
    elsif params[:object_type] == "group"
      object = Occam::Group.where(:uid => uuid).first
    elsif params[:object_type] == "experiment"
      object = Occam::Experiment.where(:uid => uuid).first
    else
      object = Occam::Object.where(:object_type_safe => object_type, :uid => uuid).first
    end

    if object.nil?
      status 404
    else
      object.revision = revision
      object_path = object.path

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)
      data = object.parentRevision

      if not data
        status 404
      else
        content_type "application/json"
        {
          "parentRevision" => data
        }.to_json
      end
    end
  end

  # Retrieve the child revisions for this object
  get '/objects/:uuid/:revision/child_revisions' do
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:uid => uuid).first

    if object.nil?
      status 404
    else
      object.revision = revision
      object_path = object.path

      # TODO: stream this file (Add a retrieveStream to Git object that returns
      # the output stream object or can take a stream.)
      data = object.childRevisions

      if not data
        status 404
      else
        content_type "application/json"
        {
          "childRevisions" => data
        }.to_json
      end
    end
  end

  # Retrieves the file as structured JSON
  get '/objects/:uuid/:revision/data' do
    # We can query by type
    format = request.preferred_type(['text/html', 'application/json'])

    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:uid => uuid).first

    if object.nil?
      status 404
    else
      object_info = object.objectInfo

      # TODO: handle file type
      case format
      when 'text/html'
        # AJAX Request for the data form
        if request.xhr?
          render :haml, :"objects/_data", :layout => false, :locals => {
            :object => object
          }
        else
        end
      when 'application/json'
        content_type "application/json"
        object.retrieveFile(object_info['file'])
      end
    end
  end

  # Retrieves the file as structured JSON
  get '/objects/:uuid/:revision/data/*' do
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:uid => uuid).first

    if object.nil?
      status 404
    else
      object_info = object.objectInfo

      # TODO: handle file type
      data = object.retrieveJSON(object_info['file'])

      # Retrieve field from splat
      params[:splat].first.split('/').each do |key|
        if data.is_a? Array
          data = data[key.to_i]
        else
          # Revert from base64

          # Add padding back
          if (key.length % 4) > 0
            key = key + ("=" * (4 - (key.length % 4)))
          end

          key = Base64.urlsafe_decode64(key)

          data = data[key]
        end

        if data.nil?
          status 404
          return
        end
      end

      content_type "application/json"
      data.to_json
    end
  end

  get '/objects/:uuid/:revision/file' do
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:uid => uuid).first

    if object.nil?
      status 404
    else
      object_info = object.objectInfo

      # TODO: handle file type
      content_type "text/plain"
      object.retrieveFile(object_info['file'])
    end
  end

  get '/objects/:uuid/:revision/schema' do
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:uid => uuid).first

    if object.nil?
      status 404
    else
      object_info = object.objectInfo

      # TODO: handle file type
      content_type "application/json"
      object.retrieveFile(object_info['schema'])
    end
  end

  get '/objects/:uuid/:revision/configurations' do
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:uid => uuid).first

    if object.nil?
      status 404
    else
      object.revision = revision
      info = object.objectInfo

      if info.empty?
        status 404
        return
      end

      if not info.has_key? 'configurations'
        status 404
      else
        configurations = info['configurations']
        configurations.map do |configuration|
          if configuration.has_key? 'schema'
            # Pull schema file from git
            configuration['schema'] = object.retrieveJSON(configuration['schema'])
          else
            configuration['schema'] = {}
          end
          configuration
        end

        render :haml, :"objects/configurations", :locals => {
          :revision       => params[:revision],
          :info           => info,
          :object         => object,
          :recipe         => nil,
          :configurations => configurations
        }
      end
    end
  end

  get '/objects/:uuid/:revision/configurations/:configuration_id' do
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:uid => uuid).first

    if object.nil?
      status 404
    else
      object.revision = revision
      info = object.objectInfo

      if info.empty?
        status 404
        return
      end

      params[:configuration_id] = params[:configuration_id].to_i

      if not info.has_key? 'configurations' or params[:configuration_id] >= info['configurations'].length
        status 404
      else
        configuration = info['configurations'][params[:configuration_id]]
        if configuration.has_key? 'schema'
          # Pull schema file from git
          schema = object.retrieveJSON(configuration['schema'])
        else
          schema = {}
        end

        render :haml, :"objects/configuration", :locals => {
          :revision      => params[:revision],
          :info          => info,
          :object        => object,
          :recipe        => nil,
          :configuration => configuration,
          :schema        => schema,
        }
      end
    end
  end

  get '/objects/:uuid/:revision/outputs/:output_id' do
    revision    = params[:revision]
    uuid        = params[:uuid]

    object = Occam::Object.where(:uid => uuid).first

    if object.nil?
      status 404
    else
      info = object.objectInfo

      if info.empty?
        status 404
        return
      end

      params[:output_id] = params[:output_id].to_i

      if not info.has_key? 'outputs' or params[:output_id] >= info['outputs'].length
        status 404
      else
        output = info['outputs'][params[:output_id]]
        if output.has_key? 'schema'
          # Pull schema file from git
          schema = object.retrieveJSON(output['schema'])
        else
          schema = {}
        end

        render :haml, :"objects/output", :locals => {
          :revision      => params[:revision],
          :info          => info,
          :object        => object,
          :recipe        => nil,
          :output        => output,
          :schema        => schema,
        }
      end
    end
  end

  # Object import
  get '/import' do
    render :haml, :"objects/import", :locals => {
      :errors      => nil,
      :default_url => ""
    }
  end

  post '/import' do
    if params.has_key? "url"
      url  = params["url"]

      json = Occam::Object.download_json(url)
      obj  = Occam::Object.import(url, json)
      deps = Occam::Object.all_unknown_dependencies(url, json)

      errors = obj.errors

      if obj.persisted?
        # Already exists, redirect to that page
        redirect "/objects/#{obj.uid}"
      else
        return render :haml, :"objects/import_confirm", :locals => {
          :path         => "/",
          :errors       => obj.errors,
          :object       => obj,
          :dependencies => deps,
          :url          => url
        }
      end
    else
      status 404
    end
  end

  # TODO: This should do import and new?
  post '/objects' do
    object = Occam::Object.import_all(params["url"])

    if object.errors.any?
      status 422
      render :haml, :"objects/import", :locals => {
        :errors => object.errors,
        :default_url => params["url"]
      }
    else
      object.install
      object.build

      redirect "/objects/#{object.uid}"
    end
  end

  # Show Paper widget item configuration
  get '/worksets/:workset_uuid/:workset_revision/objects/:uuid/:revision/pages/:page_index/items/:item_index/configurations/:configuration_index/?' do
    objectConfigurationShow(params)
  end
end
