class Occam
  # This module contains methods that render a schema specification.
  module SchemaHelpers
    def render_schema(schema, dropdowns=true, key=nil)
      if (schema.is_a? Hash and schema.has_key? "type" and not schema["type"].is_a? Hash) or schema.is_a? String or (schema.is_a? Array and schema.length > 0 and schema[0].is_a? String)
        puts("a")
        puts schema
        # Output form input
        value = ""
        if schema.is_a? Hash and schema.has_key?("default")
          value = schema["default"]
        end

        if schema.is_a? Hash and schema.has_key?("type")
          type = schema["type"]
        else
          type = schema
        end

        if type.is_a?(Array)
          type = "array"
        end

        # Render item
        if schema["type"].is_a? Array and dropdowns
          "<p class='#{type}'>#{schema["label"]}</p>" +
          "<div class='select'><select>" + schema["type"].map { |sub_type|
            selected = (value == sub_type)
            "<option#{selected ? " selected='selected'" : ""}>#{sub_type}</option>"
          }.join("") + "</select></div>" +
          "<div class='dots'></div>" +
          "<div class='description'>#{schema["description"]}</div>"
        else
          "<p class='#{type}'>#{key}</p><p>#{value} #{type}</p>" +
          "<div class='dots'></div>" +
          "<div class='description'>#{schema["description"]}</div>"
        end
      elsif schema.is_a? Hash
        puts("b")
        puts schema
        # Render group
        ordering = schema["__ordering"]
        if not schema.has_key? "__ordering"
          ordering = schema.keys
        end
        "<ul class='configuration-group'>" + ordering.map { |k|
          puts "id-#{k}"
          v = schema[k]
          if (v.is_a? Hash and v.has_key? "type" and not v["type"].is_a? Hash) or v.is_a? String or (v.is_a? Array and v.length > 0 and v[0].is_a? String)
            # An input value
            "<li>#{render_schema(v, dropdowns, k)}</li>"
          elsif v.is_a? Array
            # A list
            "<li><h2><span class='expand shown'>&#9662;</span>#{k} 0 or more</h2>#{render_schema(v, dropdowns, k)}</li>"
          else
            # A group
            "<li><h2><span class='expand shown'>&#9662;</span>#{k}</h2>#{render_schema(v, dropdowns, k)}</li>"
          end
        }.join("") + "</ul>"
      elsif schema.is_a? Array
        puts("c")
        # Render list
        render_schema(schema[0], dropdowns, key)
      else
        # Invalid item
      end
    end
  end

  helpers SchemaHelpers
end
